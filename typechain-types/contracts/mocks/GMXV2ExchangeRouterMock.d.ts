/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { EventData, PastEventOptions } from "web3-eth-contract";

export interface GMXV2ExchangeRouterMockContract
  extends Truffle.Contract<GMXV2ExchangeRouterMockInstance> {
  "new"(
    meta?: Truffle.TransactionDetails
  ): Promise<GMXV2ExchangeRouterMockInstance>;
}

type AllEvents = never;

export interface GMXV2ExchangeRouterMockInstance
  extends Truffle.ContractInstance {
  cancelOrder: {
    (key: string, txDetails?: Truffle.TransactionDetails): Promise<
      Truffle.TransactionResponse<AllEvents>
    >;
    call(key: string, txDetails?: Truffle.TransactionDetails): Promise<void>;
    sendTransaction(
      key: string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<string>;
    estimateGas(
      key: string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<number>;
  };

  claimFundingFees: {
    (
      markets: string[],
      tokens: string[],
      receiver: string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<Truffle.TransactionResponse<AllEvents>>;
    call(
      markets: string[],
      tokens: string[],
      receiver: string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<BN[]>;
    sendTransaction(
      markets: string[],
      tokens: string[],
      receiver: string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<string>;
    estimateGas(
      markets: string[],
      tokens: string[],
      receiver: string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<number>;
  };

  claimFundingFeesReceiver(
    txDetails?: Truffle.TransactionDetails
  ): Promise<string>;

  createOrder: {
    (
      params: {
        addresses: {
          receiver: string;
          callbackContract: string;
          uiFeeReceiver: string;
          market: string;
          initialCollateralToken: string;
          swapPath: string[];
        };
        numbers: {
          sizeDeltaUsd: number | BN | string;
          initialCollateralDeltaAmount: number | BN | string;
          triggerPrice: number | BN | string;
          acceptablePrice: number | BN | string;
          executionFee: number | BN | string;
          callbackGasLimit: number | BN | string;
          minOutputAmount: number | BN | string;
        };
        orderType: number | BN | string;
        decreasePositionSwapType: number | BN | string;
        isLong: boolean;
        shouldUnwrapNativeToken: boolean;
        referralCode: string;
      },
      txDetails?: Truffle.TransactionDetails
    ): Promise<Truffle.TransactionResponse<AllEvents>>;
    call(
      params: {
        addresses: {
          receiver: string;
          callbackContract: string;
          uiFeeReceiver: string;
          market: string;
          initialCollateralToken: string;
          swapPath: string[];
        };
        numbers: {
          sizeDeltaUsd: number | BN | string;
          initialCollateralDeltaAmount: number | BN | string;
          triggerPrice: number | BN | string;
          acceptablePrice: number | BN | string;
          executionFee: number | BN | string;
          callbackGasLimit: number | BN | string;
          minOutputAmount: number | BN | string;
        };
        orderType: number | BN | string;
        decreasePositionSwapType: number | BN | string;
        isLong: boolean;
        shouldUnwrapNativeToken: boolean;
        referralCode: string;
      },
      txDetails?: Truffle.TransactionDetails
    ): Promise<string>;
    sendTransaction(
      params: {
        addresses: {
          receiver: string;
          callbackContract: string;
          uiFeeReceiver: string;
          market: string;
          initialCollateralToken: string;
          swapPath: string[];
        };
        numbers: {
          sizeDeltaUsd: number | BN | string;
          initialCollateralDeltaAmount: number | BN | string;
          triggerPrice: number | BN | string;
          acceptablePrice: number | BN | string;
          executionFee: number | BN | string;
          callbackGasLimit: number | BN | string;
          minOutputAmount: number | BN | string;
        };
        orderType: number | BN | string;
        decreasePositionSwapType: number | BN | string;
        isLong: boolean;
        shouldUnwrapNativeToken: boolean;
        referralCode: string;
      },
      txDetails?: Truffle.TransactionDetails
    ): Promise<string>;
    estimateGas(
      params: {
        addresses: {
          receiver: string;
          callbackContract: string;
          uiFeeReceiver: string;
          market: string;
          initialCollateralToken: string;
          swapPath: string[];
        };
        numbers: {
          sizeDeltaUsd: number | BN | string;
          initialCollateralDeltaAmount: number | BN | string;
          triggerPrice: number | BN | string;
          acceptablePrice: number | BN | string;
          executionFee: number | BN | string;
          callbackGasLimit: number | BN | string;
          minOutputAmount: number | BN | string;
        };
        orderType: number | BN | string;
        decreasePositionSwapType: number | BN | string;
        isLong: boolean;
        shouldUnwrapNativeToken: boolean;
        referralCode: string;
      },
      txDetails?: Truffle.TransactionDetails
    ): Promise<number>;
  };

  createOrderParams(
    txDetails?: Truffle.TransactionDetails
  ): Promise<{
    0: {
      receiver: string;
      callbackContract: string;
      uiFeeReceiver: string;
      market: string;
      initialCollateralToken: string;
      swapPath: string[];
    };
    1: {
      sizeDeltaUsd: BN;
      initialCollateralDeltaAmount: BN;
      triggerPrice: BN;
      acceptablePrice: BN;
      executionFee: BN;
      callbackGasLimit: BN;
      minOutputAmount: BN;
    };
    2: BN;
    3: BN;
    4: boolean;
    5: boolean;
    6: string;
  }>;

  sendTokens: {
    (
      token: string,
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<Truffle.TransactionResponse<AllEvents>>;
    call(
      token: string,
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<void>;
    sendTransaction(
      token: string,
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<string>;
    estimateGas(
      token: string,
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<number>;
  };

  sendWnt: {
    (
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<Truffle.TransactionResponse<AllEvents>>;
    call(
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<void>;
    sendTransaction(
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<string>;
    estimateGas(
      receiver: string,
      amount: number | BN | string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<number>;
  };

  methods: {
    cancelOrder: {
      (key: string, txDetails?: Truffle.TransactionDetails): Promise<
        Truffle.TransactionResponse<AllEvents>
      >;
      call(key: string, txDetails?: Truffle.TransactionDetails): Promise<void>;
      sendTransaction(
        key: string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<string>;
      estimateGas(
        key: string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<number>;
    };

    claimFundingFees: {
      (
        markets: string[],
        tokens: string[],
        receiver: string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<Truffle.TransactionResponse<AllEvents>>;
      call(
        markets: string[],
        tokens: string[],
        receiver: string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<BN[]>;
      sendTransaction(
        markets: string[],
        tokens: string[],
        receiver: string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<string>;
      estimateGas(
        markets: string[],
        tokens: string[],
        receiver: string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<number>;
    };

    claimFundingFeesReceiver(
      txDetails?: Truffle.TransactionDetails
    ): Promise<string>;

    createOrder: {
      (
        params: {
          addresses: {
            receiver: string;
            callbackContract: string;
            uiFeeReceiver: string;
            market: string;
            initialCollateralToken: string;
            swapPath: string[];
          };
          numbers: {
            sizeDeltaUsd: number | BN | string;
            initialCollateralDeltaAmount: number | BN | string;
            triggerPrice: number | BN | string;
            acceptablePrice: number | BN | string;
            executionFee: number | BN | string;
            callbackGasLimit: number | BN | string;
            minOutputAmount: number | BN | string;
          };
          orderType: number | BN | string;
          decreasePositionSwapType: number | BN | string;
          isLong: boolean;
          shouldUnwrapNativeToken: boolean;
          referralCode: string;
        },
        txDetails?: Truffle.TransactionDetails
      ): Promise<Truffle.TransactionResponse<AllEvents>>;
      call(
        params: {
          addresses: {
            receiver: string;
            callbackContract: string;
            uiFeeReceiver: string;
            market: string;
            initialCollateralToken: string;
            swapPath: string[];
          };
          numbers: {
            sizeDeltaUsd: number | BN | string;
            initialCollateralDeltaAmount: number | BN | string;
            triggerPrice: number | BN | string;
            acceptablePrice: number | BN | string;
            executionFee: number | BN | string;
            callbackGasLimit: number | BN | string;
            minOutputAmount: number | BN | string;
          };
          orderType: number | BN | string;
          decreasePositionSwapType: number | BN | string;
          isLong: boolean;
          shouldUnwrapNativeToken: boolean;
          referralCode: string;
        },
        txDetails?: Truffle.TransactionDetails
      ): Promise<string>;
      sendTransaction(
        params: {
          addresses: {
            receiver: string;
            callbackContract: string;
            uiFeeReceiver: string;
            market: string;
            initialCollateralToken: string;
            swapPath: string[];
          };
          numbers: {
            sizeDeltaUsd: number | BN | string;
            initialCollateralDeltaAmount: number | BN | string;
            triggerPrice: number | BN | string;
            acceptablePrice: number | BN | string;
            executionFee: number | BN | string;
            callbackGasLimit: number | BN | string;
            minOutputAmount: number | BN | string;
          };
          orderType: number | BN | string;
          decreasePositionSwapType: number | BN | string;
          isLong: boolean;
          shouldUnwrapNativeToken: boolean;
          referralCode: string;
        },
        txDetails?: Truffle.TransactionDetails
      ): Promise<string>;
      estimateGas(
        params: {
          addresses: {
            receiver: string;
            callbackContract: string;
            uiFeeReceiver: string;
            market: string;
            initialCollateralToken: string;
            swapPath: string[];
          };
          numbers: {
            sizeDeltaUsd: number | BN | string;
            initialCollateralDeltaAmount: number | BN | string;
            triggerPrice: number | BN | string;
            acceptablePrice: number | BN | string;
            executionFee: number | BN | string;
            callbackGasLimit: number | BN | string;
            minOutputAmount: number | BN | string;
          };
          orderType: number | BN | string;
          decreasePositionSwapType: number | BN | string;
          isLong: boolean;
          shouldUnwrapNativeToken: boolean;
          referralCode: string;
        },
        txDetails?: Truffle.TransactionDetails
      ): Promise<number>;
    };

    createOrderParams(
      txDetails?: Truffle.TransactionDetails
    ): Promise<{
      0: {
        receiver: string;
        callbackContract: string;
        uiFeeReceiver: string;
        market: string;
        initialCollateralToken: string;
        swapPath: string[];
      };
      1: {
        sizeDeltaUsd: BN;
        initialCollateralDeltaAmount: BN;
        triggerPrice: BN;
        acceptablePrice: BN;
        executionFee: BN;
        callbackGasLimit: BN;
        minOutputAmount: BN;
      };
      2: BN;
      3: BN;
      4: boolean;
      5: boolean;
      6: string;
    }>;

    sendTokens: {
      (
        token: string,
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<Truffle.TransactionResponse<AllEvents>>;
      call(
        token: string,
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<void>;
      sendTransaction(
        token: string,
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<string>;
      estimateGas(
        token: string,
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<number>;
    };

    sendWnt: {
      (
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<Truffle.TransactionResponse<AllEvents>>;
      call(
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<void>;
      sendTransaction(
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<string>;
      estimateGas(
        receiver: string,
        amount: number | BN | string,
        txDetails?: Truffle.TransactionDetails
      ): Promise<number>;
    };
  };

  getPastEvents(event: string): Promise<EventData[]>;
  getPastEvents(
    event: string,
    options: PastEventOptions,
    callback: (error: Error, event: EventData) => void
  ): Promise<EventData[]>;
  getPastEvents(event: string, options: PastEventOptions): Promise<EventData[]>;
  getPastEvents(
    event: string,
    callback: (error: Error, event: EventData) => void
  ): Promise<EventData[]>;
}
