/* Autogenerated file. Do not edit manually. */
/* tslint:disable */
/* eslint-disable */

import BN from "bn.js";
import { EventData, PastEventOptions } from "web3-eth-contract";

export interface AAVEContract extends Truffle.Contract<AAVEInstance> {
  "new"(meta?: Truffle.TransactionDetails): Promise<AAVEInstance>;
}

export interface AaveSupply {
  name: "AaveSupply";
  args: {
    asset: string;
    amount: BN;
    0: string;
    1: BN;
  };
}

export interface AaveWithdraw {
  name: "AaveWithdraw";
  args: {
    asset: string;
    amount: BN;
    0: string;
    1: BN;
  };
}

type AllEvents = AaveSupply | AaveWithdraw;

export interface AAVEInstance extends Truffle.ContractInstance {
  getPositionSizes(
    _assets: string[],
    _trade: string,
    txDetails?: Truffle.TransactionDetails
  ): Promise<BN[]>;

  methods: {
    getPositionSizes(
      _assets: string[],
      _trade: string,
      txDetails?: Truffle.TransactionDetails
    ): Promise<BN[]>;
  };

  getPastEvents(event: string): Promise<EventData[]>;
  getPastEvents(
    event: string,
    options: PastEventOptions,
    callback: (error: Error, event: EventData) => void
  ): Promise<EventData[]>;
  getPastEvents(event: string, options: PastEventOptions): Promise<EventData[]>;
  getPastEvents(
    event: string,
    callback: (error: Error, event: EventData) => void
  ): Promise<EventData[]>;
}
