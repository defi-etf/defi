import { deployProxy, getDeployConfig } from "./utils";

async function main() {
    const config = getDeployConfig()
    await deployProxy('DripOperator', 'DRIP_OPERATOR', [config.reportDelay])
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });