import { ethers, upgrades } from 'hardhat'
import { verify } from "./utils";
import { getAddresses } from '../utils/get-addresses';

async function main() {
    const { REGISTRY } = getAddresses()
    const Registry = await ethers.getContractFactory('Registry')
    const upgraded = await upgrades.upgradeProxy(REGISTRY, Registry)
    const implementation = await upgrades.erc1967.getImplementationAddress(upgraded.address)
    await verify(implementation)
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });