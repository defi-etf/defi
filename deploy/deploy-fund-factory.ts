import { deployProxy, getDeployConfig } from "./utils";

async function main() {
    const { createFundFee } = getDeployConfig()
    const factory = await deployProxy('FundFactory', 'FUND_FACTORY')
    await factory.setCreateFundFee(createFundFee)
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });