#!/bin/bash
set -e
npx hardhat run --network $1 deploy/deploy-registry.ts
npx hardhat run --network $1 deploy/deploy-fund-factory.ts
npx hardhat run --network $1 deploy/deploy-feeder.ts
npx hardhat run --network $1 deploy/deploy-interaction.ts
npx hardhat run --network $1 deploy/deploy-drip-operator.ts
npx hardhat run --network $1 deploy/deploy-fees.ts
npx hardhat run --network $1 deploy/deploy-whitelist.ts
npx hardhat run --network $1 deploy/deploy-trade-params-updater.ts
npx hardhat run --network $1 deploy/deploy-trade-beacon.ts
npx hardhat run --network $1 deploy/deploy-upgrader.ts
npx hardhat run --network $1 deploy/deploy-subaccount-registry.ts
npx hardhat run --network $1 deploy/deploy-tvl-computer.ts
npx hardhat run --network $1 deploy/upgrade-registry.ts