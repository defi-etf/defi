import { deploy, getDeployConfig } from "./utils";

async function main() {
    const config = getDeployConfig()
    await deploy('TradeParamsUpdater', 'TRADE_PARAMS_UPDATER', [config.tradeParamsUpgradePeriod, config.minTradeParamsUpgradePeriod])   
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });