import { ethers, upgrades } from 'hardhat'
import { saveAddresses } from './utils/save-addresses';
import { deploy, verify } from './utils';

async function main() {
    const ZeroX = await deploy('ZeroX')
    const AAVE = await deploy('AAVE')
    const GMX = await deploy('GMX')
    const GMXV2 = await deploy('GMXV2')
    const Trade = await ethers.getContractFactory('Trade', {
        libraries: {
            ZeroX: ZeroX.address,
            AAVE: AAVE.address,
            GMX: GMX.address,
            GMXV2: GMXV2.address
        }
    });
    const beacon = await upgrades.deployBeacon(Trade, {
        unsafeAllowLinkedLibraries: true
    }).then(b => b.deployed());
    await saveAddresses({
        TRADE_BEACON: beacon.address
    })
    await verify(await beacon.implementation())
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });