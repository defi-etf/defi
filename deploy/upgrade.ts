import '@nomiclabs/hardhat-ethers';
import hre, { ethers, upgrades } from 'hardhat';
import { UpgraderInstance } from '../typechain-types/contracts/main/Upgrader';
import { getAddresses } from './utils';
import { FactoryOptions } from 'hardhat/types';
import { deploy } from './utils';

async function upgrade(
    name: string,
    proxyAddress: string,
    factoryOpts: FactoryOptions | undefined = undefined,
): Promise<string> {
    const factory = await ethers.getContractFactory(name, factoryOpts)
    const implementation = await upgrades.prepareUpgrade(proxyAddress, factory, {
        unsafeAllowLinkedLibraries: factoryOpts?.libraries !== undefined
    })
    await hre.run("verify:verify", {
        address: implementation,
    });
    return implementation
}

async function main() {
    const { FUND_FACTORY, FEEDER, INTERACTION, DRIP_OPERATOR, FEES, TRADE_BEACON, UPGRADER, REGISTRY, SUBACCOUNT_REGISTRY, TVL_COMPUTER } = getAddresses()
    const upgrader: UpgraderInstance = (await ethers.getContractFactory('Upgrader'))
        .attach(UPGRADER)
    const fundFactory = await upgrade('FundFactory', FUND_FACTORY)
    const feeder = await upgrade('Feeder', FEEDER)
    const interaction = await upgrade('Interaction', INTERACTION)
    const dripOperator = await upgrade('DripOperator', DRIP_OPERATOR)
    const fees = await upgrade('Fees', FEES)
    const ZeroX = await deploy('ZeroX')
    const AAVE = await deploy('AAVE')
    const GMXV2 = await deploy('GMXV2')
    const tradeBeacon = await upgrade('Trade', TRADE_BEACON, {
        libraries: {
            ZeroX: ZeroX.address,
            AAVE: AAVE.address,
            GMXV2: GMXV2.address
        }
    })
    const subaccountRegistry = await upgrade('SubaccountRegistry', SUBACCOUNT_REGISTRY)
    const tvlComputer = await upgrade('TVLComputer', TVL_COMPUTER)
    const registry = await upgrade('Registry', REGISTRY)
    await upgrader.requestUpgrade([
        { destination: FUND_FACTORY, implementation: fundFactory },
        { destination: FEEDER, implementation: feeder },
        { destination: INTERACTION, implementation: interaction },
        { destination: DRIP_OPERATOR, implementation: dripOperator },
        { destination: FEES, implementation: fees },
        { destination: TRADE_BEACON, implementation: tradeBeacon },
        { destination: REGISTRY, implementation: registry },
        { destination: SUBACCOUNT_REGISTRY, implementation: subaccountRegistry },
        { destination: TVL_COMPUTER, implementation: tvlComputer }
    ])
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });