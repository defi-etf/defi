import hre, { ethers } from 'hardhat'
import addresses from '../../addresses.json'
import deployConfig from '../../deploy.json';
import { Addresses } from './addresses'
import { verify } from './verify';
import { saveAddresses } from './save-addresses';
import { getNetworkKey } from './get-network-key';

export function getAddresses(): Addresses {
    return addresses[getNetworkKey(hre)]
}

export interface DeployConfig {
    reportDelay: number;
    minUpgradePeriod: number;
    upgradePeriod: number;
    minTradeParamsUpgradePeriod: number;
    tradeParamsUpgradePeriod: number;
    serviceMf: string;
    servicePf: string;
    serviceSf: string;
    createFundFee: string;
}

export function getDeployConfig(): DeployConfig {
    return {
        ...deployConfig.default,
        ...(deployConfig[getNetworkKey(hre)] ?? {})
    }
}

export async function deploy(
    name: string,
    addressKey: keyof Addresses | undefined = undefined,
    constructorArguments: any[] = []
): Promise<ethers.Contract> {
    const factory = await hre.ethers.getContractFactory(name)
    const deployed = await factory.deploy(...constructorArguments)
        .then(c => c.deployed())
    await verify(deployed.address, constructorArguments)
    if (addressKey) {
        await saveAddresses({
            [addressKey]: deployed.address
        })
    }
    return deployed
}
