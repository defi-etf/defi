export interface Addresses {
	TRIGGER_SERVER: string;
	FUND_FACTORY: string;
	USDT: string;
	FEEDER: string;
	INTERACTION: string;
	FEES: string;
	TRADE_BEACON: string;
	TRADE_ACCESS: string;
	SWAPPER: string;
	TRADE_UPGRADER: string;
	DRIP_OPERATOR: string;
	ETH_PRICE_FEED: string;
	WHITELIST: string;
	TRADE_PARAMS_UPDATER: string;
	UPGRADER: string;
	REGISTRY: string;
	SUBACCOUNT_REGISTRY: string;
	GMX_ROUTER: string;
	GMX_POSITION_ROUTER: string;
	GMX_VAULT: string;
	GMX_READER: string;
	AAVE_POOL: string;
	AAVE_POOL_DATA_PROVIDER: string;
	TVL_COMPUTER: string;

	AAVE: {
		POOL: string;
		POOL_DATA_PROVIDER: string
	} | undefined;

	GMX: {
		Vault: string;
		Router: string;
		PositionRouter: string
	} | undefined;
}