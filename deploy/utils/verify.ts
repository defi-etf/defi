import hardhat from 'hardhat'

export async function verify(address: string, constructorArguments?: any[]): Promise<void> {
    try {
        await hardhat.run("verify:verify", {
            address,
            constructorArguments
        })
    } catch (err) {
        if (err.message.includes("Reason: Already Verified")) {
            console.log("Contract is already verified!");
        } else {
            throw err
        }
    }
}