import { promises } from 'fs';
import hre from 'hardhat';
import { Addresses } from './addresses';
import { getNetworkKey } from './get-network-key';

export async function saveAddresses(update: Partial<Addresses>): Promise<void> {
	const addresses = JSON.parse(await promises.readFile('./addresses.json').then(b => b.toString('utf8')))
	const key = getNetworkKey(hre);
	const currentAddresses = addresses[key];
	return promises.writeFile('./addresses.json', JSON.stringify({
		...addresses,
		[key]: {
			...currentAddresses,
			...update,
		},
	}, undefined, 2));
}