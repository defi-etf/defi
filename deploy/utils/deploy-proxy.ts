import { ethers, upgrades } from "hardhat"
import { Addresses } from "./addresses"
import { saveAddresses } from "./save-addresses"
import { verify } from "./verify"

export async function deployProxy(name: string, addressKey: keyof Addresses, constructorArguments: any[] = []): Promise<ethers.Contract> {
    const factory = await ethers.getContractFactory(name)
    const proxy = await upgrades.deployProxy(factory, constructorArguments)
        .then(c => c.deployed())
    await saveAddresses({
        [addressKey]: proxy.address
    })
    const implementation = await upgrades.erc1967.getImplementationAddress(proxy.address)
    await verify(implementation)
    return proxy
}