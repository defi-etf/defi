import { HardhatRuntimeEnvironment } from 'hardhat/types'

export function getNetworkKey(hre: HardhatRuntimeEnvironment): string {
    return hre.network.name === 'devnet'
        ? 'OPTIMISM_GOERLI'
        : hre.network.name.toUpperCase()
}