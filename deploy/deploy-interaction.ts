import { ethers } from 'hardhat'
import { deployProxy, verify } from './utils';

async function main() {
    const investPeriodUtils = await ethers.getContractFactory("InvestPeriod")
        .then(f => f.deploy())
        .then(c => c.deployed())
    await verify(investPeriodUtils.address)
    await deployProxy('Interaction', 'INTERACTION', [investPeriodUtils.address])
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });