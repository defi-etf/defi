import { deploy } from "./utils";

async function main() {
    await deploy('Whitelist', 'WHITELIST')
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });