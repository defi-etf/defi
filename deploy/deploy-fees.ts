import { deployProxy, getDeployConfig } from "./utils";

async function main() {
    const config = getDeployConfig()
    const fees = await deployProxy('Fees', 'FEES')
    await fees.setServiceFees(config.serviceSf, config.servicePf, config.serviceMf)
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });