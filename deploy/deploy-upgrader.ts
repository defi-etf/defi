import { UpgradeableBeaconInstance } from '../typechain-types/@openzeppelin/contracts/proxy/beacon/UpgradeableBeacon';
import { getUpgradeableBeaconFactory } from '@openzeppelin/hardhat-upgrades/dist/utils';
import hardhat from 'hardhat'
import { deploy, getDeployConfig } from "./utils";
import { getAddresses } from '../utils'

async function main() {
    const { TRADE_BEACON } = getAddresses()
    const config = getDeployConfig()
    const upgrader = await deploy('Upgrader', 'UPGRADER', [config.minUpgradePeriod, config.upgradePeriod])
    const beacon: UpgradeableBeaconInstance = (await getUpgradeableBeaconFactory(hardhat))
        .attach(TRADE_BEACON)
    await beacon.transferOwnership(upgrader.address)
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });