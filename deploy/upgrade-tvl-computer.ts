import hre, { ethers, upgrades } from 'hardhat';
import { getAddresses } from './utils';

async function main() {
	const computer = await ethers.getContractFactory('TVLComputer')
	const { TVL_COMPUTER } = getAddresses()
	const implementation = await upgrades.upgradeProxy(TVL_COMPUTER, computer)
	await hre.run("verify:verify", {
		address: implementation,
	});
	return implementation
}

main()
	.then(() => process.exit(0))
	.catch((error) => {
		console.error(error);
		process.exit(1);
	});
