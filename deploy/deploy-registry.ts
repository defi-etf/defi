import { deployProxy } from "./utils";

async function main() {
    await deployProxy('Registry', 'REGISTRY')
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });