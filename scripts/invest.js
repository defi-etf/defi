const hre = require("hardhat");
const { ethers, upgrades } = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const {
    INTERACTION, USDT, FEEDER, TRIGGER_SERVER, MANAGER_ADDRESS
} = require('../addresses.json');
const {ether} = require("@openzeppelin/test-helpers");

async function main() {
    console.log('Running script');

    // const Interaction = await hre.ethers.getContractFactory("Interaction");
    // const interaction = Interaction.attach(INTERACTION);
    //
    const Feeder = await hre.ethers.getContractFactory("Feeder");
    const feeder = Feeder.attach(FEEDER);

    // await upgrades.forceImport(FEEDER, Feeder);

    // const USDT = await hre.ethers.getContractFactory("ERC20Mock");
    // const usdt = await USDT.attach(USDT);

    // await usdt.approve(INTERACTION, ether("10000000000").toString());
    //
    // await interaction.stake("6", "50000000000000000000");


    const [owner, otherAccount] = await ethers.getSigners();
    await owner.sendTransaction({
        to: TRIGGER_SERVER,
        value: ethers.utils.parseEther("10.0"), // Sends exactly 1.0 ether
    });

    await hre.network.provider.request({
        method: "hardhat_impersonateAccount",
        params: [TRIGGER_SERVER],
    });
    const signer = await ethers.getSigner(TRIGGER_SERVER)

    // this.feeder = await upgrades.upgradeProxy(FEEDER, Feeder);

    console.log(signer.address);

    let x = await feeder.wards(signer.address);
    console.log(x);
    await feeder.connect(signer).drip(
        "325358155317505455785185414052628004928",
        "0xCBc15094f58c86cA26e9982bC920d71Fe6bc29c3",
        "0"
    );

    await hre.network.provider.request({
        method: "hardhat_stopImpersonatingAccount",
        params: [TRIGGER_SERVER],
    });
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
