const hre = require("hardhat");
const { ethers, upgrades } = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const {USDC_KOVAN,
    TRIGGER_SERVER_KOVAN,
    PROXY_OWNER_KOVAN,
    DEPLOYER,
    FUND_FACTORY} = require('../addresses-bsc.json');

async function main() {
    console.log('Running script');
    /// Deploy wETH
    const TradeFactory = await hre.ethers.getContractFactory("Trade");
    const trade = TradeFactory.attach("0xE3ba1992D645505887370Fac8198034036a2AE38");

    const isManager = await trade.managers("0x8407862Df273758816cf3De65058Ef3ECDA168E3");
    // await trade.setManager("0xDc42159B37835eaBcf7dACF4a045D9F21285dEE3", true);
    console.log(isManager);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
