const hre = require("hardhat");
const { ethers, upgrades } = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const {USDC_KOVAN,
    TRIGGER_SERVER_KOVAN,
    PROXY_OWNER_KOVAN,
    DEPLOYER,
    FUND_FACTORY} = require('../addresses-bsc.json');

async function main() {
    console.log('Running script');
    /// Deploy wETH
    const FundFactory = await hre.ethers.getContractFactory("FundFactory");
    const factory = FundFactory.attach(FUND_FACTORY);

    let rnd = ethers.Wallet.createRandom().address;
    console.log("Creating fund: " + rnd);
    let receipt = await factory.newFund(
        {
            fundId: rnd,
            fundName:'Test2',
            lockUpPeriod: 0,
            subscriptionFee: "50000000000000000",
            managementFeePeriod: 0,
            managementFee: "100000000000000000",
            performanceFeePeriod: 0,
            performanceFee: "150000000000000000",
            adminFee: 0,
            minStakingAmount: 0,
            minWithdrawAmount: 0,
            investPeriod: 60,
        });
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
