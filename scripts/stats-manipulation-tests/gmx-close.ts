import { ethers } from "hardhat";
import { getAddresses } from '../../utils';
import { receiver } from './config.json'

export const BASIS_POINTS_DIVISOR = 10000;
export const DEFAULT_SLIPPAGE_AMOUNT = 30;

const indexToken = "0x2f2a2543B76A4166549F7aaB2e75Bef0aefC5B0f"
const collateralToken = "0x2f2a2543B76A4166549F7aaB2e75Bef0aefC5B0f"
const isLong = true
const receiveCollateral = false

async function main() {
    const { GMX_POSITION_ROUTER, GMX_READER, USDT, GMX_VAULT } = getAddresses()
    const gmxPositionRouter = await ethers.getContractAt('IPositionRouter', GMX_POSITION_ROUTER)
    const gmxReader = await ethers.getContractAt('IGmxReader', GMX_READER)
    const path = receiveCollateral
        ? [collateralToken]
        : collateralToken.toLowerCase() === USDT.toLowerCase()
            ? [USDT]
            : [collateralToken, USDT]
    const signer = (await ethers.getSigners())[0]
    const position = await gmxReader.getPositions(
        GMX_VAULT,
        signer.address,
        [collateralToken],
        [indexToken],
        [isLong]   
    )
    const averagePrice = position[2]
    const priceBasisPoints = isLong
		? BASIS_POINTS_DIVISOR - DEFAULT_SLIPPAGE_AMOUNT
		: BASIS_POINTS_DIVISOR + DEFAULT_SLIPPAGE_AMOUNT;
    const price = averagePrice.mul(priceBasisPoints).div(BASIS_POINTS_DIVISOR)
    const minExecutionFee = await gmxPositionRouter.minExecutionFee()
    await gmxPositionRouter.createDecreasePosition(
        path,
        indexToken,
        0,
        position[0],
        isLong,
        receiver,
        price,
        0,
        minExecutionFee,
        false,
        ethers.constants.AddressZero,
        { value: minExecutionFee }
    )
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });