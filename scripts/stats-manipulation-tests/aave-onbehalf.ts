import { ethers } from "hardhat";
import { getAddresses } from '../../utils';
import { receiver } from './config.json'

const amount = 1000000

async function main() {
    const { AAVE_POOL, USDT } = getAddresses()
    const aavePool = await ethers.getContractAt('IPool', AAVE_POOL)
    const usdt = await ethers.getContractAt('IERC20', USDT)
    await usdt.approve(AAVE_POOL, amount)
        .then(tx => tx.wait())
    await aavePool.supply(USDT, amount, receiver, 0)
        .then(tx => tx.wait())
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });