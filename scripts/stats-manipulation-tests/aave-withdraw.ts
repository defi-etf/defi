import { ethers } from "hardhat";
import { getAddresses } from '../../utils';
import { receiver } from './config.json'

const amount = 1000000

async function main() {
    const { AAVE_POOL, USDT } = getAddresses()
    const aavePool = await ethers.getContractAt('IPool', AAVE_POOL)
    const usdt = await ethers.getContractAt('IERC20', USDT)
    const signer = (await ethers.getSigners())[0]
    await usdt.approve(AAVE_POOL, amount)
        .then(tx => tx.wait())
    await aavePool.supply(USDT, amount, signer.address, 0)
        .then(tx => tx.wait())
    await aavePool.withdraw(USDT, amount, receiver)
        .then(tx => tx.wait())
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });