import { BigNumber } from 'ethers';

const hre = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const { OPTIMISM_GOERLI } = require('../addresses.json');

async function main() {
  console.log('Running script');
  const Feeder = await hre.ethers.getContractFactory("Feeder");
  const feeder = await Feeder.attach(OPTIMISM_GOERLI.FEEDER);
  const f = feeder.filters.DepositProcessed();
  const events = await feeder.queryFilter(f, 0);

  let maxGasUsage = BigNumber.from(0);
  for (const e of events) {
    const receipt = await e.getTransactionReceipt();
    if (receipt.gasUsed.gte(maxGasUsage)) {
      maxGasUsage = receipt.gasUsed;
    }
  }

  console.log(maxGasUsage.toString());
  // 1031868 - arb
  // 1326753 - goerli
  // 1326753 * 1.15 ~ 1530000
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
