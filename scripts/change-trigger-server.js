const hre = require("hardhat");
const { ethers, upgrades } = require("hardhat");
require("@nomiclabs/hardhat-etherscan");
const {BSC} = require('../addresses.json');

async function main() {
    console.log('Running script');
    const TradeAccess = await hre.ethers.getContractFactory("TradeAccess");
    const tradeAccess = TradeAccess.attach(BSC.TRADE_ACCESS);
    await tradeAccess.setGlobalAdmin(BSC.TRIGGER_SERVER);

    const Factory = await hre.ethers.getContractFactory("FundFactory");
    const factory = Factory.attach(BSC.FUND_FACTORY);
    await factory.setTriggerServer(BSC.TRIGGER_SERVER);

    const Interaction = await hre.ethers.getContractFactory("Interaction");
    const interaction = Interaction.attach(BSC.INTERACTION);
    await interaction.setTrigger(BSC.TRIGGER_SERVER);

    console.log('Success');
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
