const hre = require("hardhat");
const { ethers, upgrades } = require("hardhat");
require("@nomiclabs/hardhat-etherscan");

const {USDC_KOVAN,
    TRIGGER_SERVER_KOVAN,
    PROXY_OWNER_KOVAN,
    DEPLOYER,
    INTERACTION,
    MANAGER_ADDRESS,
    FUND_FACTORY_KOVAN} = require('../addresses-bsc.json');
const {ether} = require("@openzeppelin/test-helpers");

async function main() {
    console.log('Running script');

    let itoken_addr = "0xb5d33B28BcD4b54A528C931d37f00ca6c4a42a0D";
    const IToken = await hre.ethers.getContractFactory("illIToken");
    const itoken = IToken.attach(itoken_addr);

    await itoken.approve(INTERACTION, ether("10000000000").toString());

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
