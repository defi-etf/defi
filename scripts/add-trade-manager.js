const hre = require("hardhat");
const { ethers, upgrades } = require("hardhat");
require("@nomiclabs/hardhat-etherscan");
const {BSC} = require('../addresses.json');

async function main() {
    console.log('Running script');
    const Trade = await hre.ethers.getContractFactory("Trade");
    const trade = Trade.attach('0xc3389ae1422d9976760EFc0cA551Ae51f77d1d9d');
    await trade.setManager(BSC.TRIGGER_SERVER, true);
    console.log('Success');
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
