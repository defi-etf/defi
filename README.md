# Deploy

```
deploy/deploy.sh [selected network]
```

# Upgrade

```
npx hardhat run --network [selected network] deploy/upgrade.ts
```