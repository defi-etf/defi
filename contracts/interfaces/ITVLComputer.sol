// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface ITVLComputer {
    event PriceFeedChanged(address token, address priceFeed);
    event GMXParamsChanged(address gmxReader, address gmxDataStore, address usdc);

    function setPriceFeeds(address[] memory tokens, address[] memory priceFeeds) external;
    function setGMXParams(address gmxReader, address gmxDataStore, address usdc) external;
    function getTVL(address trade) external view returns (uint256);
}
