// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IDripOperator {
    struct UpdateManagerRequest {
        address manager;
        uint256 share;
    }
    struct UpdateTrailingStopRequest {
        bool needToApply;
        bool managerStopEnabled;
        uint256 managerStopValue;
        bool globalStopEnabled;
        uint256 globalStopValue;
    }
    event TvlReported(uint256 fundId, uint256 tvl);
    // returns true if a report was finished
    function drip(uint256 fundId, uint256 tradeTvl) external returns (bool);
    function requestUpdateManager(uint256 fundId, address manager, uint256 share, uint256 tradeLock) external;
    function requestUpdateTrailingStop(uint256 fundId, bool managerStopEnabled, uint256 managerStopValue, bool globalStopEnabled, uint256 globalStopValue) external;
    function cancelUpdateManager(uint256 fundId) external;
    function cancelUpdateTrailingStop(uint256 fundId) external;
    function isDripInProgress(uint256 fundId) external view returns (bool);
    function isDripEnabled(uint256 fundId) external view returns (bool);
    function updateManagerRequests(uint256 fundId) external view returns (address, uint256);
}