// SPDX-License-Identifier: ISC
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/IERC20MetadataUpgradeable.sol";

import "./IFeeder.sol";
import "./IInteraction.sol";
import "./IDripOperator.sol";
import "./IFees.sol";
import "./IFeeder.sol";
import "./IWhitelist.sol";
import "./IUpgrader.sol";
import "./IFundFactory.sol";
import "./ITradeParamsUpdater.sol";
import "./aave/IPool.sol";
import "./aave/IPoolDataProvider.sol";
import "./ISubaccountRegistry.sol";
import "./ITVLComputer.sol";
import "../integration/oracles/interfaces/IPriceFeed.sol";
import "../integration/gmx-v2/IExchangeRouter.sol";
import "../integration/gmx-v2/IReader.sol";
import "../integration/gmx-v2/data/DataStore.sol";

pragma solidity ^0.8.0;

interface IRegistry {
    function triggerServer() external view returns (address);
    function usdt() external view returns (IERC20MetadataUpgradeable);
    function feeder() external view returns (IFeeder);
    function interaction() external view returns (IInteraction);
    function fees() external view returns (IFees);
    function tradeBeacon() external view returns (address);
    function dripOperator() external view returns (IDripOperator);
    function ethPriceFeed() external view returns (IPriceFeed);
    function whitelist() external view returns (IWhitelist);
    function tradeParamsUpdater() external view returns (ITradeParamsUpdater);
    function upgrader() external view returns (IUpgrader);
    function swapper() external view returns (address);
    function aavePoolDataProvider() external view returns (IPoolDataProvider);
    function aavePool() external view returns (IPool);
    function fundFactory() external view returns (IFundFactory);
    function gmxV2ExchangeRouter() external view returns (IExchangeRouter);
    function gmxV2OrderVault() external view returns (address);
    function gmxV2Router() external view returns (address);
    function gmxV2Reader() external view returns (IReader);
    function gmxV2DataStore() external view returns (DataStore);
    function subaccountRegistry() external view returns (ISubaccountRegistry);
    function tvlComputer() external view returns (ITVLComputer);
}
