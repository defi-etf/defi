// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

interface ISubaccountRegistry {
    event AccountEnabled(address indexed owner, address account);
    event AccountDisabled(address indexed owner, address account);

    function accountOwners(address subaccount) external view returns (address);
    function enableAccount(address payable account) external payable;
    function disableAccount(address account) external;
}