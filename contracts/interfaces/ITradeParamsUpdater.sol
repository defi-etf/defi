// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IDelayedExecutor.sol";

interface ITradeParamsUpdater is IDelayedExecutor {
    function lastTxs(address _destination) external view returns (uint256);
    function nearestUpdate(address _destination) external view returns (uint256);
}