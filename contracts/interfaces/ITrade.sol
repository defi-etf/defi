// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;
import "./FundState.sol";
import "../integration/gmx-v2/order/BaseOrderUtils.sol";

interface ITrade {
    /**
    * Events
    */
    event ManagerChanged(address newManager);
    event StateChanged(FundState newState);
    event OwnershipTransferred(address to);
    event WhitelistMaskUpdated(bytes _newMask);
    event AllowedServicesUpdated(uint256 _newMask);
    event TrailingStopUpdated(bool _managerStopEnabled, uint256 _managerStopValue, bool _globalStopEnabled, uint256 _globalStopValue);
    event ManagerTrailingStopMoved(uint256 _tokenRate);
    event GlobalTrailingStopMoved(uint256 _tokenRate);
    /**
    * Public
    */
    function swap(
        address tokenA,
        address tokenB,
        uint256 amountA,
        bytes memory payload
    ) external returns(uint256);

    function multiSwap(
        bytes[] calldata data
    ) external;

    function gmxV2CreateOrder(BaseOrderUtils.CreateOrderParams calldata params) external payable returns (bytes32);
    function gmxV2CancelOrder(bytes32 key) external payable;
    function gmxV2ClaimFundingFees(address[] memory markets, address[] memory tokens) external payable returns (uint256[] memory);

    function aaveSupply(address _asset, uint256 _amount) external;
    function aaveWithdraw(address _asset, uint256 _amount) external;
    function setTradingScope(bytes memory whitelistMask, uint256 serviceMask) external;
    function setAaveReferralCode(uint16 refCode) external;
    function setGmxRefCode(bytes32 _gmxRefCode) external;
    function setState(FundState newState) external;
    function chargeDebt() external;
    function isManager(address _address) external view returns (bool);
    function isOwner(address _address) external view returns (bool);
    function manager() external view returns (address);
    function owner() external view returns (address);
    function whitelistMask() external view returns (bytes memory);
    function servicesEnabled() external view returns (bool[] memory);
    function isLockedUntil(address manager) external view returns (uint256);
    /**
    * Auth
    */
    function transferToFeeder(uint256 amount) external;
    function transferOwnership(address to) external;
    function setManager(address manager) external;
    function setTrailingStop(bool managerStopEnabled, uint256 managerStopValue, bool globalStopEnabled, uint256 globalStopValue) external;
    function moveTrailingStops(uint256 newTokenRate) external;
    function triggerGlobalTrailingStop() external;
    function triggerManagerTrailingStop() external;
    function lockTrade(address manager, uint256 duringTime) external;
    function initialize(
        address owner,
        address manager,
        bytes calldata _whitelistMask,
        uint256 serviceMask,
        uint256 fundId,
        bool managerStopEnabled,
        uint256 managerStopValue,
        bool globalStopEnabled,
        uint256 globalStopValue
    ) external;
    /**
    * View
    */
    function usdtAmount() external view returns(uint256);
    function debt() external view returns(uint256);

    function getAavePositionSizes(address[] calldata _assets) external view
        returns (uint256[] memory assetPositions);

    function getAssetsSizes(address[] calldata assets) external view returns(uint256[] memory);

    function status() external view returns(FundState);

    function fundId() external view returns(uint256);

    function getGmxRefCode() external view returns(bytes32);

    function getAaveRefCode() external view returns(uint16);

    function trailingStop() external view returns (bool, uint256, uint256, bool, uint256, uint256);
}
