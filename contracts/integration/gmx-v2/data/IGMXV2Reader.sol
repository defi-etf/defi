// SPDX-License-Identifier: BUSL-1.1

pragma solidity ^0.8.0;

import "../position/Position.sol";
import "../market/Market.sol";
import "../order/Order.sol";
import "../market/MarketUtils.sol";
import "../IDataStore.sol";
import "../reader/ReaderUtils.sol";
import "../pricing/PositionPricingUtils.sol";

interface IGMXV2Reader {
    function getMarket(IDataStore dataStore, address key) external view returns (Market.Props memory);
    function getMarketBySalt(IDataStore dataStore, bytes32 salt) external view returns (Market.Props memory);
    function getPosition(IDataStore dataStore, bytes32 key) external view returns (Position.Props memory);
    function getOrder(IDataStore dataStore, bytes32 key) external view returns (Order.Props memory);
    function getPositionPnlUsd(
        IDataStore dataStore,
        Market.Props memory market,
        MarketUtils.MarketPrices memory prices,
        bytes32 positionKey,
        uint256 sizeDeltaUsd
    ) external view returns (int256, int256, uint256);
    function getAccountPositions(
        IDataStore dataStore,
        address account,
        uint256 start,
        uint256 end
    ) external view returns (Position.Props[] memory);
    function getAccountOrders(
        IDataStore dataStore,
        address account,
        uint256 start,
        uint256 end
    ) external view returns (Order.Props[] memory);
    function getPositionInfo(
        IDataStore dataStore,
        address referralStorage,
        bytes32 positionKey,
        MarketUtils.MarketPrices memory prices,
        uint256 sizeDeltaUsd,
        address uiFeeReceiver,
        bool usePositionSizeAsSizeDeltaUsd
    ) external view returns (ReaderUtils.PositionInfo memory);
    function getMarkets(IDataStore dataStore, uint256 start, uint256 end) external view returns (Market.Props[] memory);
}
