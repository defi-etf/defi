// SPDX-License-Identifier: BUSL-1.1

pragma solidity ^0.8.0;

import "./market/Market.sol";
import "./data/DataStore.sol";
// @title Reader
// @dev Library for read functions
interface IReader {
    function getMarket(DataStore dataStore, address key) external view returns (Market.Props memory);
}
