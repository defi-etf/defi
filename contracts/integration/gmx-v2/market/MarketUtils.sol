// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "./Price.sol";

library MarketUtils {
    using Price for Price.Props;

    struct MarketPrices {
        Price.Props indexTokenPrice;
        Price.Props longTokenPrice;
        Price.Props shortTokenPrice;
    }
}
