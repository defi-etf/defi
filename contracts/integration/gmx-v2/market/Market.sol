// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

library Market {
    struct Props {
        address marketToken;
        address indexToken;
        address longToken;
        address shortToken;
    }
}
