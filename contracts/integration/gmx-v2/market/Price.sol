// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

library Price {
    struct Props {
        uint256 min;
        uint256 max;
    }
}
