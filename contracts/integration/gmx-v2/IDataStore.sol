// SPDX-License-Identifier: BUSL-1.1

pragma solidity ^0.8.0;

interface IDataStore {
    function getUint(bytes32 key) external view returns (uint256);
    function setUint(bytes32 key, uint256 value) external returns (uint256);
    function removeUint(bytes32 key) external;
    function applyDeltaToUint(bytes32 key, int256 value, string memory errorMessage) external returns (uint256);
    function applyDeltaToUint(bytes32 key, uint256 value) external returns (uint256);
    function applyBoundedDeltaToUint(bytes32 key, int256 value) external returns (uint256);
    function incrementUint(bytes32 key, uint256 value) external returns (uint256);
    function decrementUint(bytes32 key, uint256 value) external returns (uint256);
}
