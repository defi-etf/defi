// SPDX-License-Identifier: BUSL-1.1

pragma solidity ^0.8.0;

library PositionUtils {
    function getPositionKey(address account, address market, address collateralToken, bool isLong) internal pure returns (bytes32) {
        bytes32 key = keccak256(abi.encode(account, market, collateralToken, isLong));
        return key;
    }
}