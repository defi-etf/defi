// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../utils/Upgradeable.sol";

contract UpgradeableMock is Upgradeable {}