// SPDX-License-Identifier: MIT

pragma solidity ^0.8.10;

import "../integration/oracles/interfaces/IPriceFeed.sol";

contract PriceFeedMock is IPriceFeed {
    int256 public answer;
    uint256 public decimals;

    constructor(uint256 _decimals) {
        decimals = _decimals;
    }

    function latestAnswer() public override view returns (int256) {
        return answer;
    }

    function setLatestAnswer(int256 _answer) public {
        answer = _answer;
    }
}
