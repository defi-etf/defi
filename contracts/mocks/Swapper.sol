// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Swapper {

    uint256 public startBlock;
    uint256 public decimals;

    address public tokenDecreasing;
    address public tokenIncreasing;
    address public tokenStable;
    address public usdt;

    uint256 stableRate;

    uint256 public rateChangeSpeed;

    constructor(
        address tokenDecreasing_,
        address tokenIncrease_,
        address tokenStable_,
        address usdt_,
        uint256 decimals_
    ) {
        startBlock = block.timestamp;

        tokenDecreasing = tokenDecreasing_;
        tokenIncreasing = tokenIncrease_;
        tokenStable = tokenStable_;
        usdt = usdt_;
        decimals = decimals_;

        stableRate = 1500000000000000000;
        rateChangeSpeed = 3306878306878; //1e18 / 604800 * 2 => 1 eth per 2 weeks
    }

    function swap(address tokenA, address tokenB, uint256 amountA) external returns(uint256) {
        if (tokenA != usdt && tokenB != usdt) {
            revert("Unsupported token");
        }
        if (tokenA == tokenB) {
            revert("Same assets!");
        }

        uint256 quote = amountA;
        if (tokenA == usdt) {
            if (tokenB == tokenStable) {
                quote = amountA * rateStable() / 10**decimals;
            }
            if (tokenB == tokenIncreasing) {
                quote = amountA * rateIncreasing() / 10**decimals;
            }
            if (tokenB == tokenDecreasing) {
                quote = amountA * rateDecreasing() / 10**decimals;
            }
            IERC20(usdt).transferFrom(msg.sender, address(this), amountA);
            IERC20(tokenB).transfer(msg.sender, quote);
        }
        if (tokenB == usdt) {
            if (tokenA == tokenStable) {
                quote = amountA * 10**decimals / rateStable();
            }
            if (tokenA == tokenIncreasing) {
                quote = amountA * 10**decimals / rateIncreasing();
            }
            if (tokenA == tokenDecreasing) {
                quote = amountA * 10**decimals / rateDecreasing();
            }
            IERC20(tokenA).transferFrom(msg.sender, address(this), amountA);
            IERC20(usdt).transfer(msg.sender, quote);
        }
        return quote;
    }

    function rate(address tokenA, address tokenB, uint256 amountA) external view returns(uint256, uint256) {
        uint256 rate = 1e18;
        uint256 stdRate = 1e18;

        if (tokenA != usdt && tokenB != usdt) {
            revert("Unsupported token");
        }
        if (tokenA == tokenB) {
            revert("Same assets!");
        }

        uint256 quote = amountA;
        if (tokenA == usdt) {
            uint256 decimalDiff = 18 - (18 - decimals);
            if (tokenB == tokenStable) {
                quote = amountA * rateStable() / 10**decimalDiff;
                stdRate = rateStable();
            }
            if (tokenB == tokenIncreasing) {
                quote = amountA * rateIncreasing() / 10**decimalDiff;
                stdRate = rateIncreasing();
            }
            if (tokenB == tokenDecreasing) {
                quote = amountA * rateDecreasing() / 10**decimalDiff;
                stdRate = rateDecreasing();
            }
            rate = quote * 1e18 / amountA;
        }
        if (tokenB == usdt) {
            if (tokenA == tokenStable) {
                quote = amountA * 1e18 / rateStable();
                stdRate = rateStable();
            }
            if (tokenA == tokenIncreasing) {
                quote = amountA * 1e18 / rateIncreasing();
                stdRate = rateIncreasing();
            }
            if (tokenA == tokenDecreasing) {
                quote = amountA * 1e18 / rateDecreasing();
                stdRate = rateDecreasing();
            }
            rate = quote * 1e18 / amountA;
        }
        return (rate, stdRate);
    }

    function setStableRate(uint256 newRate) external {
        stableRate = newRate;
    }

    function setRateChangeSpeed(uint256 newRate) external {
        rateChangeSpeed = newRate;
    }

    function resetStartBlock() external {
        startBlock = block.timestamp;
    }

    // 1 usdt worth `rateStable` of tokenStable
    function rateStable() public view returns (uint256) {
        return stableRate;
    }

    // 1 usdt worth `rateDecreasing` of tokenDecreasing
    function rateDecreasing() public view returns (uint256) {
        uint256 diff = block.timestamp - startBlock;
        return (1e18 + diff * rateChangeSpeed) * 100;
    }

    // 1 usdt worth `rateIncreasing` of tokenIncreasing
    function rateIncreasing() public view returns (uint256) {
        uint256 diff = block.timestamp - startBlock;
        return 1e20 - diff * rateChangeSpeed;
    }
}
