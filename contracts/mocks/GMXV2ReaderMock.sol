// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "../integration/gmx-v2/IReader.sol";
import "../integration/gmx-v2/data/DataStore.sol";

contract GMXV2ReaderMock is IReader {
    mapping(address => mapping(address => Market.Props)) getMarketResult;

    function stubGetMarket(address dataStore, address key, Market.Props calldata props) external {
        getMarketResult[dataStore][key] = props;
    }

    function getMarket(DataStore dataStore, address key) external view returns (Market.Props memory) {
        return getMarketResult[address(dataStore)][key];
    }
}