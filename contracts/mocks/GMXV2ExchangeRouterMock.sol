// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "../integration/gmx-v2/IExchangeRouter.sol";

contract GMXV2ExchangeRouterMock is IExchangeRouter {
    BaseOrderUtils.CreateOrderParams public createOrderParams;
    address public claimFundingFeesReceiver;

    function createOrder(BaseOrderUtils.CreateOrderParams calldata params) external payable returns (bytes32) {
        createOrderParams = params;
        return 0;
    }

    function sendWnt(address receiver, uint256 amount) external payable {}

    function sendTokens(address token, address receiver, uint256 amount) external payable {}
    
    function cancelOrder(bytes32 key) external payable {}

    function claimFundingFees(
        address[] memory markets,
        address[] memory tokens,
        address receiver
    ) external payable returns (uint256[] memory) {
        claimFundingFeesReceiver = receiver;
        return new uint256[](0);
    }
}