// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "../interfaces/IRegistry.sol";

abstract contract Upgradeable is UUPSUpgradeable, OwnableUpgradeable {
    // @address:REGISTRY
    IRegistry constant registry = address(0);
    
    function _authorizeUpgrade(address) internal override {
        address upgrader = address(registry) == address(0)
            ? address(0)
            : address(registry.upgrader());
        require(
            upgrader == address(0) ? tx.origin == owner() : msg.sender == upgrader,
            "Sender is not the upgrader"
        );
    }
}