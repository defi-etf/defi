// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "../interfaces/IRegistry.sol";
import "../interfaces/ITrade.sol";

library WhitelistUtils {
    // @address:REGISTRY
    IRegistry constant registry = address(0);

    function checkTokenAllowance(address _token, ITrade _trade) internal view {
        (uint256 index, bool found) = registry.whitelist().getTokenIndex(_token);
        require(found, "T/TF"); // forbidden token
        uint256 maskIndex = index / 8;
        uint8 tokenIndex = uint8(index % 8);
        bytes memory _whitelistMask = _trade.whitelistMask();
        require(uint8(_whitelistMask[_whitelistMask.length - maskIndex - 1]) & (1 << tokenIndex) == (1 << tokenIndex), "T/TF");
    }
}