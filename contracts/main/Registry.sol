// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";
import "../utils/Upgradeable.sol";
import "../interfaces/IRegistry.sol";

contract Registry is IRegistry, Upgradeable {
    function initialize() public initializer {
        __Ownable_init();
    }
    // @address:USDT
    IERC20MetadataUpgradeable public constant usdt = address(0);
    // @address:TRIGGER_SERVER
    address public constant triggerServer = address(0);
    // @address:FEEDER
    IFeeder public constant feeder = address(0);
    // @address:INTERACTION
    IInteraction public constant interaction = address(0);
    // @address:FEES
    IFees public constant fees = address(0);
    // @address:TRADE_BEACON
    address public constant tradeBeacon = address(0);
    // @address:DRIP_OPERATOR
    IDripOperator public constant dripOperator = address(0);
    // @address:ETH_PRICE_FEED
    IPriceFeed public constant ethPriceFeed = address(0);
    // @address:WHITELIST
    IWhitelist public constant whitelist = address(0);
    // @address:TRADE_PARAMS_UPDATER
    ITradeParamsUpdater public constant tradeParamsUpdater = address(0);
    // @address:UPGRADER
    IUpgrader public constant upgrader = address(0);
    // @address:SWAPPER
    address public constant swapper = address(0);
    // @address:AAVE_POOL_DATA_PROVIDER
    IPoolDataProvider public constant aavePoolDataProvider = address(0);
    // @address:AAVE_POOL
    IPool public constant aavePool = address(0);
    // @address:FUND_FACTORY
    IFundFactory public constant fundFactory = address(0);
    // @address:SUBACCOUNT_REGISTRY
    ISubaccountRegistry public constant subaccountRegistry = address(0);
    // @address:GMX_V2_EXCHANGE_ROUTER
    IExchangeRouter public constant gmxV2ExchangeRouter = address(0);
    // @address:GMX_V2_ORDER_VAULT
    address public constant gmxV2OrderVault = address(0);
    // @address:GMX_V2_ROUTER
    address public constant gmxV2Router = address(0);
    // @address:GMX_V2_READER
    IReader public constant gmxV2Reader = address(0);
    // @address:GMX_V2_DATASTORE
    DataStore public constant gmxV2DataStore = address(0);
    // @address:TVL_COMPUTER
    ITVLComputer public constant tvlComputer = address(0);
}
