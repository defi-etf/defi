// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "../interfaces/IFeeder.sol";
import "../interfaces/ITrade.sol";
import "../interfaces/IRegistry.sol";
import "../interfaces/IDripOperator.sol";
import "../integration/oracles/interfaces/IPriceFeed.sol";
import "../utils/Upgradeable.sol";

// @address:REGISTRY
IRegistry constant registry = address(0);
uint256 constant MAX_MANAGER_SHARE = 1e18;
uint256 constant MAX_TRAILING_STOP = 1e18;

contract DripOperator is Upgradeable, IDripOperator {

    struct ReportState {
        bool isReporting;
        uint256 tokenSupply;
        uint256 depositTvl;
        uint256 debt;
        uint256 toWithdraw;
        bool withdrawalsProcessed;
        uint256 finalTvl;
        uint256 withdrawTvl;
    }

    mapping (uint256 => ReportState) public reportState;
    mapping (uint256 => uint256) public reportDates;
    uint256 public reportDelay;
    mapping (uint256 => UpdateManagerRequest) public updateManagerRequests;
    mapping (uint256 => UpdateTrailingStopRequest) public updateTrailingStopRequests;

    function initialize(uint256 _reportDelay) public initializer {
        __Ownable_init();
        reportDelay = _reportDelay;
    }

    function drip(uint256 fundId, uint256 tradeTvl) external override returns (bool) {
        require(msg.sender == address(registry.interaction()), "DO/SNI"); // sender not interaction
        require(isDripEnabled(fundId), "DO/DNE"); // delay not expired
        IFeeder feeder = registry.feeder();
        IFeeder.FundInfo memory fund = feeder.getFund(fundId);
        if (!reportState[fundId].isReporting) {
            (uint256 toDeposit, uint256 toWithdraw, uint256 pf, uint256 ef) = feeder.pendingTvl(fundId, tradeTvl, 0);
            uint256 tvlAfterEF = tradeTvl > ef ? tradeTvl - ef : tradeTvl;
            reportState[fundId] = ReportState(
                true,
                fund.itoken.totalSupply(),
                tradeTvl - pf,
                0,
                feeder.fundTotalWithdrawals(fundId),
                false,
                tradeTvl + toDeposit - toWithdraw - pf,
                tvlAfterEF - pf
            );
            int256 toFeeder = int256(toWithdraw) + int256(pf) + int256(ef);
            int256 subtracted = toFeeder - int256(ITrade(fund.trade).usdtAmount());
            if (subtracted > 0) {
                require(uint256(subtracted) < toDeposit, "DO/NEL"); // not enough liquidity (deposits or usdt on Trade)
                toFeeder -= subtracted;
                reportState[fundId].debt = uint256(subtracted);
            }

            if (toFeeder > 0) {
                feeder.transferFromTrade(fundId, uint256(toFeeder));
            }

            feeder.gatherFees(fundId, tradeTvl, ef);
            emit TvlReported(fundId, tradeTvl);
        }
        uint256 batchSize = MAX_USERS_PER_BATCH;
        ReportState storage _reportState = reportState[fundId];
        (uint256 depositsProcessed, uint256 debtLeft) = feeder.drip(
            fundId,
            _reportState.debt,
            _reportState.tokenSupply,
            _reportState.depositTvl,
            batchSize
        );
        _reportState.debt = debtLeft;
        batchSize -= depositsProcessed;
        if (batchSize == 0) {
            return false;
        }
        if (!_reportState.withdrawalsProcessed) {
            uint256 withdrawalsProcessed = feeder.withdrawMultiple(
                fundId,
                _reportState.tokenSupply,
                _reportState.toWithdraw,
                _reportState.withdrawTvl,
                batchSize
            );
            batchSize -= withdrawalsProcessed;
            if (batchSize == 0) {
                return false;
            }
        }
        _reportState.withdrawalsProcessed = true;
        uint256 indentedWithdrawalsLeft = feeder.moveIndentedWithdrawals(fundId, batchSize);
        if (indentedWithdrawalsLeft == 0) {
            feeder.saveHWM(fundId, _reportState.finalTvl);
            reportDates[fundId] = block.timestamp;
            applyUpdateManagerRequest(fundId);
            applyUpdateTrailingStopRequest(fundId);
            getTradingContract(fundId).moveTrailingStops(feeder.tokenRate(fundId, _reportState.finalTvl));
            delete reportState[fundId];
            return true;
        } else {
            return false;
        }
    }

    function requestUpdateManager(uint256 fundId, address manager, uint256 share, uint256 tradeLock) override external {
        ITrade tradeContract = getTradingContract(fundId);
        require(tradeContract.isOwner(msg.sender), "DO/AD"); // access denied
        require(share <= MAX_MANAGER_SHARE, "DO/IMS"); // invalid managerShare
        updateManagerRequests[fundId].manager = manager;
        updateManagerRequests[fundId].share = share;
        if (tradeLock > 0) {
            tradeContract.lockTrade(tradeContract.manager(), tradeLock);
        }
    }

    function cancelUpdateManager(uint256 fundId) override external {
        ITrade trade = getTradingContract(fundId);
        require(trade.isOwner(msg.sender), "DO/AD"); // access denied
        delete updateManagerRequests[fundId];
    }

    function requestUpdateTrailingStop(uint256 fundId, bool managerStopEnabled, uint256 managerStopValue, bool globalStopEnabled, uint256 globalStopValue) external {
        ITrade tradeContract = getTradingContract(fundId);
        require(tradeContract.isOwner(msg.sender), "DO/AD");
        if (managerStopEnabled) {
            require(managerStopValue >= 0 && managerStopValue <= MAX_TRAILING_STOP, "DO/ITS"); // invalid trailing stop
        }
        if (globalStopEnabled) {
            require(globalStopValue >= 0 && globalStopValue <= MAX_TRAILING_STOP, "DO/ITS"); // invalid trailing stop
        }
        updateTrailingStopRequests[fundId].needToApply = true;
        updateTrailingStopRequests[fundId].managerStopEnabled = managerStopEnabled;
        updateTrailingStopRequests[fundId].managerStopValue = managerStopValue;
        updateTrailingStopRequests[fundId].globalStopEnabled = globalStopEnabled;
        updateTrailingStopRequests[fundId].globalStopValue = globalStopValue;
    }

    function cancelUpdateTrailingStop(uint256 fundId) override external {
        ITrade tradeContract = getTradingContract(fundId);
        require(tradeContract.isOwner(msg.sender), "DO/AD");
        delete updateTrailingStopRequests[fundId];
    }

    function isDripInProgress(uint256 fundId) external view returns (bool) {
        return reportState[fundId].isReporting;
    }

    function isDripEnabled(uint256 fundId) public override view returns (bool) {
        return reportDates[fundId] == 0 || block.timestamp > reportDates[fundId] + reportDelay;
    }

    function applyUpdateManagerRequest(uint256 fundId) private {
        IFeeder.FundInfo memory fund = registry.feeder().getFund(fundId);
        if (updateManagerRequests[fundId].manager != address(0)) {
            ITrade(fund.trade).setManager(updateManagerRequests[fundId].manager);
            registry.fees().setManagerShare(fundId, updateManagerRequests[fundId].share);
            delete updateManagerRequests[fundId];
        }
    }

    function applyUpdateTrailingStopRequest(uint256 fundId) private {
        if (!updateTrailingStopRequests[fundId].needToApply) return;
        getTradingContract(fundId).setTrailingStop(
            updateTrailingStopRequests[fundId].managerStopEnabled,
            updateTrailingStopRequests[fundId].managerStopValue,
            updateTrailingStopRequests[fundId].globalStopEnabled,
            updateTrailingStopRequests[fundId].globalStopValue
        );
        delete updateTrailingStopRequests[fundId];
    }

    function getTradingContract(uint256 fundId) private view returns (ITrade) {
        (address trade,,) = registry.interaction().fundInfo(fundId);
        require(trade != address(0), "DO/FNE"); // fund does not exist
        return ITrade(trade);
    }
}