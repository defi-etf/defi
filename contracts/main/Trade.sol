// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";

import "./../utils/TryCall.sol";
import "../utils/WhitelistUtils.sol";
import "../interfaces/ITrade.sol";
import "../interfaces/FundState.sol";
import "../interfaces/IRegistry.sol";
import "../libs/ZeroX.sol";
import "../libs/AAVE.sol";
import "../libs/GMXV2.sol";

// @address:REGISTRY
IRegistry constant registry = address(0);

contract TradeStorage is Initializable {
    mapping(address => bool) public managers; // legacy
    FundState state;
    uint16 public aaveReferralCode;
    bytes32 public gmxRefCode;
    bytes _whitelistMask;
    bool _gmxEnabled;
    bool _aaveEnabled;
    uint256 _fundId;
    uint256 _debt;
    address _manager;
    address _owner;
    mapping(address => uint256) public tradeLock;
    bool _managerTrailingStopEnabled;
    uint256 _managerTrailingStopValue;
    uint256 _managerTrailingStopTokenRate;
    bool _globalTrailingStopEnabled;
    uint256 _globalTrailingStopValue;
    uint256 _globalTrailingStopTokenRate;
    uint256[37] _gap;
}

contract Trade is ITrade, TradeStorage, ReentrancyGuardUpgradeable {
    using SafeERC20Upgradeable for IERC20Upgradeable;
    using SafeERC20Upgradeable for IERC20MetadataUpgradeable;

    modifier onlyOwner() {
        require(isOwner(msg.sender), "T/OO"); // only owner
        _;
    }

    modifier onlyManager() {
        require(isManager(msg.sender), "T/OM"); // only manager
        _;
    }

    modifier saveDebt() {
        if (msg.sender != registry.triggerServer()) {
            _;
            return;
        }
        uint256 gasLeft = gasleft();
        _;
        gasLeft = gasLeft - gasleft();
        _debt += ethToUsd(gasLeft * tx.gasprice);
    }

    modifier saveDebtPayable() {
        if (msg.sender != registry.triggerServer()) {
            _;
            return;
        }
        _;
        _debt += ethToUsd(msg.value);
    }

    modifier notExceededDebt(address token, uint256 amount) {
        if (token == address(registry.usdt())) {
            require(amount <= usdtAmount() - _debt, "T/DEB"); // debt exceeded balance
        }
        _;
    }

    modifier notTradeLocked(address tokenTo) {
        address account = _getBaseAccount(msg.sender);
        if (tokenTo == address(0) || tokenTo != address(registry.usdt())) {
            require(tradeLock[account] == 0 || tradeLock[account] < block.timestamp, "T/TL"); // trade locked
        }
        _;
    }

    function initialize(
        address owner,
        address manager,
        bytes calldata whitelistMask,
        uint256 serviceMask,
        uint256 fundId,
        bool managerStopEnabled,
        uint256 managerStopValue,
        bool globalStopEnabled,
        uint256 globalStopValue
    ) external initializer {
        __ReentrancyGuard_init();
        _fundId = fundId;
        _owner = owner;
        state = FundState.Opened;
        _managerTrailingStopEnabled = managerStopEnabled;
        _managerTrailingStopValue = managerStopValue;
        _globalTrailingStopEnabled = globalStopEnabled;
        _globalTrailingStopValue = globalStopValue;
        _setWhitelistMask(whitelistMask);
        _setServiceMask(serviceMask);
        this.setManager(manager);
    }

    function setTradingScope(bytes memory whitelistMask, uint256 serviceMask) external {
        require(msg.sender == address(registry.tradeParamsUpdater()), "T/AD"); // access denied
        require(registry.feeder().fundTotalWithdrawals(_fundId) == 0, "T/UW"); // has unprocessed withdrawals
        _setWhitelistMask(whitelistMask);
        _setServiceMask(serviceMask);
    }

    function chargeDebt() external override {
        registry.usdt().safeApprove(address(registry.fees()), _debt);
        registry.fees().gatherEf(_fundId, _debt, address(registry.usdt()));
        _debt = 0;
    }

    function setFundId(uint256 fundId) external {
        require(registry.feeder().getFund(fundId).trade == address(this), "T/WID"); // wrong fund id
        _fundId = fundId;
    }

    function usdtAmount() public view returns(uint256) {
        return registry.usdt().balanceOf(address(this));
    }

    function status() public view returns(FundState) {
        return state;
    }

    function debt() public view returns(uint256) {
        return _debt;
    }

    function transferToFeeder(uint256 amount) external {
        require(msg.sender == address(registry.feeder()), "T/AD"); // access denied
        registry.usdt().safeTransfer(address(registry.feeder()), amount);
    }

    function transferOwnership(address to) external onlyOwner {
        if (managers[to]) {
            managers[to] = false; // for back compatibility
        }
        uint256 collectedFees = registry.fees().fundBalance(_fundId);
        if (collectedFees > 0) {
            registry.fees().withdrawFund(_fundId, _owner, collectedFees);
        }
        _owner = to;
        emit OwnershipTransferred(to);
    }

    function setState(FundState newState) external override {
        require(
            msg.sender == registry.triggerServer() && (state == FundState.Blocked && newState == FundState.Opened)
            || (isOwner(msg.sender) && newState != FundState.Blocked && state != FundState.Blocked),
            "T/AD"
        ); // access denied
        require(state != newState, "T/SS"); // same state
        state = newState;
        emit StateChanged(state);
    }

    function multiSwap(bytes[] calldata data) external override {
        for (uint i; i < data.length; i++) {
            (, address tokenA, address tokenB, uint256 amountA, bytes memory payload) = abi.decode(data[i],
                (address, address, address, uint256, bytes)
            );
            swap(tokenA, tokenB, amountA, payload);
        }
    }

    function swap(
        address tokenA,
        address tokenB,
        uint256 amountA,
        bytes memory payload
    ) public override nonReentrant saveDebt notExceededDebt(tokenA, amountA) notTradeLocked(tokenB) returns(uint256) {
        return ZeroX.swap(tokenA, tokenB, amountA, payload, this);
    }

    function lockTrade(address manager, uint256 duringTime) external override {
        require(
            isOwner(msg.sender)
            || msg.sender == address(registry.dripOperator()),
            "T/AD"
        ); // access denied
        tradeLock[manager] = duringTime;
    }

    function setManager(address manager) external override {
        require(
            msg.sender == address(this)
            || msg.sender == address(registry.dripOperator()),
            "T/AD"
        ); // access denied
        if (managers[manager]) {
            if (_owner == address(0)) {
                _owner = manager;
            }
            managers[manager] = false; // for back compatibility
        }
        _manager = manager;
        emit ManagerChanged(manager);
    }

    function setTrailingStop(bool managerStopEnabled, uint256 managerStopValue, bool globalStopEnabled, uint256 globalStopValue) external {
        require(msg.sender == address(registry.dripOperator()), "T/AD");
        _managerTrailingStopEnabled = managerStopEnabled;
        _managerTrailingStopValue = managerStopValue;
        _globalTrailingStopEnabled = globalStopEnabled;
        _globalTrailingStopValue = globalStopValue;
        emit TrailingStopUpdated(_managerTrailingStopEnabled, _managerTrailingStopValue, _globalTrailingStopEnabled, _globalTrailingStopValue);
    }

    function triggerManagerTrailingStop() external {
        require(msg.sender == registry.triggerServer(), "T/AD");
        require(_managerTrailingStopEnabled, "T/SD");
        _managerTrailingStopEnabled = false;
        _managerTrailingStopValue = 0;
        _managerTrailingStopTokenRate = 0;
        tradeLock[_manager] = registry.interaction().nextPeriod(_fundId);
        emit TrailingStopUpdated(_managerTrailingStopEnabled, _managerTrailingStopValue, _globalTrailingStopEnabled, _globalTrailingStopValue);
        emit ManagerTrailingStopMoved(_managerTrailingStopTokenRate);
    }

    function triggerGlobalTrailingStop() external {
        require(msg.sender == registry.triggerServer(), "T/AD");
        require(_globalTrailingStopEnabled, "T/SD");
        _globalTrailingStopEnabled = false;
        _globalTrailingStopValue = 0;
        _globalTrailingStopTokenRate = 0;
        state = FundState.Blocked;
        emit StateChanged(state);
        emit TrailingStopUpdated(_globalTrailingStopEnabled, _globalTrailingStopValue, _globalTrailingStopEnabled, _globalTrailingStopValue);
        emit GlobalTrailingStopMoved(_globalTrailingStopTokenRate);
    }

    function moveTrailingStops(uint256 newTokenRate) external {
        require(msg.sender == address(registry.dripOperator()), "T/AD");
        if (_managerTrailingStopEnabled) {
            uint256 nextManagerTrailingStop = newTokenRate * (1e18 - _managerTrailingStopValue) / 1e18;
            if (nextManagerTrailingStop > _managerTrailingStopTokenRate) {
                _managerTrailingStopTokenRate = nextManagerTrailingStop;
                emit ManagerTrailingStopMoved(_managerTrailingStopTokenRate);
            }
        }
        if (_globalTrailingStopEnabled) {
            uint256 nextGlobalTrailingStop = newTokenRate * (1e18 - _globalTrailingStopValue) / 1e18;
            if (nextGlobalTrailingStop > _globalTrailingStopTokenRate) {
                _globalTrailingStopTokenRate = nextGlobalTrailingStop;
                emit GlobalTrailingStopMoved(_globalTrailingStopTokenRate);
            }
        }
    }

    function setAaveReferralCode(uint16 refCode) external override onlyOwner {
        aaveReferralCode = refCode;
    }

    function setGmxRefCode(bytes32 _gmxRefCode) external override onlyOwner {
        gmxRefCode = _gmxRefCode;
    }

    function aaveSupply(
        address _asset,
        uint256 _amount
    ) external override onlyManager notExceededDebt(_asset, _amount) notTradeLocked(address(0)) {
        AAVE.supply(_asset, _amount, this);
    }

    function aaveWithdraw(
        address _asset,
        uint256 _amount
    ) external override saveDebt {
        AAVE.withdraw(_asset, _amount, this);
    }

    function getAavePositionSizes(address[] calldata _assets) external view override
    returns (uint256[] memory assetPositions)
    {
        return AAVE.getPositionSizes(_assets, this);
    }

    function getAssetsSizes(address[] calldata assets) external override view returns(uint256[] memory) {
        uint256[] memory sizes = new uint256[](assets.length);

        for (uint256 i = 0; i < assets.length; i++) {
            sizes[i] = IERC20(assets[i]).balanceOf(address(this));
        }

        return sizes;
    }

    function gmxV2CreateOrder(BaseOrderUtils.CreateOrderParams calldata params)
    external payable saveDebt saveDebtPayable nonReentrant
    notExceededDebt(params.addresses.initialCollateralToken, params.numbers.initialCollateralDeltaAmount) returns (bytes32) {
        return GMXV2.createOrder(params, this);
    }

    function gmxV2CancelOrder(bytes32 key) external payable saveDebt nonReentrant {
        return GMXV2.cancelOrder(key, this);
    }

    function gmxV2ClaimFundingFees(address[] memory markets, address[] memory tokens) external payable returns (uint256[] memory) {
        return GMXV2.claimFundingFees(markets, tokens, address(this));
    }

    function _setServiceMask(uint256 _serviceMask) private {
        _gmxEnabled = _serviceMask & 1 == 1;
        _aaveEnabled = _serviceMask & 1 << 1 == 1 << 1;
        emit AllowedServicesUpdated(_serviceMask);
    }

    function _setWhitelistMask(bytes memory whitelistMask) private {
        uint256 tokenCount = registry.whitelist().tokenCount();
        if (tokenCount < whitelistMask.length * 8) {
            // cannot be more than 1 byte longer than maximum capacity
            require(whitelistMask.length * 8 - tokenCount < 8, "T/UT");
            bytes1 lastByte = whitelistMask[0];
            // mask that allows all tokens of the last byte of whitelistMask
            bytes1 allowedTokensMask = bytes1(uint8((1 << tokenCount % 8) - 1));
            require(lastByte | allowedTokensMask == allowedTokensMask, "T/UT");
        }
        _whitelistMask = whitelistMask;
        emit WhitelistMaskUpdated(_whitelistMask);
    }

    function whitelistMask() external view returns (bytes memory) {
        return _whitelistMask;
    }

    function fundId() external view returns (uint256) {
        return _fundId;
    }

    function manager() external view returns (address) {
        return _manager;
    }

    function owner() external view returns (address) {
        return _owner;
    }

    function servicesEnabled() external view returns (bool[] memory) {
        bool[] memory result = new bool[](2);
        result[0] = _gmxEnabled;
        result[1] = _aaveEnabled;
        return result;
    }

    function isManager(address _address) public view returns (bool) {
        address account = _getBaseAccount(_address);
        return managers[account] || account == _manager;
    }

    function isOwner(address _address) public view returns (bool) {
        return managers[_address] || _address == _owner;
    }

    function getGmxRefCode() public view returns (bytes32) {
        return gmxRefCode;
    }

    function getAaveRefCode() external view returns(uint16) {
        return aaveReferralCode;
    }

    function isLockedUntil(address manager) external view returns (uint256) {
        return tradeLock[_getBaseAccount(manager)];
	}

    function _getBaseAccount(address _address) private view returns(address) {
        address accountOwner = registry.subaccountRegistry().accountOwners(_address);
        return accountOwner == address(0) ? _address : accountOwner;
    }

    function ethToUsd(uint256 eth) private view returns (uint256) {
        IPriceFeed ethPriceFeed = registry.ethPriceFeed();
        return eth
            / 10**uint256(ethPriceFeed.decimals())
            * 10**(registry.usdt()).decimals()
            * uint256(ethPriceFeed.latestAnswer())
            / 10**18;
    }

    function trailingStop() external view returns (bool, uint256, uint256, bool, uint256, uint256) {
        return (
            _managerTrailingStopEnabled,
            _managerTrailingStopValue,
            _managerTrailingStopTokenRate,
            _globalTrailingStopEnabled,
            _globalTrailingStopValue,
            _globalTrailingStopTokenRate
        );
    }
}
