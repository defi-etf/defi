// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "hardhat/console.sol";

import "../utils/Upgradeable.sol";
import "../interfaces/IWhitelist.sol";
import "../interfaces/IRegistry.sol";
import "../interfaces/ITrade.sol";
import "../interfaces/ITVLComputer.sol";
import "../integration/gmx-v2/data/IGMXV2Reader.sol";
import "../integration/gmx-v2/position/Position.sol";
import "../integration/gmx-v2/order/Order.sol";
import "../integration/gmx-v2/IDataStore.sol";
import "../integration/gmx-v2/position/PositionUtils.sol";

// @address:REGISTRY
IRegistry constant registry = address(0);
uint256 constant gmxPrecision = 30;

contract TVLComputer is Upgradeable, ITVLComputer {
    using Position for Position.Props;
    using Order for Order.Props;

    address public gmxReader;
    address public gmxDataStore;
    address public usdc;
    mapping(address => address) public priceFeeds;
    mapping(address => bool) public syntheticTokens;
    mapping(address => uint256) public syntheticTokensDecimals;

    function initialize() public initializer {
        __Ownable_init();
    }

    function setPriceFeeds(address[] memory tokens, address[] memory priceFeeds) public onlyOwner {
        require(tokens.length == priceFeeds.length, "TC/II"); // invalid input

        for (uint256 i = 0; i < tokens.length; i++) {
            setPriceFeed(tokens[i], priceFeeds[i]);
        }
    }

    function setSyntheticTokens(address[] memory tokens, uint256[] memory decimals) public onlyOwner {
        for (uint256 i = 0; i < tokens.length; i++) {
            syntheticTokens[tokens[i]] = true;
            syntheticTokensDecimals[tokens[i]] = decimals[i];
        }
    }

    function setGMXParams(address _gmxReader, address _gmxDataStore, address _usdc) public onlyOwner {
        gmxReader = _gmxReader;
        gmxDataStore = _gmxDataStore;
        usdc = _usdc;
        emit GMXParamsChanged(gmxReader, gmxDataStore, usdc);
    }

    function getTVL(address trade) public view returns (uint256 balance) {
        balance = ITrade(trade).usdtAmount();
        bool[] memory indexes = convertToBoolArray(ITrade(trade).whitelistMask());
        for (uint256 i = 0; i < indexes.length; i++) {
            if (!indexes[i]) continue;
            address token = IWhitelist(address(registry.whitelist())).tokens(i);
            if (token == address(0) || syntheticTokens[token] == true) continue;
            uint256 tokenBalance = IERC20(token).balanceOf(trade);
            tokenBalance += getAaveTokenValue(token, trade);
            balance += convertToUsdt(token, tokenBalance);
        }
        balance += getAaveTokenValue(address(registry.usdt()), trade);
        if (gmxReader != address(0)) {
            balance += getGMXValue(trade);
        }
        if (ITrade(trade).debt() >= balance) {
            balance = 0;
        } else {
            balance -= ITrade(trade).debt();
        }
    }

    function getGMXValue(address trade) public view returns (uint256) {
        require(gmxReader != address(0), "TC/NGP"); // no GMX params set
        IGMXV2Reader reader = IGMXV2Reader(gmxReader);
        IDataStore dataStore = IDataStore(gmxDataStore);

        Position.Props[] memory positions = reader.getAccountPositions(dataStore, trade, 0, 100);
        uint256 tvl = 0;

        for (uint256 i = 0; i < positions.length; i++) {
            Market.Props memory market = reader.getMarket(dataStore, positions[i].addresses.market);
            Price.Props memory indexPrice = Price.Props(getGMXOraclePrice(market.indexToken), getGMXOraclePrice(market.indexToken));
            Price.Props memory longPrice = Price.Props(getGMXOraclePrice(market.longToken), getGMXOraclePrice(market.longToken));
            Price.Props memory shortPrice = Price.Props(getGMXOraclePrice(market.shortToken), getGMXOraclePrice(market.shortToken));
            MarketUtils.MarketPrices memory prices = MarketUtils.MarketPrices(indexPrice, longPrice, shortPrice);
            bytes32 positionKey = PositionUtils.getPositionKey(trade, positions[i].addresses.market, positions[i].addresses.collateralToken, positions[i].flags.isLong);
            ReaderUtils.PositionInfo memory positionInfo = reader.getPositionInfo(
                dataStore,
                0xe6fab3F0c7199b0d34d7FbE83394fc0e0D06e99d,
                positionKey,
                prices,
                0,
                address(0),
                true
            );
            int256 pnl = positionInfo.basePnlUsd - int256(convertToGMXUsd(positions[i].addresses.collateralToken, positionInfo.fees.totalCostAmount));
            int256 netValue = int256(convertToGMXUsd(positions[i].addresses.collateralToken, positions[i].numbers.collateralAmount)) + pnl;
            if (netValue < 0) netValue = 0;
            tvl += uint256(netValue);
        }

        Order.Props[] memory orders = reader.getAccountOrders(dataStore, trade, 0, 100);
        for (uint256 i = 0; i < orders.length; i++) {
            if (
                orders[i].numbers.orderType != Order.OrderType.MarketIncrease &&
                orders[i].numbers.orderType != Order.OrderType.LimitIncrease
            ) continue;
            tvl += convertToGMXUsd(orders[i].addresses.initialCollateralToken, orders[i].numbers.initialCollateralDeltaAmount);
        }

        return tvl / (10 ** (gmxPrecision - registry.usdt().decimals()));
    }

    function getAaveTokenValue(address token, address trade) private view returns (uint256) {
        if (address(registry.aavePoolDataProvider()) == address(0)) { return 0; }
        DataTypes.ReserveData memory reserveData = registry.aavePool().getReserveData(token);
        if (reserveData.aTokenAddress == address(0)) { return 0; }
        (uint256 aaveBalance,,,,,,,,) = registry.aavePoolDataProvider().getUserReserveData(token, trade);
        return aaveBalance;
    }

    function convertToBoolArray(bytes memory mask) private pure returns (bool[] memory) {
        bool[] memory booleanArray = new bool[](mask.length * 8);
        uint256 index = 0;
        for (int256 i = int256(mask.length) - 1; i >= 0; i--) {
            uint8 byteValue = uint8(mask[uint256(i)]);
            for (uint8 j = 0; j < 8; j++) {
                booleanArray[index] = (byteValue & (1 << j)) != 0;
                index++;
            }
    }
        return booleanArray;
    }

    function setPriceFeed(address token, address priceFeed) private onlyOwner {
        priceFeeds[token] = priceFeed;
        emit PriceFeedChanged(token, priceFeed);
    }

    function getPositionKey(address account, address market, address collateralToken, bool isLong) private pure returns (bytes32) {
        return keccak256(abi.encode(account, market, collateralToken, isLong));
    }

    function convertToUsdt(address token, uint256 amount) private view returns (uint256) {
        uint256 tokenDecimals = getTokenDecimals(token);
        uint256 usdtDecimals = registry.usdt().decimals();
        return amount * getPrice(token, usdtDecimals) / 10 ** (tokenDecimals);
    }

    function convertToGMXUsd(address token, uint256 amount) private view returns (uint256) {
        return amount * getGMXOraclePrice(token);
    }

    function getPrice(address token, uint256 decimals) private view returns (uint256) {
        if (token == address(registry.usdt())) {
            return 10 ** decimals;
        }
        require(priceFeeds[token] != address(0), "TC/EF"); // empty feed
        return uint256(IPriceFeed(priceFeeds[token]).latestAnswer())
            * 10 ** (decimals)
            / 10 ** uint256(IPriceFeed(priceFeeds[token]).decimals());
    }

    function getGMXOraclePrice(address token) private view returns (uint256) {
        return getPrice(token, gmxPrecision)
            / 10 ** getTokenDecimals(token);
    }

    function getTokenDecimals(address token) private view returns (uint256) {
        return syntheticTokens[token] ? syntheticTokensDecimals[token] : IERC20MetadataUpgradeable(token).decimals(); 
    }
}
