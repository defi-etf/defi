// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "../utils/InvestPeriod.sol";
import "../interfaces/IFeeder.sol";
import "../interfaces/IFees.sol";
import "../interfaces/ITrade.sol";
import "../interfaces/IInteraction.sol";
import "../interfaces/IRegistry.sol";
import "../interfaces/FundState.sol";
import "../interfaces/IDripOperator.sol";
import "../interfaces/IUpgrader.sol";
import "../interfaces/ITradeParamsUpdater.sol";

import "../utils/Upgradeable.sol";

// @address:REGISTRY
IRegistry constant registry = address(0);

contract Interaction is Upgradeable, IInteraction {
    using SafeERC20Upgradeable for IERC20Upgradeable;
    uint256 public periodLength;
    mapping (uint256 => FundInfo) public funds;
    // Fund Id => amount
    mapping (uint256 => uint256) public deposits;
    mapping (uint256 => uint256) public withdrawals;
    InvestPeriod investPeriodUtils;

    function initialize(address _investPeriodUtils) public initializer {
        __Ownable_init();
        investPeriodUtils = InvestPeriod(_investPeriodUtils);
    }

    modifier noDripInProgress(uint256 fundId) {
        require(!registry.dripOperator().isDripInProgress(fundId), "I/DIP");
        _;
    }

    function setInvestPeriodUtils(address _investPeriodUtils) external onlyOwner {
        investPeriodUtils = InvestPeriod(_investPeriodUtils);
    }

    function newFund(
        uint256 fundId,
        bool hwm,
        uint256 investPeriod,
        address manager,
        IToken itoken,
        address tradeContract,
        uint256 indent,
        bool isPrivate
    ) external override {
        require(msg.sender == address(registry.fundFactory()), "I/AD"); // access denied
        require(!fundExist(fundId), "I/FE"); //fund exist
        registry.feeder().newFund(fundId, manager, itoken, tradeContract, hwm);

        funds[fundId] = FundInfo({
            trade: tradeContract,
            period: investPeriod,
            nextPeriod: 0,
            itoken: itoken,
            indent: indent,
            hwm: hwm,
            isPrivate: isPrivate
        });

        emit NewFund(fundId, manager, address(itoken));
    }

    function setIsPrivate(uint256 fundId, bool isPrivate) external override {
        require(fundExist(fundId), "I/FNE"); // fund not exist
        require(ITrade(funds[fundId].trade).isOwner(msg.sender), "I/AD"); // access denied
        require(isPrivate != funds[fundId].isPrivate, "I/IC"); // invalid change
        funds[fundId].isPrivate = isPrivate;
        emit PrivacyChanged(fundId, isPrivate);
    }

    // @notice Stake user's USDC and return shares amount
    function stake(uint256 fundId, uint256 amount) external override noDripInProgress(fundId) returns (uint256) {
        require(fundExist(fundId), "I/FNE");//fund NOT exist
        require(ITrade(funds[fundId].trade).status() == FundState.Opened, "I/FC"); // fund closed
        if (funds[fundId].isPrivate) {
            require(ITrade(funds[fundId].trade).isOwner(msg.sender), "I/FIP"); // fund is private
        }

        deposits[fundId] += amount;

        IERC20Upgradeable usdt = registry.usdt();
        IFeeder feeder = registry.feeder();
        usdt.safeTransferFrom(msg.sender, address(feeder), amount);
        uint256 staked = feeder.stake(fundId, msg.sender, amount);

        emit Stake(fundId, msg.sender, staked, amount, amount - staked);

        return amount;
    }

    // @notice Put a request for withdrawing funds from the contract
    // Funds will be withdraw on the next vesting period
    // `amount` is amount of shares(i.e. ITKN)
    // If fund is closed, funds will be withdrawn immediately
    function unstake(uint256 fundId, uint256 amount) external override noDripInProgress(fundId) {
        require(fundExist(fundId), "I/FNE");
        require(funds[fundId].itoken.balanceOf(msg.sender) >= amount, "I/LE");
        uint256 nextUpgrade = registry.upgrader().nextUpgradeDate();
        require(nextUpgrade == 0 || nextUpgrade > block.timestamp, "I/UIP"); // upgrade in progress
        ITradeParamsUpdater tradeParamsUpdater = registry.tradeParamsUpdater();
        uint256 nextParamsUpdate = tradeParamsUpdater.nearestUpdate(funds[fundId].trade);
        if (nextParamsUpdate != 0 && nextParamsUpdate <= block.timestamp) {
            tradeParamsUpdater.executeTx(tradeParamsUpdater.lastTxs(funds[fundId].trade));
        }
        funds[fundId].itoken.burnFrom(msg.sender, amount);
        registry.feeder().requestWithdrawal(
            fundId,
            msg.sender, 
            amount,
            funds[fundId].indent > 0 &&
            this.nextPeriod(fundId) - funds[fundId].indent < block.timestamp &&
            (nextUpgrade == 0 || nextUpgrade > this.nextPeriod(fundId)) &&
            (nextParamsUpdate == 0 || nextParamsUpdate > this.nextPeriod(fundId)) &&
            ITrade(funds[fundId].trade).status() == FundState.Opened
        );

        emit UnStake(fundId, msg.sender, amount, funds[fundId].itoken.balanceOf(msg.sender));
    }

    function cancelDeposit(uint256 fundId) external override noDripInProgress(fundId) {
        registry.feeder().cancelDeposit(fundId, msg.sender);
    }

    function cancelWithdraw(uint256 fundId) external override noDripInProgress(fundId) {
        uint256 tokens = registry.feeder().cancelWithdrawal(fundId, msg.sender);

        funds[fundId].itoken.mint(msg.sender, tokens);
    }

    function drip(uint256 fundId, uint256 tradeTvl) external override {
        require(msg.sender == address(registry.triggerServer()), "I/AD"); // access denied
        require(fundExist(fundId), "I/FNE");
        if (registry.dripOperator().drip(fundId, tradeTvl)) {
            emit NextPeriod(fundId, this.nextPeriod(fundId));
        }
    }

    function isDripEnabled(uint256 fundId) external override view returns(bool) {
        return registry.dripOperator().isDripEnabled(fundId);
    }

    function fundExist(uint256 fundId) public override view returns(bool) {
        return funds[fundId].trade != address(0);
    }

    function tokenForFund(uint256 fundId) public override view returns (address) {
        require(fundExist(fundId), "I/FNE");
        return address(funds[fundId].itoken);
    }

    function stakers(uint256 fundId) public override view returns (uint256) {
        IToken itoken = IToken(tokenForFund(fundId));
        return itoken.holders();
    }

    function estimatedWithdrawAmount(uint256 fundId, uint256 tradeTvl, uint256 amount) public override view returns (uint256) {
        IFeeder feeder = registry.feeder();
        uint256 pf = feeder.calculatePf(fundId, tradeTvl);
        return amount * feeder.tokenRate(fundId, tradeTvl - pf) / 10**18;
    }

    ///@return FundAddress, Investors amount, Next period ts
    function fundInfo(uint256 fundId) external override view returns (address, uint256, uint256) {
        return (
            funds[fundId].trade,
            stakers(fundId),
            this.nextPeriod(fundId)
        );
    }

    function isPrivate(uint256 fundId) external view returns (bool) {
        return funds[fundId].isPrivate;
    }

    function tokenRate(uint256 fundId, uint256 tradeTvl) public override view returns (uint256) {
        return registry.feeder().tokenRate(fundId, tradeTvl);
    }

    function hwmValue(uint256 fundId) public override view returns (uint256) {
        return registry.feeder().hwmValue(fundId);
    }

    function userTVL(uint256 fundId, uint256 tradeTvl, address user) external override view returns (uint256) {
        return funds[fundId].itoken.balanceOf(user) * tokenRate(fundId, tradeTvl) / 10**18;
    }

    // @notice Get fund metrics for the current reporting period
    // @return Returns pending amounts to withdraw and deposit, as well as pending PF and USDT amount to close
    function pendingTvl(uint256[] calldata _funds, uint256[] calldata _tradeTvls, uint256 gasPrice) public override view returns(
        FundPendingTvlInfo[] memory results
    ) {
        IFeeder feeder = registry.feeder();
        results = new FundPendingTvlInfo[](_funds.length);
        for (uint256 i = 0; i < _funds.length; i++) {
            (uint256 toDeposit, uint256 toWithdraw, uint256 pf, uint256 ef) = feeder.pendingTvl(_funds[i], _tradeTvls[i], gasPrice);
            int256 diff = int256(registry.usdt().balanceOf(funds[_funds[i]].trade))
                + int256(toDeposit)
                - int256(toWithdraw)
                - int256(pf)
                - int256(ef);
            results[i] = FundPendingTvlInfo(
                toDeposit,
                toWithdraw,
                pf,
                diff < 0 ? uint256(-diff) : 0,
                this.totalFees(_funds[i]),
                stakers(_funds[i]),
                feeder.fundTotalWithdrawals(_funds[i])
            );
        }
        return results;
    }

    function tokenSupply(uint256 fundId) public override view returns (uint256) {
        return funds[fundId].itoken.totalSupply();
    }

    function nextPeriod(uint256 fundId) external view override returns (uint256) {
        return investPeriodUtils.getNextPeriodDate(funds[fundId].period, block.timestamp);
    }

    function userTokensAmount(uint256 fundId, address user) external override view returns (uint256) {
        return funds[fundId].itoken.balanceOf(user);
    }

    function totalFees(uint256 fundId) external view returns (uint256) {
        (uint256 live, uint256 sf, uint256 pf, uint256 mf) = registry.fees().gatheredFees(fundId);
        return sf + pf + mf;
    }

    function pendingDepositAndWithdrawals(uint256 fundId, address user) external view override returns (uint256, uint256, uint256){
        return registry.feeder().getUserAccrual(fundId, user);
    }
}
