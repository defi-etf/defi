// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

import "@openzeppelin/contracts/proxy/ERC1967/ERC1967Proxy.sol";
import "@openzeppelin/contracts/proxy/beacon/BeaconProxy.sol";

import "../interfaces/IInteraction.sol";
import "../interfaces/IFundFactory.sol";
import "../interfaces/IFees.sol";
import "../interfaces/ITrade.sol";
import "../interfaces/IRegistry.sol";

import "../utils/InvestPeriod.sol";
import "../utils/Upgradeable.sol";
import "./Token.sol";

contract FundFactory is Initializable, Upgradeable, IFundFactory {
    uint256 constant day = 24 * 60 * 60;
    uint256 public createFundFee;

    function initialize() public initializer {
        __Ownable_init();
        createFundFee = 0;
    }

    function setCreateFundFee(uint256 createFundFee_) external onlyOwner {
        createFundFee = createFundFee_;
    }

    function newFund(FundInfo calldata fundInfo) public override returns (uint256) {
        _checkIndent(fundInfo.numbers.indent, fundInfo.numbers.investPeriod);
        BeaconProxy trade = new BeaconProxy(registry.tradeBeacon(), new bytes(0));
        ITrade(address(trade)).initialize(
            msg.sender,
            fundInfo.manager,
            fundInfo.whitelistMask,
            fundInfo.numbers.serviceMask,
            fundInfo.id,
            fundInfo.flags.managerStopEnabled,
            fundInfo.numbers.managerStopValue,
            fundInfo.flags.globalStopEnabled,
            fundInfo.numbers.globalStopValue
        );
        registry.interaction().newFund(
            fundInfo.id,
            fundInfo.flags.hwm,
            fundInfo.numbers.investPeriod,
            msg.sender,
            new Token(fundInfo.id),
            address(trade),
            fundInfo.numbers.indent,
            fundInfo.flags.isPrivate
        );
        _collectFees(fundInfo);
        emit FundCreated(
            msg.sender,
            fundInfo.id,
            fundInfo.flags.hwm,
            fundInfo.numbers.subscriptionFee,
            fundInfo.numbers.performanceFee,
            fundInfo.numbers.managementFee,
            fundInfo.numbers.investPeriod,
            fundInfo.whitelistMask,
            fundInfo.numbers.serviceMask
        );
        return fundInfo.id;
    }

    function _collectFees(FundInfo calldata fundInfo) private {
        registry.fees().newFund(
            fundInfo.id,
            fundInfo.numbers.subscriptionFee, 
            fundInfo.numbers.performanceFee,
            fundInfo.numbers.managementFee,
            fundInfo.numbers.managerShare
        );
        registry.fees().gatherCf(fundInfo.id, msg.sender, createFundFee, address(registry.usdt()));
    }

    function _checkIndent(uint256 _indent, uint256 _investPeriod) private {
        uint maxIndent = day * (_investPeriod <= day * 7 && block.chainid != 420 ? 3 : 7);
        require(_indent <= maxIndent, "FF/ITS");
    }
}
