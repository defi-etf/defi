// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "../interfaces/ISubaccountRegistry.sol";
import "../utils/Upgradeable.sol";

contract SubaccountRegistry is ISubaccountRegistry, Upgradeable {
    mapping(address => address) public accountOwners;
    
    function enableAccount(address payable account) external payable {
        require(accountOwners[account] == address(0), "SR/AAE"); // account already enabled
        accountOwners[account] = msg.sender;
        account.transfer(msg.value);
        emit AccountEnabled(msg.sender, account);
    }

    function disableAccount(address account) external {
        require(accountOwners[account] == msg.sender, "SR/AD"); // access denied;
        accountOwners[account] = address(0); 
        emit AccountDisabled(msg.sender, account);
    }
}