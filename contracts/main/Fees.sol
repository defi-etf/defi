// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/Initializable.sol";
import "@openzeppelin/contracts-upgradeable/proxy/utils/UUPSUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";

import "../interfaces/IFees.sol";
import "../interfaces/IFeeder.sol";
import "../interfaces/IRegistry.sol";
import "../interfaces/ITrade.sol";
import "../utils/Upgradeable.sol";

uint256 constant MAX_MANAGER_SHARE = 1e18;
uint256 constant MAX_SF = 50000000000000000;
uint256 constant MAX_PF = 500000000000000000;

// @address:REGISTRY
IRegistry constant registry = 0x0000000000000000000000000000000000000000;

contract Fees is Initializable, Upgradeable, IFees {
    using SafeERC20Upgradeable for IERC20Upgradeable;
    using SafeERC20Upgradeable for IERC20MetadataUpgradeable;

    mapping (uint256 => FundFees) public funds; // fundId => fees
    mapping (uint256 => FundFees) public gatheredFees;
    mapping (uint256 => uint256) public fundBalance;

    uint256 public gatheredServiceFees;

    uint256 public serviceSf;
    uint256 public servicePf;
    uint256 public serviceMf;
    mapping (uint256 => uint256) public managerShare;
    mapping (address => uint256) public managerBalance;

    modifier feederOnly() {
        require(msg.sender == address(registry.feeder()), "FE/AD"); // access denied
        _;
    }

    modifier factoryOnly() {
        require(msg.sender == address(registry.fundFactory()), "FE/AD"); // access denied
        _;
    }

    function initialize() public initializer {
        __Ownable_init();
    }

    function setServiceFees(uint256 sf, uint256 pf, uint256 mf) external onlyOwner {
        require(sf <= MAX_SF, "FE/IS"); // invalid sf
        require(pf <= MAX_PF, "FE/IP"); // invalid pf
        serviceSf = sf;
        servicePf = pf;
        serviceMf = mf;

        emit ServiceFeesChanged(serviceSf, servicePf, serviceMf);
    }

    function newFund(uint256 fundId, uint256 sf, uint256 pf, uint256 mf, uint256 _managerShare) external override factoryOnly {
        require(funds[fundId].live == 0, "FE/FE"); // fund exists
        require(sf <= MAX_SF, "FE/IS"); // invalid sf
        require(pf <= MAX_PF, "FE/IP"); // invalid pf
        require(_managerShare <= MAX_MANAGER_SHARE, "FE/IMS"); // invalid managerShare

        funds[fundId] = FundFees({
            live: 1,
            sf: sf,
            pf: pf,
            mf: mf
        });
        managerShare[fundId] = _managerShare;

        emit NewFund(fundId, sf, pf, mf);
    }

    function setManagerShare(uint256 fundId, uint256 share) external override {
        require(msg.sender == address(registry.dripOperator()), "FE/AD"); // access denied
        require(funds[fundId].live == 1, "FE/FNE"); // fund doesn't exist
        (address trade,,) = registry.interaction().fundInfo(fundId);
        managerShare[fundId] = share;
        emit ManagerShareChanged(fundId, share);
    }

    function fees(uint256 fundId) external override view returns(uint256 sf, uint256 pf, uint256 mf) {
        require(funds[fundId].live == 1, "FE/FNE"); // fund doesn't exist

        return (funds[fundId].sf, funds[fundId].pf, funds[fundId].mf);
    }

    function serviceFees() external override view returns(uint256 sf, uint256 pf, uint256 mf) {
        return (serviceSf, servicePf, serviceMf);
    }

    // Must be called from the feeder
    function gatherSf(uint256 fundId, uint256 pending, address token) external override feederOnly returns(uint256) {
        require(funds[fundId].live == 1, "FE/FNE");
        uint256 totalSf = (funds[fundId].sf + serviceSf) * pending / 1e18;
        uint256 fundShare = _distributeFees(fundId, token, pending, totalSf, serviceSf);

        FundFees storage fundFees = gatheredFees[fundId];
        fundFees.sf += fundShare;

        emit SfCharged(fundId, totalSf);

        return pending - totalSf;
    }

    function gatherPf(uint256 fundId, uint256 pending, address token) external override feederOnly {
        require(funds[fundId].live == 1, "FE/FNE");
        uint256 totalPf = this.calculatePF(fundId, pending);
        uint256 fundShare = _distributeFees(fundId, token, pending, totalPf, servicePf);

        FundFees storage fundFees = gatheredFees[fundId];
        fundFees.pf += fundShare;

        emit PfCharged(fundId, totalPf);
    }

    function gatherEf(uint256 fundId, uint256 amount, address token) external override {
        require(funds[fundId].live == 1, "FE/FNE");
        (address trade,,) = registry.interaction().fundInfo(fundId);
        require(msg.sender == address(registry.feeder()) || msg.sender == trade, "FE/AD");
        gatheredServiceFees += amount;
        IERC20Upgradeable(token).safeTransferFrom(msg.sender, address(this), amount);
        emit EfCharged(fundId, amount);
    }

    // create fund fee, we consider it as execution fee
    function gatherCf(uint256 fundId, address payer, uint256 amount, address token) external override factoryOnly {
        require(funds[fundId].live == 1, "FE/FNE");
        gatheredServiceFees += amount;
        IERC20Upgradeable(token).safeTransferFrom(payer, address(this), amount);
        emit EfCharged(fundId, amount);
    }

    function withdraw(address user, uint256 amount) external onlyOwner {
        require(amount > 0, "FE/IA"); // invalid amount
        require(gatheredServiceFees >= amount, "FE/AEB"); // amount exceeds balance
        gatheredServiceFees -= amount;
        registry.usdt().safeTransfer(user, amount);
        emit Withdrawal(user, address(registry.usdt()), amount);
    }

    function withdrawFund(uint256 fundId, address destination, uint256 amount) external {
        require(funds[fundId].live == 1, "FE/FNE");
        require(amount > 0, "FE/IA"); // invalid amount
        (address trade,,) = registry.interaction().fundInfo(fundId);
        require(ITrade(trade).isOwner(msg.sender) || msg.sender == trade, "FE/SNO"); // sender not owner
        require(fundBalance[fundId] >= amount, "FE/AEB");
        fundBalance[fundId] -= amount;
        registry.usdt().safeTransfer(destination, amount);
        emit WithdrawalFund(fundId, destination, address(registry.usdt()), amount);
    }

    function withdrawManager(address destination, uint256 amount) external {
        require(amount > 0, "FE/IA"); // invalid amount
        require(managerBalance[msg.sender] >= amount, "FE/AEB"); // amount exceeds balance
        managerBalance[msg.sender] -= amount;
        registry.usdt().safeTransfer(destination, amount);
        emit WithdrawalManager(msg.sender, destination, address(registry.usdt()), amount);
    }

    function withdrawAll(uint256[] memory fundIds, address destination) external {
        uint256[] memory ownerWithdrawnAmounts = new uint256[](fundIds.length);
        uint256 amount = 0;
        address manager = msg.sender;
        for (uint256 i = 0; i < fundIds.length; i++) {
            uint256 fundId = fundIds[i];
            require(funds[fundId].live == 1, "FE/FNE");
            (address trade,,) = registry.interaction().fundInfo(fundId);
            require(ITrade(trade).isOwner(manager), "FE/AD"); // sender not owner
            ownerWithdrawnAmounts[i] = fundBalance[fundId];
            amount += fundBalance[fundId];
            fundBalance[fundId] = 0;
        }
        uint256 managerWithdrawnAmount = managerBalance[manager];
        amount += managerWithdrawnAmount;
        managerBalance[manager] = 0;
        if (amount == 0) return;
        registry.usdt().safeTransfer(destination, amount);
        emit WithdrawalManager(manager, destination, address(registry.usdt()), managerWithdrawnAmount);
        for (uint256 i = 0; i < fundIds.length; i++) {
            emit WithdrawalFund(fundIds[i], destination, address(registry.usdt()), ownerWithdrawnAmounts[i]);   
        }
    }

    function totalFees(uint256[] memory fundIds, address manager) external override view returns (uint256) {
        uint256 amount = managerBalance[manager];
        for (uint256 i = 0; i < fundIds.length; i++) {
            uint256 fundId = fundIds[i];
            (address trade,,) = registry.interaction().fundInfo(fundId);
            if (ITrade(trade).isOwner(manager)) {
                amount += fundBalance[fundId];
            }
        }
        return amount;
    }

    function calculatePF(uint256 fundId, uint256 amount) external override view returns (uint256) {
        return (funds[fundId].pf + servicePf) * amount / 1e18;
    }

    function _distributeFees(uint256 fundId, address token, uint256 tvlIncrement, uint256 totalFee, uint256 serviceShare) private returns (uint256) {
        (address trade,,) = registry.interaction().fundInfo(fundId);
        uint256 serviceFee = serviceShare * tvlIncrement / 1e18;
        gatheredServiceFees += serviceFee;
        uint256 fundFee = totalFee - serviceFee;
        uint256 managerFee = fundFee * managerShare[fundId] / 1e18;
        uint256 ownerFee = fundFee - managerFee;
        fundBalance[fundId] += ownerFee;
        address manager = ITrade(trade).manager();
        managerBalance[ITrade(trade).manager()] += managerFee;
        address owner = ITrade(trade).owner();
        IERC20Upgradeable(token).safeTransferFrom(msg.sender, address(this), totalFee);
        emit FeeEarned(manager, managerFee);
        emit FeeEarned(owner, ownerFee);
        return fundFee;
    }
}
