// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./DelayedExecutor.sol";
import "../interfaces/ITrade.sol";
import "../interfaces/IRegistry.sol";
import "../interfaces/ITradeParamsUpdater.sol";

contract TradeParamsUpdater is ITradeParamsUpdater, DelayedExecutor {
    // @address:REGISTRY
    IRegistry constant registry = address(0);

    mapping(address => uint256) public lastTxs;

    constructor(uint256 _updatePeriod, uint256 _minDelay) DelayedExecutor(_updatePeriod, _minDelay) {}

    function requestTx(address _destination, bytes calldata _message) public override(IDelayedExecutor, DelayedExecutor) returns (uint256 _id) {
        if (lastTxs[_destination] != 0) {
            _cancelTx(lastTxs[_destination]);
        }
        uint256 _id = super.requestTx(_destination, _message);
        lastTxs[_destination] = _id;
    }

    function cancelTx(uint256 _id) public override(IDelayedExecutor, DelayedExecutor) {
        _cancelTx(_id);
    }

    function executeTx(uint256 _id) public override(IDelayedExecutor, DelayedExecutor) {
        address destination = transactions[_id].destination;
        super.executeTx(_id);
        delete lastTxs[destination];
    }

    function nearestUpdate(address _destination) external view returns (uint256) {
        return transactions[lastTxs[_destination]].date;
    }

    function _cancelTx(uint256 _id) private {
        address destination = transactions[_id].destination;
        super.cancelTx(_id);
        delete lastTxs[destination];
    }

    function _authorizeTx(address _destination, bytes memory message) internal override {
        require(ITrade(_destination).isOwner(msg.sender), "TPU/AD"); // access denied
    }

    function _authorizeExecuteTx(address _destination, bytes memory message) internal override {
        require(
            ITrade(_destination).isOwner(msg.sender)
                || msg.sender == registry.triggerServer()
                || msg.sender == address(registry.interaction()),
            "TPU/AD" // access denied
        );
    }
}