// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

import "../interfaces/ITrade.sol";
import "../utils/WhitelistUtils.sol";
import "../utils/TryCall.sol";

library ZeroX {
    event SwapSuccess(address tokenA, address tokenB, uint256 amountIn, uint256 amountOut);

    // @address:REGISTRY
    IRegistry constant registry = address(0);
    
    using SafeERC20Upgradeable for IERC20Upgradeable;

    function swap(
        address tokenA,
        address tokenB,
        uint256 amountA,
        bytes memory payload,
        ITrade trade
    ) external returns(uint256) {
        address usdt = address(registry.usdt());
        if (tokenB != usdt) {
            require(trade.status() == FundState.Opened, "T/FC"); // fund is closed
            WhitelistUtils.checkTokenAllowance(tokenB, trade);
            require(trade.isManager(msg.sender), "T/OM"); // only manager
        } else {
            require(trade.isManager(msg.sender) || msg.sender == address(registry.triggerServer()), "T/OM"); // only manager
        }
        address swapper = registry.swapper();
        IERC20Upgradeable(tokenA).safeApprove(swapper, amountA);
        uint256 balanceStart = IERC20(tokenB).balanceOf(address(trade));
        TryCall.call(swapper, payload);
        IERC20Upgradeable(tokenA).safeApprove(swapper, 0);
        uint256 diff = IERC20(tokenB).balanceOf(address(trade)) - balanceStart;
        require(diff > 0, "T/SF"); // swap failed
        emit SwapSuccess(tokenA, tokenB, amountA, diff);
        return diff;
    }
}