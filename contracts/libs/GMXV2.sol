// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

import "../interfaces/IRegistry.sol";
import "../interfaces/ITrade.sol";
import "../utils/WhitelistUtils.sol";

library GMXV2 {
    // @address:REGISTRY
    IRegistry constant registry = address(0);
    
    using SafeERC20Upgradeable for IERC20Upgradeable;

    function createOrder(
        BaseOrderUtils.CreateOrderParams calldata _params,
        ITrade _trade
    ) external returns (bytes32) {
        validateOrder(_params, _trade);
        bool isIncrease = _params.orderType == Order.OrderType.MarketIncrease || _params.orderType == Order.OrderType.LimitIncrease;
        address orderVault = registry.gmxV2OrderVault();
        IExchangeRouter exchangeRouter = registry.gmxV2ExchangeRouter();
        exchangeRouter.sendWnt{value: msg.value}(orderVault, msg.value);
        if (isIncrease) {
            IERC20Upgradeable(_params.addresses.initialCollateralToken).safeApprove(
                registry.gmxV2Router(),
                _params.numbers.initialCollateralDeltaAmount
            );
            exchangeRouter.sendTokens(
                _params.addresses.initialCollateralToken,
                orderVault,
                _params.numbers.initialCollateralDeltaAmount
            );
        }
        BaseOrderUtils.CreateOrderParams memory params = _params;
        params.addresses.uiFeeReceiver = address(0);
        params.addresses.receiver = address(_trade);
        Market.Props memory market = getMarket(_params.addresses.market);
        address pnlToken = _params.isLong ? market.longToken : market.shortToken;
        params.decreasePositionSwapType = _params.addresses.initialCollateralToken == pnlToken
            ? Order.DecreasePositionSwapType.NoSwap
            : Order.DecreasePositionSwapType.SwapPnlTokenToCollateralToken;
        params.referralCode = _trade.getGmxRefCode();
        return exchangeRouter.createOrder(params);
    }

    function getReceiveToken(BaseOrderUtils.CreateOrderParams calldata _params) internal returns (address) {
        address receiveToken = _params.addresses.initialCollateralToken;
        for (uint256 i = 0; i < _params.addresses.swapPath.length; i++) {
            Market.Props memory nextMarket = getMarket(_params.addresses.swapPath[i]);
            if (nextMarket.shortToken == receiveToken) {
                receiveToken = nextMarket.longToken;
            } else if (nextMarket.longToken == receiveToken) {
                receiveToken = nextMarket.shortToken;
            } else {
                revert("T/ISP"); // invalid swap path
            }
        }
        return receiveToken;
    }

    function getMarket(address key) internal returns (Market.Props memory) {
        return registry.gmxV2Reader()
            .getMarket(registry.gmxV2DataStore(), key);
    }

    function claimFundingFees(
        address[] memory markets,
        address[] memory tokens,
        address receiver
    ) external returns (uint256[] memory) {
        return registry.gmxV2ExchangeRouter().claimFundingFees(markets, tokens, receiver);
    }
 
    function cancelOrder(bytes32 _key, ITrade _trade) external {
        require(_trade.isManager(msg.sender) || msg.sender == registry.triggerServer(), "T/OM");
        registry.gmxV2ExchangeRouter().cancelOrder{value: msg.value}(_key);
    }

    function validateOrder(BaseOrderUtils.CreateOrderParams calldata _params, ITrade _trade) internal {
        require(_params.orderType != Order.OrderType.MarketSwap && _params.orderType != Order.OrderType.LimitSwap, "T/OTNS");
        bool isIncrease = _params.orderType == Order.OrderType.MarketIncrease || _params.orderType == Order.OrderType.LimitIncrease;
        require(!isIncrease || _trade.servicesEnabled()[0], "T/FS");
        require(!isIncrease || _trade.status() == FundState.Opened, "T/FC");
        address receiveToken = getReceiveToken(_params);
        if (receiveToken != address(registry.usdt())) {
            require(_trade.isLockedUntil(msg.sender) == 0 || _trade.isLockedUntil(msg.sender) < block.timestamp, "T/TL"); // trade locked
        }
        if (_params.orderType == Order.OrderType.MarketDecrease) {
            require(_trade.isManager(msg.sender) || (msg.sender == registry.triggerServer() && receiveToken == address(registry.usdt())), "T/OM");
        } else {
            require(_trade.isManager(msg.sender), "T/OM");
        }
        if (isIncrease) {
            address indexToken = getMarket(_params.addresses.market).indexToken;
            WhitelistUtils.checkTokenAllowance(indexToken, _trade);
        } else {
            if (receiveToken != address(registry.usdt())) {
                WhitelistUtils.checkTokenAllowance(receiveToken, _trade);
                require(_trade.status() == FundState.Opened, "T/FC"); // fund closed
            }
        }
    }
}