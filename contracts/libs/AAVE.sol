// SPDX-License-Identifier: ISC

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";

import "../interfaces/ITrade.sol";
import "../interfaces/IRegistry.sol";

library AAVE {
    event AaveSupply(address asset, uint256 amount);
    event AaveWithdraw(address asset, uint256 amount);

    // @address:REGISTRY
    IRegistry constant registry = address(0);
    
    using SafeERC20Upgradeable for IERC20Upgradeable;

    function supply(
        address _asset,
        uint256 _amount,
        ITrade _trade
    ) external {
        require(_trade.status() == FundState.Opened, "T/FC"); // fund is closed
        require(_trade.servicesEnabled()[1], "T/FS"); // forbidden service
        require(_amount <= IERC20(_asset).balanceOf(address(_trade)), "T/ANA"); // not enough amount for staking in aave
        IERC20Upgradeable(_asset).safeApprove(address(registry.aavePool()), _amount);

        registry.aavePool().supply(
            _asset,
            _amount,
            address(_trade),
            _trade.getAaveRefCode()
        );

        emit AaveSupply(_asset, _amount);
    }

    function withdraw(
        address _asset,
        uint256 _amount,
        ITrade _trade
    ) external {
        require(_trade.isManager(msg.sender) || msg.sender == address(registry.triggerServer()), "T/OM"); // only manager
        registry.aavePool().withdraw(_asset, _amount, address(_trade));

        emit AaveWithdraw(_asset, _amount);
    }

    function getPositionSizes(address[] calldata _assets, ITrade _trade) external view
    returns (uint256[] memory assetPositions)
    {
        assetPositions = new uint256[](_assets.length);
        for (uint256 i; i < _assets.length; i++) {
            (uint256 currentATokenBalance, , , , , , , , ) = registry.aavePoolDataProvider()
                .getUserReserveData(_assets[i], address(_trade));
            assetPositions[i] = currentATokenBalance;
        }
    }
}