import { expect } from 'chai';
import { Contracts, setup } from './setup';
import { createFund, ether, expectTrailingStop, percentsToEth, usdc } from './utils';
import { FUND_ID } from './constants';

const Trade = artifacts.require('Trade')

contract('FundFactory', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
	let contracts: Contracts;

	beforeEach(async function () {
		const init = await setup(deployer, signer1, signer2, trigger, signer3);
		contracts = init.contracts;
	});

	describe('Valid fees values on create fund', async function () {
		it('should revert invalid sf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { sf: percentsToEth(6) })).to.revertedWith("FE/IS");
		});

		it('should work with max sf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { sf: percentsToEth(5) })).not.to.be.reverted;
		});

		it('should work with 0 sf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { sf: percentsToEth(0) })).not.to.be.reverted;
		});

		it('should work with common sf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { sf: percentsToEth(1) })).not.to.be.reverted;
		});

		it('should revert invalid pf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { pf: percentsToEth(51) })).to.revertedWith("FE/IP");
		});

		it('should work with max pf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { pf: percentsToEth(50) })).not.to.be.reverted;
		});

		it('should work with 0 pf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { pf: percentsToEth(0) })).not.to.be.reverted;
		});

		it('should work with common pf', async function () {
			await expect(createFund(contracts.fundFactory, signer1, { pf: percentsToEth(10) })).not.to.be.reverted;
		});
	});

	describe('Fee for fund creation', async function () {
		it('should take zero fee by default', async function () {
			const initialBalance = await contracts.usdt.balanceOf(signer1);
			expect(await contracts.fundFactory.createFundFee()).to.equal(0)
			await createFund(contracts.fundFactory, signer1);
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance);
			expect(await contracts.fees.gatheredServiceFees()).to.equal(0);
		});

		it('should be able to set fee amount', async function () {
			const initialBalance = await contracts.usdt.balanceOf(signer1);
			const fee = usdc('1');
			expect(initialBalance).to.be.greaterThan(fee);
			await contracts.fundFactory.setCreateFundFee(fee);
			await contracts.usdt.approve(contracts.fees.address, fee, { from: signer1 });
			await createFund(contracts.fundFactory, signer1);
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.sub(fee));
			expect(await contracts.fees.gatheredServiceFees()).to.equal(fee);
		});

		it('should revert if could not pay fee', async function () {
			const fee = usdc('1000');
			await contracts.fundFactory.setCreateFundFee(fee);
			await contracts.usdt.burn(signer1, await contracts.usdt.balanceOf(signer1));
			await contracts.usdt.approve(contracts.fees.address, fee, { from: signer1 });
			await expect(createFund(contracts.fundFactory, signer1)).to.be.reverted;
		});
	});

	describe('Fund parameters are stored properly', async function () {
		it('should save manager and his share', async function () {
			const managerShare = percentsToEth(10);
			await createFund(contracts.fundFactory, signer1, { manager: signer1, managerShare, isPrivate: true });
			const trade = await Trade.at((await contracts.interaction.funds(FUND_ID))[0]);
			expect(await trade.owner()).to.equal(signer1);
			expect(await trade.manager()).to.equal(signer1);
			expect(await contracts.fees.managerShare(FUND_ID)).to.equal(managerShare);
			expect((await contracts.interaction.isPrivate(FUND_ID))).to.equal(true);
		});

		it('not allowed to create fund with invalid share', async function () {
			await expect(
				createFund(contracts.fundFactory, signer1, { manager: signer1, managerShare: percentsToEth(101), isPrivate: true })
			).to.revertedWith('FE/IMS');
		});
	});

	describe('Trailing stops', async function () {
		it('allows to set trailing stops config when fund is being created', async function () {
			await createFund(contracts.fundFactory, signer1, {
				globalStop: {
					isEnabled:  true,
					value: '100000000000000000'
				},
				managerStop: {
					isEnabled:  true,
					value: '100000000000000000'
				}
			})
			const trade = await Trade.at((await contracts.interaction.funds(FUND_ID))[0]);
			await contracts.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});

			await contracts.interaction.drip(FUND_ID, '0', { from: trigger })

			await expectTrailingStop(trade, true, ether('0.1'), ether('0.09'), true, ether('0.1'), ether('0.09'))
		})
	})
});
