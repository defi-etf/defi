import { expect } from 'chai';
import { Contracts, setup  } from './setup';
import BN from 'bn.js';
import { FundOpts, createFund, ether, getSwapPayload, parseEventFromLogs, usdc, percentsToEth, blockFund } from './utils';
import { TradeInstance } from '../typechain-types/contracts/main/Trade.sol/Trade';
import {
    ETH_DECIMALS,
    ETH_PRICE,
    FUND_ID,
    GAS_PRICE,
    ONE_DAY,
    ONE_WEEK, PRICEFEED_DECIMALS,
    UPGRADE_PERIOD, USDT_DECIMALS,
    FundState, FUND_ID_HEX,
} from './constants';
import { Interface } from 'ethers/lib/utils';
import { time } from '@nomicfoundation/hardhat-network-helpers';
import { ethers } from 'hardhat';
import { OrderType, gmxV2Order } from './utils/gmxv2';

const Trade = artifacts.require('Trade')
const ERC20Mock = artifacts.require('ERC20Mock')
const {expectEvent} = require('@openzeppelin/test-helpers');

const EXECUTION_FEE = ether('1');

contract('Trade', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
    let contracts: Contracts;
    let trade: TradeInstance;

    beforeEach(async function () {
        const init = await setup(deployer, signer1, signer2, trigger, signer3);
		    contracts = init.contracts;
        await contracts.whitelist.addToken(contracts.increasing.address)
        await contracts.whitelist.addToken(contracts.decreasing.address)
        await contracts.whitelist.addToken(contracts.stable.address)
    })

    const swap = (from: string, tokenA: string, tokenB: string, amount?: string) =>
      trade.swap(
        tokenA,
        tokenB,
        amount || usdc('1'),
        getSwapPayload(tokenA, tokenB, amount || usdc('1').toString()),
        { from }
      );
    const gmxV2Increase = (from: string, initialCollateralDeltaAmount = 0) =>
      trade.gmxV2CreateOrder({
            ...gmxV2Order,
            addresses: {
                ...gmxV2Order.addresses,
                initialCollateralToken: contracts.stable.address
            },
            numbers: {
                ...gmxV2Order.numbers,
                initialCollateralDeltaAmount,
            },
            orderType: OrderType.MarketIncrease
        },
        {from, value: EXECUTION_FEE});
    const gmxV2Decrease = (from: string, receiveToken: string) =>
      trade.gmxV2CreateOrder({
            ...gmxV2Order,
            addresses: {
                ...gmxV2Order.addresses,
                initialCollateralToken: receiveToken,
            },
            orderType: OrderType.MarketDecrease
        },
        {from, value: EXECUTION_FEE});
    const aaveWithdraw = (from: string) => trade.aaveWithdraw(contracts.usdt.address, usdc('1'), {from});

    async function setupFund(opts: Partial<FundOpts> = {}) {
        await createFund(contracts.fundFactory, signer1, {
            whitelistMask: '0x01',
            pf: '0',
            sf: '0',
            ...opts
        })
        trade = await Trade.at((await contracts.interaction.funds(opts.id ?? FUND_ID))['0'])
        await contracts.usdt.mint(trade.address, 1)
    }

    async function requestTx(calldata: string): Promise<number> {
        const receipt = await contracts.tradeParamsUpdater.requestTx(trade.address, calldata, {from: signer1})
        const logs = parseEventFromLogs(receipt, 'event TxRequested(address indexed,uint256 indexed,uint256,address indexed,bytes)')
        return logs[0].args[1]
    }

    it('doesnt allow to set mask longer than amount of tokens in global whitelist', async function() {
        await expect(createFund(contracts.fundFactory, signer1, {
            whitelistMask: '0x' + Buffer.from([0b1111]).toString('hex')
        }))
        .to.revertedWith('T/UT')

        await expect(createFund(contracts.fundFactory, signer1, {
            whitelistMask: '0x' + Buffer.from([0b1, 0]).toString('hex')
        }))
        .to.revertedWith('T/UT')
    })

    it('doesnt allow to use GMX if GMX disabled by service mask', async function () {
        await setupFund({ serviceMask: 0b10 })
        await expect(trade.gmxV2CreateOrder(gmxV2Order, {from: signer1})).to.revertedWith('T/FS')
    })

    it('doesnt allow to use AAVE if AAVE disabled by service mask', async function () {
        await setupFund({ serviceMask: 0b01 })

        await expect(trade.aaveSupply(contracts.usdt.address, 1, {from: signer1}))
            .to.revertedWith('T/FS')
    })

    it('doesnt allows to use AAVE if AAVE enabled by service mask', async function () {
        await setupFund({ serviceMask: 0b10 })

        await expect(trade.aaveSupply(contracts.usdt.address, 1, {from: signer1}))
            .to.not.revertedWith('T/FS')
    })

    it('should allow to transfer ownership', async function () {
        await setupFund({ manager: signer1 });
        expect(await trade.owner()).to.equal(signer1);
        await expect(trade.transferOwnership(signer2, { from: signer2 })).to.be.revertedWith("T/OO");
        await expect(trade.transferOwnership(signer2, { from: trigger })).to.be.revertedWith("T/OO");
        const receipt = await trade.transferOwnership(signer2, { from: signer1 });
        expect(await trade.owner()).to.equal(signer2);
        await expectEvent(receipt, 'OwnershipTransferred', { to: signer2 });
    });

    it('should withdraw all fees together with transferring ownership', async function () {
        const manager = signer1;
        await setupFund({
            manager,
            managerShare: percentsToEth(50),
            sf: '0',
            pf: percentsToEth(50),
        });
        const initialBalance = await contracts.usdt.balanceOf(manager);
        await contracts.interaction.stake(FUND_ID, usdc('100'), {from: signer3});
        await contracts.interaction.drip(FUND_ID, 0, {from: trigger});
        await contracts.usdt.mint(trade.address, usdc('100'), {from: deployer});
        await contracts.interaction.drip(FUND_ID, usdc('200'), {from: trigger});
        expect(await contracts.fees.managerBalance(manager)).to.equal(usdc('25'));
        expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(usdc('25'));
        await trade.transferOwnership(signer3, { from: manager });
        expect(await contracts.fees.managerBalance(manager)).to.equal(usdc('25'));
        expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(0);
        expect(await contracts.usdt.balanceOf(manager)).to.equal(initialBalance.add(usdc('25')));
    });

    describe('GMX ref code', async function () {
        beforeEach(setupFund)

        it('doesnt allow to set ref code if address is not an owner', async function () {
            await expect(trade.setGmxRefCode(ethers.constants.AddressZero)).to.revertedWith('T/OO')
        })
    });

    describe('charge debt for autoclosing', async function() {
        const SWAP_GAS_USAGE = new BN(108772);
        const DECREASE_GMX_V2_POSITION_GAS_USAGE = new BN(183251);
        const CANCEL_GMX_V2_ORDER_GAS_USAGE = new BN(26821);
        const executionFee = SWAP_GAS_USAGE.add(DECREASE_GMX_V2_POSITION_GAS_USAGE)
          .add(CANCEL_GMX_V2_ORDER_GAS_USAGE)
          .mul(new BN(GAS_PRICE))
          .add(EXECUTION_FEE)
          .div(new BN(10).pow(new BN(PRICEFEED_DECIMALS)))
          .mul(new BN(10).pow(new BN(USDT_DECIMALS)))
          .mul(new BN(ETH_PRICE))
          .div(new BN(10).pow(new BN(ETH_DECIMALS)))

        beforeEach(async function() {
            await setupFund({
                whitelistMask: '0x' + Buffer.from([0b111]).toString('hex'),
                serviceMask: 0b11,
            })
            await contracts.swapMock.setStableRate(usdc('1'))
            await contracts.usdt.mint(trade.address, usdc('3000'))
            await contracts.stable.mint(contracts.swapMock.address, usdc('1000'))
            await contracts.ethPriceFeed.setLatestAnswer(ETH_PRICE);
            await contracts.gmxV2ReaderMock.stubGetMarket(
                await contracts.registry.gmxV2DataStore(),
                gmxV2Order.addresses.market,
                {
                    marketToken: gmxV2Order.addresses.market,
                    indexToken: contracts.increasing.address,
                    longToken: contracts.increasing.address,
                    shortToken: contracts.usdt.address
                }
            )

            await swap(signer1, contracts.usdt.address, contracts.stable.address);
            await swap(trigger, contracts.stable.address, contracts.usdt.address);
            await gmxV2Decrease(trigger, contracts.usdt.address);
            await trade.gmxV2CancelOrder(ethers.constants.HashZero, {from: trigger})
        });

        it('should gather EF from Trade contract for GMX + swap', async function() {
            expect(await trade.debt()).to.equal(executionFee);
            expect(await contracts.fees.gatheredServiceFees()).to.equal(0);
            await trade.chargeDebt({ from: trigger });
            expect(await contracts.fees.gatheredServiceFees()).to.equal(executionFee);
        });

        it('should not allow to trade with usdt that is owed', async function() {
            const usdtBalance = await contracts.usdt.balanceOf(trade.address);
            await expect(swap(signer1, contracts.usdt.address, contracts.stable.address, usdtBalance.toString())).to.be.revertedWith('T/DEB');
            await expect(
                trade.gmxV2CreateOrder({
                    ...gmxV2Order,
                    addresses: {
                        ...gmxV2Order.addresses,
                        initialCollateralToken: contracts.usdt.address,
                    },
                    numbers: {
                        ...gmxV2Order.numbers,
                        initialCollateralDeltaAmount: usdtBalance.toString()
                    }
                }, { from: signer1, value: new BN(1) })
            ).to.be.revertedWith('T/DEB');
        });
    });

    describe('ZeroX', async function() {
        beforeEach(async function() {
            await setupFund({
                whitelistMask: '0x' + Buffer.from([0b111]).toString('hex')
            })
            await contracts.swapMock.setStableRate(usdc('1'))
            const buyPayload = getSwapPayload(contracts.usdt.address, contracts.stable.address, 1)
            await contracts.usdt.mint(trade.address, 1)
            await contracts.stable.mint(contracts.swapMock.address, 1)
            await trade.swap(
                contracts.usdt.address,
                contracts.stable.address,
                '1',
                buyPayload,
                {from: signer1}
            )
        })

        it('doesnt allow to decrease token position if sender is not a manager', async function() {
            await expect(swap(signer2, contracts.stable.address, contracts.usdt.address)).to.revertedWith('T/OM')
        })

        it('allows to decrease token position if sender is trigger server', async function() {
            await expect(swap(trigger, contracts.stable.address, contracts.usdt.address)).not.to.revertedWith('T/OM')
        })
    })

    describe('AAVE', async function() {
        it('doesnt allow to withdraw from AAVE if sender is not a manager', async function() {
            await setupFund()

            await expect(trade.aaveWithdraw(contracts.stable.address, '1', {from: signer2}))
                .to.revertedWith('T/OM')
        })

        it('allows to withdraw from AAVE if sender is trigger server', async function() {
            await setupFund()

            await expect(trade.aaveWithdraw(contracts.stable.address, '1', {from: trigger}))
                .not.to.revertedWith('T/OM')
        })
    })

    describe('Whitelist updates', async function() {
        let calldata: string
        let newMask: string

        beforeEach(async function() {
            await setupFund()
            newMask = '0x' + Buffer.from([0b111]).toString('hex')
            calldata = new Interface([
                'function setTradingScope(bytes,uint256)'
            ]).encodeFunctionData(
                'setTradingScope',
                [newMask, 0]
            )
        })

        it('doesnt allow to update whitelist mask and allowed service from address other than params updater', async function() {
            await expect(trade.setTradingScope('0x00', 0)).to.revertedWith('T/AD')
        })

        it('allows to update whitelist mask via params updater', async function() {
            const id = await requestTx(calldata)

            await time.increase(UPGRADE_PERIOD)

            const receipt = await contracts.tradeParamsUpdater.executeTx(id, {from: signer1})
            const logs = parseEventFromLogs(receipt, 'event WhitelistMaskUpdated(bytes)')
            const actualMask = logs[0].args[0]
            expect(await trade.whitelistMask()).to.equal(newMask)
            expect(actualMask).to.equal(newMask)
        })

        it('executes update with unstake', async function() {
            await contracts.usdt.mint(signer1, 1)
            await contracts.interaction.stake(FUND_ID, 1, {from: signer1})
            await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
            await requestTx(calldata)
            await time.increase(UPGRADE_PERIOD)

            const itkn = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID))
            await itkn.approve(contracts.interaction.address, await itkn.balanceOf(signer1), {from: signer1})
            const receipt = await contracts.interaction.unstake(FUND_ID, 10, {from: signer1})

            const logs = parseEventFromLogs(receipt, 'event WhitelistMaskUpdated(bytes)')
            const actualMask = logs[0].args[0]
            expect(await trade.whitelistMask()).to.equal(newMask)
            expect(actualMask).to.equal(newMask)
        })

        it('doesnt allow to update whitelist mask via params updater before update date', async function() {
            const id = await requestTx(calldata)

            await expect(contracts.tradeParamsUpdater.executeTx(id, {from: signer1}))
                .to.revertedWith('DE/DNP')
        })

        it('doesnt allow to update whitelist mask larger than tokens amount', async function() {
            calldata = new Interface([
                'function setTradingScope(bytes,uint256)'
            ]).encodeFunctionData(
                'setTradingScope',
                ['0x' + Buffer.from([0b1111]).toString('hex'), 0]
            )
            const id = await requestTx(calldata)
            await time.increase(UPGRADE_PERIOD)

            await expect(contracts.tradeParamsUpdater.executeTx(id, {from: signer1}))
                .to.revertedWith('T/UT')
        })

        it('doesnt allow to request update if sender is not a manager', async function() {
            await expect(contracts.tradeParamsUpdater.requestTx(trade.address, calldata, {from: signer2}))
                .to.revertedWith('TPU/AD')
        })

        it('doesnt allow to request update if sender is trigger server', async function() {
            await expect(contracts.tradeParamsUpdater.requestTx(trade.address, calldata, {from: trigger}))
                .to.revertedWith('TPU/AD')
        })

        it('doesnt allow to execute update if executor is not a manager', async function() {
            const id = await requestTx(calldata)
            await time.increase(UPGRADE_PERIOD)

            await expect(contracts.tradeParamsUpdater.executeTx(id, {from: signer2}))
                .to.revertedWith('TPU/AD')
        })

        it('allows to execute update if executor is trigger server', async function() {
            const id = await requestTx(calldata)
            await time.increase(UPGRADE_PERIOD)

            await expect(contracts.tradeParamsUpdater.executeTx(id, {from: trigger}))
                .to.not.revertedWith('TPU/AD')
        })

        it('doesnt allow to execute update if the fund has unprocessed withdrawals', async function() {
            const itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID))
            await contracts.usdt.mint(signer2, 1)
            await contracts.usdt.approve(contracts.interaction.address, 1, {from: signer2})
            await contracts.interaction.stake(FUND_ID, 1, {from: signer2})
            await contracts.interaction.drip(FUND_ID, 0, {from: trigger})
            const id = await requestTx(calldata)
            const itknBalance = await itoken.balanceOf(signer2)
            await itoken.approve(contracts.interaction.address, itknBalance, {from: signer2})
            await contracts.interaction.unstake(FUND_ID, itknBalance, {from: signer2})
            await time.increase(UPGRADE_PERIOD)

            await expect(contracts.tradeParamsUpdater.executeTx(id, {from: signer1}))
                .to.revertedWith('T/UW')
        })

        it('allows to cancel update', async function() {
            const id = await requestTx(calldata)

            await contracts.tradeParamsUpdater.cancelTx(id, {from: signer1})

            await expect(contracts.tradeParamsUpdater.executeTx(id, {from: signer1}))
                .to.revertedWith('DE/TXNF')
        })

        it('doesnt allow to cancel update if sender is not a manager', async function() {
            const id = await requestTx(calldata)

            await expect(contracts.tradeParamsUpdater.cancelTx(id, {from: signer2}))
                .to.revertedWith('TPU/AD')
        })

        it('disables indent for withdrawals if whitelist update date is earlier thant fund report date', async function() {
            await time.increase(ONE_DAY * 4) // moving to the first monday
            await contracts.usdt.burn(signer1, await contracts.usdt.balanceOf(signer1))
            await contracts.usdt.mint(signer1, usdc('1'))
            await setupFund({
                id: '0',
                indent: 2 * ONE_DAY
            })
            const itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(0))
            await time.increase(ONE_WEEK - ONE_DAY)
            await requestTx(calldata)
            await time.increase(ONE_DAY)
            await contracts.usdt.approve(contracts.interaction.address, usdc('1'), {from: signer1})
            await contracts.interaction.stake(0, usdc('1'), {from: signer1})
            await contracts.interaction.drip(0, 0, {from: trigger})
            await time.increase(ONE_WEEK)
            await contracts.interaction.drip(0, 0, {from: trigger})
            await time.increase(5 * ONE_DAY)
            await itoken.approve(contracts.interaction.address, ether('10'), {from: signer1})
            await contracts.interaction.unstake(0, ether('10'), {from: signer1})

            await time.increase(ONE_DAY)
            await contracts.interaction.drip(0, usdc('1'), {from: trigger})

            await expect(contracts.upgrader.upgrade()).to.not.revertedWith('TU/UW')
            expect((await contracts.usdt.balanceOf(signer1)).toString()).to.equal(usdc('1'))
        })

        it('cancels current update if new one requested', async function() {
            const id = await requestTx(calldata)
            await requestTx(calldata)

            expect((await contracts.tradeParamsUpdater.transactions(id)).date.toString())
                .to.equal('0')
        })

        it('cleans lastTx for executed transaction', async function() {
            const id = await requestTx(calldata)
            await time.increase(UPGRADE_PERIOD)
            await contracts.tradeParamsUpdater.executeTx(id, {from: signer1})

            expect((await contracts.tradeParamsUpdater.lastTxs(trade.address)).toString())
                .to.equal('0')
        })
    })

    describe('Service management', async function () {
        it('allows to disable and enable services via params updater', async function () {
            await setupFund({ serviceMask: 0b01 })
            const calldata = new Interface([
                'function setTradingScope(bytes,uint256)'
            ]).encodeFunctionData(
                'setTradingScope',
                ['0x', 0b10]
            )
            const id = await requestTx(calldata)
            await time.increase(UPGRADE_PERIOD)

            await contracts.tradeParamsUpdater.executeTx(id, {from: signer1})
            const values = await trade.servicesEnabled()
            expect(values.length).to.equal(2)
            expect(values[0]).to.equal(false)
            expect(values[1]).to.equal(true)
        })
    })

    describe('Fund manager', async function () {
        beforeEach(async function() {
            await setupFund({ manager: signer2, whitelistMask: '0x' + Buffer.from([0b111]).toString('hex') });
            await contracts.usdt.mint(trade.address, usdc('10000'));
            await contracts.usdt.mint(contracts.swapMock.address, ether('10000'));
            await contracts.stable.mint(contracts.swapMock.address, ether('10000'));
        });

        it('only manager is allowed to open position', async function () {
            const usdt = contracts.usdt.address;
            const stable = contracts.stable.address;
            const users = [signer1, signer3, trigger];
            for (const from of users) {
                await expect(swap(from, usdt, stable)).to.revertedWith('T/OM');
                await expect(gmxV2Increase(from, 1)).to.revertedWith('T/OM');
                await expect(trade.aaveSupply(usdt, usdc('1'), {from})).to.revertedWith('T/OM');
            }
        });

        it('close position to USDT is available only for manager and trigger server', async function () {
            const usdt = contracts.usdt.address;
            const stable = contracts.stable.address;

            const unauthUsers = [signer1, signer3];
            for (const from of unauthUsers) {
                await expect(swap(from, stable, usdt)).to.revertedWith('T/OM');
                await expect(gmxV2Decrease(from, usdt)).to.revertedWith('T/OM');
                await expect(aaveWithdraw(from)).to.revertedWith('T/OM');
            }

            const authUsers = [signer2, trigger];
            for (const from of authUsers) {
                await expect(swap(from, stable, usdt)).to.not.revertedWith('T/OM');
                await expect(gmxV2Decrease(from, usdt)).to.not.revertedWith('T/OM');
                await expect(aaveWithdraw(from)).to.not.revertedWith('T/OM');
            }
        });

        it('denies access to a subaccount if manager was changed', async function () {
            await contracts.subaccountRegistry.enableAccount(signer3, { from: signer2 })
            await contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, '0', '0', { from: signer1 })
            await contracts.interaction.drip(FUND_ID, '0', { from: trigger })

            await expect(gmxV2Increase(signer3)).to.revertedWith('T/OM')
        });

        it('no one is allowed to call setManager directly (use Interaction instead)', async function () {
            const users = [signer1, signer2, signer3, trigger];
            for (const from of users) {
                await expect(trade.setManager(from, { from })).to.revertedWith('T/AD');
            }
        });
    });

    describe('Fund state', async function() {
        beforeEach(async function() {
            await setupFund({ manager: signer2 });
        });

        it('should revert opening position in a closed fund', async function () {
            await contracts.interaction.stake(FUND_ID, usdc('1000'), { from: signer1 });
            await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
            await trade.setState(FundState.Closed, { from: signer1 });
            await expect(gmxV2Decrease(signer2, contracts.increasing.address)).to.be.revertedWith('T/FC');

            await expect(swap(signer2, contracts.usdt.address, contracts.stable.address)).to.be.revertedWith('T/FC');
            await expect(gmxV2Increase(signer2)).to.be.revertedWith('T/FC');
            await expect(gmxV2Decrease(signer2, contracts.increasing.address)).to.be.revertedWith('T/FC');
            await expect(trade.aaveSupply(contracts.usdt.address, usdc('1'), {from: signer2})).to.be.revertedWith('T/FC');
        });

        it('closing positions to USDT is still possible in closed or banned funds', async function () {
            await expect(swap(signer2, contracts.stable.address, contracts.usdt.address)).not.to.revertedWith('T/FC');
            await expect(gmxV2Decrease(signer2, contracts.usdt.address)).not.to.revertedWith('T/FC');
            await expect(trade.aaveWithdraw(contracts.usdt.address, usdc('1'), {from: signer2})).not.to.revertedWith('T/FC');
        });

        it('should emit event', async function () {
            let receipt = await trade.setState(FundState.Closed, { from: signer1 });
            await expectEvent(receipt, 'StateChanged', { newState: String(FundState.Closed) });
            receipt = await trade.setState(FundState.Opened, { from: signer1 });
            await expectEvent(receipt, 'StateChanged', { newState: String(FundState.Opened) });
        });

        it('close fund', async function() {
            await expect(trade.setState(FundState.Closed, { from: signer2 })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Closed, { from: trigger })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Closed, { from: trigger })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Closed, { from: signer1 })).not.to.be.reverted;
        });

        it('re-open fund', async function () {
            await trade.setState(FundState.Closed, { from: signer1 });
            await expect(trade.setState(FundState.Opened, { from: trigger })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Opened, { from: signer2 })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Opened, { from: signer1 })).not.to.be.reverted;
        });

        it('unblock fund', async function () {
            await blockFund(contracts.dripOperator, contracts.interaction, trade, signer1, trigger);
            await expect(trade.setState(FundState.Opened, { from: signer1 })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Opened, { from: signer2 })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Closed, { from: signer1 })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Closed, { from: signer2 })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Closed, { from: trigger })).to.be.revertedWith('T/AD');
            await expect(trade.setState(FundState.Opened, { from: trigger })).not.to.be.reverted;
        });
    })

    describe('Subaccounts', async function () {
        beforeEach(async function() {
            await setupFund()
            await contracts.subaccountRegistry.enableAccount(signer2, { from: signer1 })
        })

        it('allows trading operations using subaccount', async function() {
            const usdt = contracts.usdt.address;
            const stable = contracts.stable.address;
            
            await expect(swap(signer2, stable, usdt)).to.not.revertedWith('T/OM');
            await expect(gmxV2Decrease(signer2, usdt)).to.not.revertedWith('T/OM');
            await expect(gmxV2Increase(signer2)).to.not.revertedWith('T/OM');
            await expect(trade.aaveSupply(contracts.usdt.address, usdc('1'), { from: signer2 })).to.not.revertedWith('T/OM');
            await expect(trade.aaveWithdraw(usdt, usdc('1'), { from: signer2 })).to.not.revertedWith('T/OM');
        })

        it('doesnt allow owner\'s operations', async function() {
            await expect(trade.setAaveReferralCode(0, { from: signer2 })).to.revertedWith('T/OO')
            await expect(trade.setGmxRefCode("0x00", { from: signer2 })).to.revertedWith('T/OO')
            await expect(trade.transferOwnership(signer2, { from: signer2 })).to.revertedWith('T/OO')
        })
    })

    describe('Trade lock', async function () {
        const LOCK_TIME = ONE_DAY;
        const timestamp = Date.UTC(1970, 0, 1) / 1000;
        const duringTime = Math.round(timestamp + LOCK_TIME);
        beforeEach(async function () {
            const init = await setup(deployer, signer1, signer2, trigger, signer3);
            contracts = init.contracts;
            await contracts.whitelist.addToken(contracts.increasing.address)
            await contracts.whitelist.addToken(contracts.decreasing.address)
            await contracts.whitelist.addToken(contracts.stable.address)
            await setupFund({
                manager: signer2,
                whitelistMask: '0x' + Buffer.from([0b111]).toString('hex'),
                serviceMask: 0b11,
                sf: '0',
            });
            await contracts.usdt.mint(trade.address, usdc('100'));
        });

        it('owner and trigger server can lock / unlock trade', async function () {
            await expect(trade.lockTrade(signer2, duringTime, { from: signer2 })).to.be.revertedWith('T/AD');
            await trade.lockTrade(signer2, duringTime, { from: signer1 });
            await trade.lockTrade(signer2, 0, { from: signer1 });
            await trade.lockTrade(signer2, duringTime + LOCK_TIME, { from: signer1 });
            await expect(trade.lockTrade(signer2, 0, { from: signer2 })).to.be.revertedWith('T/AD');
            await expect(trade.lockTrade(signer2, 0, { from: signer3 })).to.be.revertedWith('T/AD');
            await trade.lockTrade(signer2, duringTime, { from: signer1 });
            expect(await trade.tradeLock(signer2)).to.equal(duringTime);
            await trade.lockTrade(signer2, 0, { from: signer1 });
            expect(await trade.tradeLock(signer2)).to.equal(0);
        });

        it('trading is not locked by default', async function () {
            expect(await trade.tradeLock(FUND_ID_HEX)).to.equal(0);
        });

        it('trigger server ignores trade lock', async function () {
            await trade.lockTrade(trigger, duringTime, { from: signer1 });
            const usdt = contracts.usdt.address;
            const stable = contracts.stable.address;
            await expect(swap(trigger, stable, usdt)).to.not.revertedWith('T/TL');
            await expect(gmxV2Decrease(trigger, usdt)).to.not.revertedWith('T/TL');
            await expect(trade.aaveWithdraw(stable, usdc('1'), {from: trigger})).to.not.revertedWith('T/TL');
        });

        it('trading is not allowed while locked', async function () {
            await trade.lockTrade(signer2, duringTime, { from: signer1 });
            const usdt = contracts.usdt.address;
            const stable = contracts.stable.address;
            await expect(swap(signer2, usdt, stable)).to.be.revertedWith('T/TL');
            await expect(gmxV2Increase(signer2, 1)).to.be.revertedWith('T/TL');
            await expect(trade.aaveSupply(stable, usdc('1'), {from: signer2})).to.be.revertedWith('T/TL');
        });

        it('closing positions to USDT is still allowed', async function () {
            await trade.lockTrade(signer2, duringTime, { from: signer1 });
            const stable = contracts.stable.address;
            const usdt = contracts.usdt.address;
            const users = [signer2, trigger];
            for (const user of users) {
                await expect(swap(user, stable, usdt)).to.not.revertedWith('T/TL');
                await expect(gmxV2Decrease(user, usdt)).to.not.revertedWith('T/TL');
                await expect(trade.aaveWithdraw(usdt, usdc('1'), {from: user})).to.not.revertedWith('T/TL');
            }
        });

        it('investment and withdrawals works normally during trade lock', async function() {
            await trade.lockTrade(signer2, duringTime, { from: signer1 });

            const initialBalance = await contracts.usdt.balanceOf(signer1);
            await contracts.interaction.stake(FUND_ID, usdc('1000'), { from: signer1 });
            await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
            const itkn = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID));
            await contracts.usdt.approve(contracts.interaction.address, usdc('1000'));
            await itkn.approve(contracts.interaction.address, await itkn.balanceOf(signer1), { from: signer1 });
            await time.increaseTo(Date.UTC(1970, 0, 4) / 1000); // first sunday
            await contracts.interaction.unstake(FUND_ID, await itkn.balanceOf(signer1), { from: signer1 });
            await contracts.interaction.drip(FUND_ID, usdc('1000'), { from: trigger });
            expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance);
        });

        it('trading is again available after time passed', async function () {
            await trade.lockTrade(signer2, duringTime, { from: signer1 });
            const usdt = contracts.usdt.address;
            const stable = contracts.stable.address;
            await time.increaseTo(Date.UTC(1970, 0, 4) / 1000); // first sunday
            await expect(swap(signer2, usdt, stable)).to.not.revertedWith('T/TL');
        });

        it('doesnt allow to use subaccount if manager was tradelocked', async function() {
            await contracts.subaccountRegistry.enableAccount(signer3, { from: signer2 })
            await trade.lockTrade(signer2, duringTime, { from: signer1 });

            await expect(gmxV2Increase(signer3)).to.be.revertedWith('T/TL');
        })
    });

    describe('Trailing stop', async function () {
        beforeEach(async function() {
            const init = await setup(deployer, signer1, signer2, trigger, signer3);
            contracts = init.contracts;
            await setupFund({
                whitelistMask: '0x',
                manager: signer2
            });
        })

        it('locks manager and resets trailing stop if manager trailing stop triggered', async () => {
            await contracts.dripOperator.requestUpdateTrailingStop(
				FUND_ID,
				true,
				ether('0.1'),
				true,
				ether('0.1'),
				{ from: signer1 }
			)
            await contracts.interaction.drip(FUND_ID, "0", {from: trigger});

            await trade.triggerManagerTrailingStop({ from: trigger })
            const {
				0: isManagerEnabled,
				1: managerValue,
				2: managerTokenRate
			} = await trade.trailingStop()
            const tradeLock = await trade.tradeLock(signer2)
            const nextPeriod = await contracts.interaction.nextPeriod(FUND_ID)
            expect(isManagerEnabled).to.equal(false)
            expect(managerValue).to.equal(ether('0'))
            expect(managerTokenRate).to.equal(ether('0'))
            expect(tradeLock).to.equal(nextPeriod)
        })

        it('blocks fund and resets trailing stop if global trailing stop triggered', async () => {
            await contracts.dripOperator.requestUpdateTrailingStop(
				FUND_ID,
				true,
				ether('0.1'),
				true,
				ether('0.1'),
				{ from: signer1 }
			)
            await contracts.interaction.drip(FUND_ID, "0", {from: trigger});

            await trade.triggerGlobalTrailingStop({ from: trigger })
            const {
				3: isGlobalEnabled,
				4: globalValue,
				5: globalTokenRate
			} = await trade.trailingStop()
            const state = await trade.status()
            expect(isGlobalEnabled).to.equal(false)
            expect(globalValue).to.equal(ether('0'))
            expect(globalTokenRate).to.equal(ether('0'))
            expect(state).to.equal('2')
        })


        it('doesnt allow to trigger trailing stops if sender is not trigger server', async () => {
            await expect(trade.triggerManagerTrailingStop({ from: signer1 }))
                .to.revertedWith('T/AD')
            await expect(trade.triggerGlobalTrailingStop({ from: signer1 }))
                .to.revertedWith('T/AD')
        })

        it('doesnt allow to trigger trailing stops if stops disabled', async () => {
            await expect(trade.triggerManagerTrailingStop({ from: trigger }))
                .to.revertedWith('T/SD')
            await expect(trade.triggerGlobalTrailingStop({ from: trigger }))
                .to.revertedWith('T/SD')
        })
    })
})