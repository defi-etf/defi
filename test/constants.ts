import BN from 'bn.js';

export enum FundState {
	Closed,
	Opened,
	Blocked
}
export const FUND_ID_HEX = '0x7cFEBE0282600DA6e7E76E8Bd3909A2E631BED1E';
export const FUND_ID = new BN(FUND_ID_HEX);
export const FUND_ID_2 = new BN('0xA0C3dB338773c16bA86c63d28675a729361E4a7a');
export const ONE_MINUTE = 60;
export const ONE_HOUR = ONE_MINUTE * 60;
export const ONE_DAY = ONE_HOUR * 24;
export const ONE_WEEK = ONE_DAY * 7;
export const ONE_MONTH = ONE_DAY * 30;
export const ONE_QUARTER = ONE_MONTH * 3;
export const UPGRADE_PERIOD = 2 * ONE_WEEK;
export const MIN_UPGRADE_PERIOD = ONE_HOUR;
export let USDT_DECIMALS = 18;
export const REPORT_DELAY = 43200; // 12 hours

export const ETH_PRICE = 200000000000; // 2000 with 8 decimals
export const PRICEFEED_DECIMALS = 8;
export const ETH_DECIMALS = 18;
export const ETH_PRICE_IN_USDT = new BN(ETH_PRICE)
	.mul(new BN(String(Math.pow(10, USDT_DECIMALS))))
	.div(new BN(Math.pow(10, PRICEFEED_DECIMALS)));
export const GAS_PRICE = 100000000; // 1 gwei
export const DRIP_GAS_USAGE = 1530000;
export const EXECUTION_FEE = new BN(DRIP_GAS_USAGE).mul(new BN(GAS_PRICE)).mul(ETH_PRICE_IN_USDT).div(new BN(String(Math.pow(10, ETH_DECIMALS))));
