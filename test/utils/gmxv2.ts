import { ethers } from "ethers";

export const OrderType = {
    MarketSwap: 0,
    LimitSwap: 1,
    MarketIncrease: 2,
    LimitIncrease: 3,
    MarketDecrease: 4,
    LimitDecrease: 5,
    StopLossDecrease: 6,
    Liquidation: 7,
};

export const decreaseOrderTypes = [
    OrderType.LimitDecrease,
    OrderType.MarketDecrease,
    OrderType.Liquidation,
    OrderType.StopLossDecrease
]

export const increaseOrderTypes = [
    OrderType.LimitIncrease,
    OrderType.MarketIncrease
]

export const supportedOrderTypes = [
    ...increaseOrderTypes,
    ...decreaseOrderTypes
]

export const DecreasePositionSwapType = {
    NoSwap: 0,
    SwapPnlTokenToCollateralToken: 1,
    SwapCollateralTokenToPnlToken: 2,
};

export const gmxV2Order = {
    addresses: {
        swapPath: [],
        initialCollateralToken: ethers.constants.AddressZero,
        market: ethers.constants.AddressZero,
        callbackContract: ethers.constants.AddressZero,
        receiver: ethers.constants.AddressZero,
        uiFeeReceiver: ethers.constants.AddressZero
    },
    numbers: {
        acceptablePrice: 0,
        callbackGasLimit: 0,
        executionFee: 0,
        initialCollateralDeltaAmount: 0,
        minOutputAmount: 0,
        sizeDeltaUsd: 0,
        triggerPrice: 0
    },
    orderType: OrderType.LimitIncrease,
    decreasePositionSwapType: DecreasePositionSwapType.NoSwap,
    isLong: true,
    referralCode: "0x0000000000000000000000000000000000000000000000000000000000000000",
    shouldUnwrapNativeToken: false
}