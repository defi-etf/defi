import { FUND_ID, ONE_WEEK, USDT_DECIMALS } from '../constants';
import { ethers } from 'ethers';
import { toWei } from 'web3-utils';
import BN from 'bn.js';
import { FundFactoryInstance } from '../../typechain-types/contracts/main/FundFactory';
import { Interface } from 'ethers/lib/utils';
import { DripOperatorInstance } from '../../typechain-types/contracts/main/DripOperator';
import { InteractionInstance } from '../../typechain-types/contracts/main/Interaction';
import { TradeInstance } from '../../typechain-types/contracts/main/Trade.sol/Trade';

// expectEvent does not support multiple events 🤡
// https://github.com/OpenZeppelin/openzeppelin-test-helpers/issues/135
export function parseEventFromLogs(receipt: any, abi: any) {
	const iface = new ethers.utils.Interface([abi]);
	const logs = [];
	for (const l of receipt.receipt.rawLogs) {
		try {
			logs.push(iface.parseLog(l));
		} catch (e) {
		}
	}
	return logs;
}

interface TrailingStopConfig {
	isEnabled: boolean;
	value: BN | string | number;
}

export interface FundOpts {
	id?: string;
	hwm?: boolean;
	pf?: string;
	sf?: string;
	investPeriod?: number;
	indent?: number;
	whitelistMask?: string;
	serviceMask?: number;
	manager?: string;
	managerShare?: string;
	isPrivate?: boolean;
	managerStop?: TrailingStopConfig;
    globalStop?: TrailingStopConfig;
}

export function percentsToEth(percents: number): string {
	return ether(String(percents / 100)).toString();
}

export function createFund(fundFactory: FundFactoryInstance, user: string, opts: FundOpts = {}) {
	return fundFactory.newFund(
		{
			id: opts.id ?? FUND_ID.toString(),
			numbers: {
				managementFee: '0', // not implemented
				// TODO: commissions should be 0 for tests where we dont test comissions
				performanceFee: opts.pf ?? '150000000000000000', // 15%
				subscriptionFee: opts.sf ?? '50000000000000000', // 5%
				investPeriod: opts.investPeriod ?? ONE_WEEK,
				indent: opts.indent || 0,
				managerShare: opts.managerShare ?? '0',
				serviceMask: opts.serviceMask  ?? 0b11,
				managerStopValue: opts.managerStop?.value ?? 0,
				globalStopValue: opts.globalStop?.value ?? 0,
			},
			flags: {
				hwm: opts.hwm ?? true,
				isPrivate: opts.isPrivate ?? false,
				managerStopEnabled: opts.managerStop?.isEnabled ?? false,
				globalStopEnabled: opts.globalStop?.isEnabled ?? false,
			},
			whitelistMask: opts.whitelistMask || '0x',
			manager: opts.manager ?? user,
		},
		{ from: user },
	);
}

export async function blockFund(
	dripOperator: DripOperatorInstance,
	interaction: InteractionInstance,
	trade: TradeInstance,
	owner: string,
	trigger: string
) {
	await dripOperator.requestUpdateTrailingStop(
		FUND_ID,
		false,
		ether('0.0'),
		true,
		ether('0.1'),
		{ from: owner }
	)
	await interaction.drip(FUND_ID, usdc('0'), { from: trigger });
	await trade.triggerGlobalTrailingStop({ from: trigger })
}

export function usdc(n: string) {
	return new BN(toWei(n, USDT_DECIMALS === 6 ? 'mwei' : 'ether'));
}

export function ether(n: string) {
	return new BN(toWei(n, 'ether'));
}

export function getSwapPayload(tokenA: string, tokenB: string, amountA: number | string | BN) {
    const swapInterface = new Interface([
        'function swap(address tokenA, address tokenB, uint256 amountA) returns(uint256)'
    ])
    return swapInterface.encodeFunctionData(
        'swap',
        [tokenA, tokenB, amountA]
    )
}

export async function expectTrailingStop(
	trade: TradeInstance,
	expectedManagerIsEnabled: boolean,
	expectedManagerValue: BN,
	expectedManagerTokenRate: BN,
	expecteGlobalIsEnabled: boolean,
	expecteGlobalValue: BN,
	expecteGlobalTokenRate: BN
) {
	const {
		0: isManagerEnabled,
		1: managerValue,
		2: managerTokenRate,
		3: isGlobalEnabled,
		4: globalValue,
		5: globalTokenRate,
	} = await trade.trailingStop()
	expect(isManagerEnabled).to.equal(expectedManagerIsEnabled);
	expect(managerValue.toString()).to.equal(expectedManagerValue.toString());
	expect(managerTokenRate.toString()).to.equal(expectedManagerTokenRate.toString());
	expect(isGlobalEnabled).to.equal(expecteGlobalIsEnabled);
	expect(globalValue.toString()).to.equal(expecteGlobalValue.toString());
	expect(globalTokenRate.toString()).to.equal(expecteGlobalTokenRate.toString());
}