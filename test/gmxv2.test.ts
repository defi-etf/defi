import { expect } from 'chai'
import { ethers } from 'hardhat'
import { Contracts, setup } from './setup'
import { TradeContract, TradeInstance } from '../typechain-types/contracts/main/Trade.sol/Trade';
import { createFund } from './utils';
import { DecreasePositionSwapType, OrderType, decreaseOrderTypes, gmxV2Order, supportedOrderTypes } from './utils/gmxv2';
import { FUND_ID, FUND_ID_2, FundState } from './constants';

const Trade: TradeContract = artifacts.require('Trade')

contract('GMXv2', function([deployer, signer1, signer2, trigger, signer4]) {
    let trading: TradeInstance
    let contracts: Contracts
    let order: Parameters<TradeInstance['gmxV2CreateOrder']>[0]

    beforeEach(async function () {
        contracts = (await setup(deployer, signer1, signer2, trigger, signer4)).contracts
        const market = ethers.constants.AddressZero
        await contracts.whitelist.addToken(contracts.increasing.address)
        await contracts.gmxV2ReaderMock.stubGetMarket(
            await contracts.registry.gmxV2DataStore(),
            market,
            {
                marketToken: market,
                indexToken: contracts.increasing.address,
                longToken: contracts.increasing.address,
                shortToken: contracts.usdt.address
            }
        )
        await createFund(contracts.fundFactory, signer1, {
            whitelistMask: '0x' + Buffer.from([0b1]).toString('hex')
        });
        const trading_address = (await contracts.interaction.funds(FUND_ID))["trade"];
        trading = await Trade.at(trading_address);
        order = {
            ...gmxV2Order,
            addresses: {
                ...gmxV2Order.addresses,
                market: market,
                initialCollateralToken: contracts.usdt.address
            }
        }
    })

    it('doesnt allow to create an order if sender is not a manager', async () => {
        for (let orderType of supportedOrderTypes) {
            await expect(trading.gmxV2CreateOrder({
                ...order,
                orderType
            }, { from: signer2 })).to.revertedWith('T/OM') 
        }
    })

    it('allows to decrease positions if sender is trigger server', async () => {
        await expect(trading.gmxV2CreateOrder({
            ...order,
            orderType: OrderType.MarketDecrease,
        }, { from: trigger })).to.not.revertedWith('T/OM')
    })

    it('replaces uiFeeReceiver with zero address', async () => {
        await trading.gmxV2CreateOrder({
            ...order,
            addresses: {
                ...order.addresses,
                uiFeeReceiver: trading.address
            }
        }, { from: signer1 })

        const params = await contracts.gmxV2ExchangeRouterMock.createOrderParams();
        expect(params.addresses.uiFeeReceiver).to.equal(ethers.constants.AddressZero)
    })

    it('replaces reciever with trading contract address', async () => {
        await trading.gmxV2CreateOrder({
            ...order,
            orderType: OrderType.MarketDecrease,
        }, { from: signer1 })

        const params = await contracts.gmxV2ExchangeRouterMock.createOrderParams()
        expect(params.addresses.receiver).to.equal(trading.address)
    })

    it('replaces decreasePositionSwapType to SwapPnlTokenToCollateralToken for long position if initialCollateralToken is not equal to pnlToken', async () => {
        await trading.gmxV2CreateOrder({
            ...order,
            addresses: {
                ...order.addresses,
                uiFeeReceiver: trading.address,
            }
        }, { from: signer1 })

        const params = await contracts.gmxV2ExchangeRouterMock.createOrderParams()
        expect(params.decreasePositionSwapType).to.equal(DecreasePositionSwapType.SwapPnlTokenToCollateralToken)
    })

    it('replaces decreasePositionSwapType to SwapPnlTokenToCollateralToken for short position if initialCollateralToken is not equal to pnlToken', async () => {
        await trading.gmxV2CreateOrder({
            ...order,
            isLong: false,
            addresses: {
                ...order.addresses,
                uiFeeReceiver: trading.address,
                initialCollateralToken: contracts.increasing.address
            }
        }, { from: signer1 })

        const params = await contracts.gmxV2ExchangeRouterMock.createOrderParams()
        expect(params.decreasePositionSwapType).to.equal(DecreasePositionSwapType.SwapPnlTokenToCollateralToken)
    })

    it('replaces decreasePositionSwapType to NoSwap for long position if initialCollateralToken is equal to pnlToken', async () => {
        await trading.gmxV2CreateOrder({
            ...order,
            addresses: {
                ...order.addresses,
                uiFeeReceiver: trading.address,
                initialCollateralToken: contracts.increasing.address
            },
            decreasePositionSwapType: DecreasePositionSwapType.SwapPnlTokenToCollateralToken
        }, { from: signer1 })

        const params = await contracts.gmxV2ExchangeRouterMock.createOrderParams()
        expect(params.decreasePositionSwapType).to.equal(DecreasePositionSwapType.NoSwap)
    })

    it('doesnt allow to increase position if the fund is closed', async () => {
        await trading.setState(FundState.Closed, { from: signer1 })
        const increaseOrderTypes = [OrderType.LimitIncrease, OrderType.MarketIncrease];

        for (let orderType of increaseOrderTypes) {
            await expect(trading.gmxV2CreateOrder({
                ...order,
                orderType
            }, { from: signer1 })).to.revertedWith('T/FC')
        }
    })
    
    it('doesn\'t allow to swap', async () => {
        const swapOrderTypes = [
            OrderType.MarketSwap,
            OrderType.LimitSwap
        ]

        for (let orderType of swapOrderTypes) {
            await expect(trading.gmxV2CreateOrder({
                ...order,
                orderType
            }, { from: signer1 })).to.revertedWith('T/OTNS')
        }
    })

    it('allows to cancel order if sender is a manager', async () => {
        await expect(trading.gmxV2CancelOrder(ethers.constants.HashZero, { from: signer1 }))
            .to.not.revertedWith('T/OM')
    })

    it('allows to cancel order if sender is trigger server', async () => {
        await expect(trading.gmxV2CancelOrder(ethers.constants.HashZero, { from: trigger }))
            .to.not.revertedWith('T/OM')
    })

    it('doesnt allow to cancel order if sender is not a manager', async () => {
        await expect(trading.gmxV2CancelOrder(ethers.constants.HashZero, { from: signer2 }))
            .to.revertedWith('T/OM')
    })

    describe('claimFundingFees', () => {
        it('replaces receiver with trading contract\'s address', async () => {
            await trading.gmxV2ClaimFundingFees([], [], { from: signer1 })

            expect(await contracts.gmxV2ExchangeRouterMock.claimFundingFeesReceiver())
                .to.equal(trading.address)
        })
    })

    describe('when GMX is disabled', () => {
        beforeEach(async () => {
            await createFund(contracts.fundFactory, signer1, {
                id: FUND_ID_2.toString(),
                whitelistMask: '0x' + Buffer.from([0b1]).toString('hex'),
                serviceMask: 0
            });
            const trading_address = (await contracts.interaction.funds(FUND_ID_2))["trade"];
            trading = await Trade.at(trading_address);
        })

        it('doesnt allow to create a GMXv2 order if GMX is disabled', async () => {
            await expect(trading.gmxV2CreateOrder(order, { from: signer1 }))
                .to.revertedWith('T/FS')
        })
    
        it('allows to decrease GMXv2 position even if GMX is disabled', async () => {
            for (let orderType of decreaseOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    orderType
                }, { from: signer1 }))
                    .to.not.revertedWith('T/FS')
            }
        })
    
        it('allows perform swap to USDT on GMXv2 even if GMX is disabled', async () => {
            const swapOrderTypes = [
                OrderType.LimitSwap,
                OrderType.MarketSwap
            ]
    
            for (let orderType of swapOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    addresses: {
                        ...order.addresses,
                        swapPath: [contracts.usdt.address]
                    },
                    orderType,
                }, { from: signer1 }))
                    .to.not.revertedWith('T/FS')
            }
        })
    })
})