import { expect } from 'chai'
import { ethers } from 'hardhat'
import { Contracts, setup } from './setup'
import { TradeContract, TradeInstance } from '../typechain-types/contracts/main/Trade.sol/Trade'
import { createFund, getSwapPayload } from './utils'
import { ERC20MockContract } from '../typechain-types/contracts/mocks/ERC20Mock'
import { FUND_ID, USDT_DECIMALS } from './constants'
import { decreaseOrderTypes, gmxV2Order, increaseOrderTypes } from './utils/gmxv2'

const ERC20Mock: ERC20MockContract = artifacts.require('ERC20Mock')
const Trade: TradeContract = artifacts.require('Trade')

contract('Whitelist', function([deployer, signer1, signer2, signer3, signer4]) {
    let contracts: Contracts

    beforeEach(async function () {
        contracts = (await setup(deployer, signer1, signer2, signer3, signer4)).contracts
    })

    it('doesnt allow to add tokens form other address than the owner', async function () {
        await expect(contracts.whitelist.addToken(contracts.usdt.address, {from: signer1}))
            .to.revertedWith('Ownable: caller is not the owner')
    })

    it('doesnt allow to add zero address as token', async function () {
        await expect(contracts.whitelist.addToken(ethers.constants.AddressZero))
            .to.revertedWith('WL/IT')
    })

    it('doesnt allow to remove token from other address than the owner', async function () {
        await expect(contracts.whitelist.removeToken(contracts.usdt.address, {from: signer1}))
            .to.revertedWith('Ownable: caller is not the owner')
    })

    describe('Trading', async function() {
        let tokenA: string
        let tokenB: string
        let tokenC: string
        let trading: TradeInstance
        const marketA = '0x47c031236e19d024b42f8AE6780E44A573170703'
        const marketB = '0xe2fedb9e6139a182b98e7c2688ccfa3e9a53c665'
        const marketC = '0xB686BcB112660343E6d15BDb65297e110C8311c4'

        beforeEach(async function() {
            // generating 256 random tokens to fill mask
            for (let i = 0; i < 256; i++) {
                const token = await ERC20Mock.new("", "", 0)
                await contracts.whitelist.addToken(token.address)
            }
            const tokenAContract = await ERC20Mock.new('A', 'A', USDT_DECIMALS)
            await tokenAContract.mint(signer1, 1)
            tokenA = tokenAContract.address
            tokenB = contracts.stable.address
            tokenC = contracts.decreasing.address
            await contracts.whitelist.addToken(tokenA)
            await contracts.whitelist.addToken(tokenB)
            await contracts.whitelist.addToken(tokenC)
            await contracts.gmxV2ReaderMock.stubGetMarket(
                await contracts.registry.gmxV2DataStore(),
                marketA,
                {
                    marketToken: marketA,
                    indexToken: tokenA,
                    longToken: tokenA,
                    shortToken: tokenB
                }
            )
            await contracts.gmxV2ReaderMock.stubGetMarket(
                await contracts.registry.gmxV2DataStore(),
                marketB,
                {
                    marketToken: marketB,
                    indexToken: ethers.constants.AddressZero,
                    longToken: tokenB,
                    shortToken: contracts.usdt.address,
                }
            )
            // testing with mask that overflows single storage slot
            const emptyMask = new Array(31).fill(0)
            await createFund(contracts.fundFactory, signer1, {
                whitelistMask: '0x' + Buffer.from([0b11, ...emptyMask, 0b1]).toString('hex')
            });
            const trading_address = (await contracts.interaction.funds(FUND_ID))["trade"];
            trading = await Trade.at(trading_address);
        })

        it('doesnt allow to swap to a forbidden token', async function () {
            const swapPayload = getSwapPayload(tokenA, tokenC, 1)

            await expect(trading.swap(
                tokenA,
                tokenC,
                1,
                swapPayload,
                { from: signer1 }
            )).to.revertedWith('T/TF')
        })

        it('allows to swap to usdt', async function () {
            const swapPayload = getSwapPayload(tokenA, contracts.usdt.address, 1)

            await expect(trading.swap(
                tokenA,
                contracts.usdt.address,
                1,
                swapPayload,
                { from: signer1 }
            )).to.not.revertedWith('T/TF')
        })

        it('reverts transaction if tokenB balance remains the same', async function () {
            await contracts.usdt.mint(trading.address, 1)
            await contracts.swapMock.setStableRate(0)
            const swapPayload = getSwapPayload(contracts.usdt.address, tokenB, 1)

            await expect(trading.swap(
                contracts.usdt.address,
                tokenB,
                1,
                swapPayload,
                { from: signer1 }
            )).to.revertedWith('T/SF')
        })

        it('doesnt allow to increase GMX V2 position if destination\'s market index token is not in funds whitelist', async function () {
            const order = {
                ...gmxV2Order,
                addresses: {
                    ...gmxV2Order.addresses,
                    market: marketB,
                    initialCollateralToken: tokenA,
                },
            }

            for (let orderType of increaseOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    orderType
                },{ from: signer1 })).to.revertedWith('T/TF')
            }
        })

        it('allows to increase GMX V2 position if destination\'s market index token is in funds whitelist', async function () {
            const order = {
                ...gmxV2Order,
                addresses: {
                    ...gmxV2Order.addresses,
                    market: marketA,
                    initialCollateralToken: tokenA,
                },
            }

            for (let orderType of increaseOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    orderType
                },{ from: signer1 })).to.not.revertedWith('T/TF')
            }
        })

        it('allows to decrease GMX V2 long position if destination token is USDT', async function () {
            const order = {
                ...gmxV2Order,
                addresses: {
                    ...gmxV2Order.addresses,
                    market: marketA,
                    swapPath: [marketB],
                    initialCollateralToken: tokenA,
                },
            }

            for (let orderType of decreaseOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    orderType
                },{ from: signer1 })).to.not.revertedWith('T/TF')
            }
        })

        it('allows to decrease GMX V2 short position if destination token is USDT', async function () {
            const order = {
                ...gmxV2Order,
                isLong: false,
                addresses: {
                    ...gmxV2Order.addresses,
                    market: marketB,
                    initialCollateralToken: tokenA,
                },
            }

            for (let orderType of decreaseOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    orderType
                },{ from: signer1 })).to.not.revertedWith('T/TF')
            }
        })

        it('doesnt allow to decrease GMX V2 long position if destination token is not in whitelist', async function () {
            await contracts.gmxV2ReaderMock.stubGetMarket(
                await contracts.registry.gmxV2DataStore(),
                marketC,
                {
                    marketToken: marketC,
                    indexToken: ethers.constants.AddressZero,
                    longToken: tokenA,
                    shortToken: tokenC
                }
            )
            const order = {
                ...gmxV2Order,
                addresses: {
                    ...gmxV2Order.addresses,
                    market: marketA,
                    swapPath: [marketC],
                    initialCollateralToken: tokenA,
                },
            }

            for (let orderType of decreaseOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    orderType
                },{ from: signer1 })).to.revertedWith('T/TF')
            }
        })

        it('doesnt allow to decrease GMX V2 short position if destination token is not in whitelist', async function () {
            await contracts.gmxV2ReaderMock.stubGetMarket(
                await contracts.registry.gmxV2DataStore(),
                marketC,
                {
                    marketToken: marketC,
                    indexToken: ethers.constants.AddressZero,
                    longToken: tokenB,
                    shortToken: tokenC
                }
            )
            const order = {
                ...gmxV2Order,
                isLong: false,
                addresses: {
                    ...gmxV2Order.addresses,
                    swapPath: [marketC],
                    market: marketA,
                    initialCollateralToken: tokenB,
                },
            }

            for (let orderType of decreaseOrderTypes) {
                await expect(trading.gmxV2CreateOrder({
                    ...order,
                    orderType
                },{ from: signer1 })).to.revertedWith('T/TF')
            }
        })
    })  
})