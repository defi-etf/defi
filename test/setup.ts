import { ether, usdc } from './utils';

import { reset } from '@nomicfoundation/hardhat-network-helpers';
import '@openzeppelin/hardhat-upgrades';
import { ethers, upgrades } from 'hardhat';
import { MIN_UPGRADE_PERIOD, UPGRADE_PERIOD, USDT_DECIMALS } from './constants';
import { ERC20MockContract, ERC20MockInstance } from '../typechain-types/contracts/mocks/ERC20Mock';
import { FundFactoryContract, FundFactoryInstance } from '../typechain-types/contracts/main/FundFactory';
import { UpgraderContract, UpgraderInstance } from '../typechain-types/contracts/main/Upgrader';
import { FeederContract, FeederInstance } from '../typechain-types/contracts/main/Feeder';
import { SwapperContract, SwapperInstance } from '../typechain-types/contracts/mocks/Swapper';
import {
	InteractionContract,
	InteractionInstance,
} from '../typechain-types/contracts/main/Interaction';
import { InvestPeriodContract, InvestPeriodInstance } from '../typechain-types/contracts/utils/InvestPeriod';
import { DripOperatorContract, DripOperatorInstance } from '../typechain-types/contracts/main/DripOperator';
import { SubaccountRegistryContract, SubaccountRegistryInstance } from '../typechain-types/contracts/main/SubaccountRegistry';
import { PriceFeedMockContract, PriceFeedMockInstance } from '../typechain-types/contracts/mocks/PriceFeedMock';
import { FeesContract, FeesInstance } from '../typechain-types/contracts/main/Fees';
import { UpgradeableBeaconInstance } from '../typechain-types/@openzeppelin/contracts/proxy/beacon/UpgradeableBeacon';
import { WhitelistContract, WhitelistInstance } from '../typechain-types/contracts/main/Whitelist';
import {
	TradeParamsUpdaterContract,
	TradeParamsUpdaterInstance,
} from '../typechain-types/contracts/main/TradeParamsUpdater';
import { RegistryContract } from '../typechain-types/contracts/main/Registry';
import { RegistryInstance } from '../typechain-types';
import {
	GMXV2ExchangeRouterMockContract,
	GMXV2ExchangeRouterMockInstance,
} from '../typechain-types/contracts/mocks/GMXV2ExchangeRouterMock';
import { GMXV2ReaderMockContract, GMXV2ReaderMockInstance } from '../typechain-types/contracts/mocks/GMXV2ReaderMock';
import { TVLComputerContract, TVLComputerInstance } from '../typechain-types/contracts/main/TVLComputer';

const ERC20Mock: ERC20MockContract = artifacts.require('ERC20Mock');
const FundFactory: FundFactoryContract = artifacts.require('FundFactory');
const Feeder: FeederContract = artifacts.require('Feeder');
const Fees: FeesContract = artifacts.require('Fees');
const SwapMock: SwapperContract = artifacts.require('Swapper');
const Interaction: InteractionContract = artifacts.require('Interaction');
const InvestPeriod: InvestPeriodContract = artifacts.require('InvestPeriod');
const DripOperator: DripOperatorContract = artifacts.require('DripOperator');
const PriceFeedMock: PriceFeedMockContract = artifacts.require('PriceFeedMock');
const Upgrader: UpgraderContract = artifacts.require('Upgrader');
const Whitelist: WhitelistContract = artifacts.require('Whitelist');
const TradeParamsUpdater: TradeParamsUpdaterContract = artifacts.require('TradeParamsUpdater');
const Registry: RegistryContract = artifacts.require('Registry');
const SubaccountRegistry: SubaccountRegistryContract = artifacts.require('SubaccountRegistry')
const GMXV2ExchangeRouterMock: GMXV2ExchangeRouterMockContract = artifacts.require('GMXV2ExchangeRouterMock');
const GMXV2ReaderMock: GMXV2ReaderMockContract = artifacts.require('GMXV2ReaderMock');
const TVLComputer: TVLComputerContract = artifacts.require('TVLComputer');

export interface Contracts {
	usdt: ERC20MockInstance;
	stable: ERC20MockInstance;
	increasing: ERC20MockInstance;
	decreasing: ERC20MockInstance;
	ethPriceFeed: PriceFeedMockInstance;
	dripOperator: DripOperatorInstance;
	fundFactory: FundFactoryInstance;
	feeder: FeederInstance;
	fees: FeesInstance;
	swapMock: SwapperInstance;
	interaction: InteractionInstance;
	investPeriod: InvestPeriodInstance;
	upgrader: UpgraderInstance;
	beacon: UpgradeableBeaconInstance;
	whitelist: WhitelistInstance;
	tradeParamsUpdater: TradeParamsUpdaterInstance;
	registry: RegistryInstance;
	subaccountRegistry: SubaccountRegistryInstance;
	gmxV2ExchangeRouterMock: GMXV2ExchangeRouterMockInstance;
	gmxV2ReaderMock: GMXV2ReaderMockInstance;
	tvlComputer: TVLComputerInstance;
}

export async function setup(
	deployer: string,
	signer1: string,
	signer2: string,
	trigger: string,
	signer3: string,
	reportDelay: number = 0,
): Promise<{ contracts: Contracts; }> {
	await reset();

	const registry = await Registry.new({ from: deployer });
	const whitelist = await Whitelist.new({ from: deployer });
	const feeder = await Feeder.new({ from: deployer });
	const upgrader = await Upgrader.new(MIN_UPGRADE_PERIOD, UPGRADE_PERIOD, { from: deployer });
	const tradeParamsUpdater = await TradeParamsUpdater.new(UPGRADE_PERIOD, UPGRADE_PERIOD / 2, { from: deployer });
	const usdt = await ERC20Mock.new('USDT', 'USDT', USDT_DECIMALS, { from: deployer });
	const stable = await ERC20Mock.new('ST', 'ST', 18, { from: deployer });
	const decreasing = await ERC20Mock.new('DE', 'DE', 18, { from: deployer });
	const increasing = await ERC20Mock.new('IN', 'IN', 18, { from: deployer });
	const swapper = await SwapMock.new(
		decreasing.address,
		increasing.address,
		stable.address,
		usdt.address,
		USDT_DECIMALS,
		{ from: deployer },
	);
	const ZeroX = await ethers.getContractFactory('ZeroX')
		.then(l => l.deploy())
	const AAVE = await ethers.getContractFactory('AAVE')
		.then(l => l.deploy())
	const GMXV2 = await ethers.getContractFactory('GMXV2')
		.then(l => l.deploy())
	const tradeFactory = await ethers.getContractFactory('Trade', {
		libraries: {
			ZeroX: ZeroX.address,
			AAVE: AAVE.address,
			GMXV2: GMXV2.address,
		},
	});
	const fees = await Fees.new({ from: deployer });
	const beacon: UpgradeableBeaconInstance = await upgrades.deployBeacon(tradeFactory, { unsafeAllowLinkedLibraries: true })
		.then(b => b.deployed()) as UpgradeableBeaconInstance;
	const investPeriod = await InvestPeriod.new({ from: deployer });
	const interaction = await Interaction.new({ from: deployer });
	const fundFactory = await FundFactory.new({ from: deployer });
	const ethPriceFeed = await PriceFeedMock.new(8, { from: deployer });
	const dripOperator = await DripOperator.new({ from: deployer });
	const subaccountRegistry = await SubaccountRegistry.new({ from: deployer });
	const gmxV2ExchangeRouterMock = await GMXV2ExchangeRouterMock.new({ from: deployer });
	const gmxV2ReaderMock = await GMXV2ReaderMock.new({ from: deployer });
	const tvlComputer = await TVLComputer.new({ from: deployer });

	await ethPriceFeed.setLatestAnswer('0');
	await beacon.transferOwnership(upgrader.address);

	await feeder.initialize({ from: deployer });
	await interaction.initialize(investPeriod.address, { from: deployer });

	await fundFactory.initialize();
	await dripOperator.initialize(reportDelay, { from: deployer });
	await fees.initialize({ from: deployer });

	await usdt.mint(signer1, usdc('1000').toString());
	await usdt.mint(signer2, usdc('1000').toString());
	await usdt.mint(signer3, usdc('1000').toString());
	await usdt.approve(interaction.address, ether('100000').toString(), { from: signer1 });
	await usdt.approve(interaction.address, ether('100000').toString(), { from: signer2 });
	await usdt.approve(interaction.address, ether('100000').toString(), { from: signer3 });
	await usdt.approve(swapper.address, ether('100000').toString(), { from: signer1 });
	await usdt.approve(swapper.address, ether('100000').toString(), { from: signer2 });
	await usdt.approve(swapper.address, ether('100000').toString(), { from: signer3 });

	return {
		contracts: {
			usdt: usdt,
			stable: stable,
			increasing: increasing,
			decreasing: decreasing,
			fundFactory: fundFactory,
			feeder: feeder,
			fees: fees,
			swapMock: swapper,
			interaction: interaction,
			investPeriod: investPeriod,
			dripOperator: dripOperator,
			ethPriceFeed: ethPriceFeed,
			upgrader: upgrader,
			beacon,
			whitelist,
			tradeParamsUpdater,
			gmxV2ExchangeRouterMock,
			gmxV2ReaderMock,
			registry,
			subaccountRegistry,
			tvlComputer,
		},
	};
}

