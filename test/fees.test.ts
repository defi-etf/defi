import { expect } from 'chai';
import { Contracts, setup } from './setup';
import { createFund, ether, parseEventFromLogs, percentsToEth, usdc } from './utils';
import { FUND_ID, FUND_ID_2 } from './constants';
import { toWei } from 'web3-utils';
import BN from 'bn.js';
import { ethers } from 'hardhat';
import { ERC20MockInstance } from '../typechain-types/contracts/mocks/ERC20Mock';

const ERC20Mock = artifacts.require('ERC20Mock');
const feeDestination = '0x999999cf1046e68e36E1aA2E0E07105eDDD1f08E';

function areNumbersClose(a: BN, b: BN) {
	const diff = a.sub(b).abs();
	return diff.lte(new BN(1));
}

contract('Fees', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
	let contracts: Contracts;

	beforeEach(async function () {
		const init = await setup(deployer, signer1, signer2, trigger, signer3);
		contracts = init.contracts;
	});

	describe('Service fees', async function () {
		it('should revert invalid sf', async function () {
			await expect(
				contracts.fees.setServiceFees(percentsToEth(6), percentsToEth(0), percentsToEth(0)),
			).to.revertedWith('FE/IS');
		});

		it('should work with max sf', async function () {
			await expect(
				contracts.fees.setServiceFees(percentsToEth(5), percentsToEth(0), percentsToEth(0)),
			).not.to.be.reverted;
		});

		it('should work with 0 sf', async function () {
			await expect(
				contracts.fees.setServiceFees(percentsToEth(0), percentsToEth(0), percentsToEth(0)),
			).not.to.be.reverted;
		});

		it('should work with common sf', async function () {
			await expect(
				contracts.fees.setServiceFees(percentsToEth(1), percentsToEth(0), percentsToEth(0)),
			).not.to.be.reverted;
		});

		it('should revert invalid pf', async function () {
			await expect(
				contracts.fees.setServiceFees(percentsToEth(0), percentsToEth(51), percentsToEth(0)),
			).to.revertedWith('FE/IP');
		});

		it('should work with max pf', async function () {
			await expect(
				contracts.fees.setServiceFees(percentsToEth(0), percentsToEth(50), percentsToEth(0)),
			).not.to.be.reverted;
		});

		it('should work with common pf', async function () {
			await expect(
				contracts.fees.setServiceFees(percentsToEth(0), percentsToEth(7.5), percentsToEth(0)),
			).not.to.be.reverted;
		});

		it('doesnt allow to withdraw if sender is not an owner', async function () {
			await contracts.fees.transferOwnership(signer1, { from: deployer });

			await expect(contracts.fees.withdraw(deployer, 0, { from: deployer }))
				.to.revertedWith('Ownable: caller is not the owner');
		});
	});

	describe('Execution fees', async function () {
		it('reverts when execution fees gathered from other address than trade or feeder', async function () {
			await createFund(contracts.fundFactory, deployer);

			await expect(contracts.fees.gatherEf(FUND_ID, 0, contracts.usdt.address))
				.to.revertedWith('FE/AD');
		});
	});

	describe('Withdraw all fees', async function () {
		async function setup(manager = signer1) {
			await createFund(contracts.fundFactory, signer1, {
				manager: manager,
				pf: '0',
				sf: percentsToEth(5),
				managerShare: percentsToEth(50),
			});
		}

		it('only owner is authorized to withdraw all', async function () {
			await setup();
			const users = [deployer, trigger, signer2, signer3];
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			for (const from of users) {
				await expect(contracts.fees.withdrawAll([FUND_ID], signer1, { from })).to.revertedWith('FE/AD');
			}
		});

		it('nothing withdrawn when 0 fees collected', async function () {
			await setup();
			await expect(contracts.fees.withdrawAll([FUND_ID], signer1, { from: signer1 })).not.to.be.reverted;
		});

		it('should transfer funds and reduce balance', async function () {
			await setup();
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			const initialBalance = await contracts.usdt.balanceOf(signer1);
			expect(await contracts.fees.managerBalance(signer1)).to.equal(usdc('2.5'));
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(usdc('2.5'));
			await contracts.fees.withdrawAll([FUND_ID], signer1, { from: signer1 });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.add(usdc('5')));
			expect(await contracts.fees.managerBalance(signer1)).to.equal(0);
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(0);
		});

		it('should transfer funds and reduce balance when manager is different from owner', async function () {
			await setup(signer2);
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer3 });
			const initialBalance1 = await contracts.usdt.balanceOf(signer1);
			const initialBalance2 = await contracts.usdt.balanceOf(signer2);
			expect(await contracts.fees.managerBalance(signer1)).to.equal(0);
			expect(await contracts.fees.managerBalance(signer2)).to.equal(usdc('2.5'));
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(usdc('2.5'));
			expect(await contracts.fees.totalFees([FUND_ID], signer1)).to.equal(usdc('2.5'));
			expect(await contracts.fees.totalFees([], signer2)).to.equal(usdc('2.5'));
			await contracts.fees.withdrawAll([FUND_ID], signer1, { from: signer1 });
			await contracts.fees.withdrawAll([], signer2, { from: signer2 });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance1.add(usdc('2.5')));
			expect(await contracts.usdt.balanceOf(signer2)).to.equal(initialBalance2.add(usdc('2.5')));
			expect(await contracts.fees.managerBalance(signer2)).to.equal(0);
			expect(await contracts.fees.managerBalance(signer1)).to.equal(0);
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(0);
		});
	});

	describe('Performance fee', async function () {
		let itoken: ERC20MockInstance;
		let tradeAddress: string;
		const fundManager = signer1;
		const feeCollector = deployer;

		beforeEach(async function () {
			await createFund(contracts.fundFactory, signer1);

			itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID));
			await itoken.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer1 });
			await itoken.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer2 });

			tradeAddress = (await contracts.interaction.funds(FUND_ID))[0];
		});

		it('Performance fee should not be gathered if tvl was increased only by new stakes', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer1 });
			const abi = 'event PfCharged(uint256 indexed,uint256)';
			let receipt = await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			let logs = parseEventFromLogs(receipt, abi);
			expect(logs[0]).to.equal(undefined);

			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer1 });
			receipt = await contracts.interaction.drip(FUND_ID, usdc('95').toString(), { from: trigger });
			logs = parseEventFromLogs(receipt, abi);
			expect(logs[0]).to.equal(undefined);

			// pf is not gathering, so the funds should not be transferred to the feeder contract
			const feederBalance = await contracts.usdt.balanceOf(contracts.feeder.address);
			expect(feederBalance.toString()).to.equal('0');
		});

		it('Performance fee should be gathered if fund had profit', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			// sf 5%, so tvl was 95 USDT - profit is 100%
			const tvl = usdc('190').toString();

			const expectedPf = usdc('14.25').toString(); // 15% of 95 usdt
			const pending = await contracts.interaction.pendingTvl([FUND_ID], [tvl], 0);
			const pf = pending[0].pf;
			expect(pf.toString()).to.equal(expectedPf);

			let receipt = await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });
			const abi = 'event PfCharged(uint256 indexed,uint256)';
			let logs = parseEventFromLogs(receipt, abi);
			expect(logs[0]).not.to.equal(undefined);

			expect(logs[0].args[1].toString()).to.equal(pf);
		});

		it('allows to withdraw service fees', async function () {
			await contracts.fees.setServiceFees(toWei('0.05', 'ether').toString(), toWei('0.15', 'ether').toString(), '0');
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			expect((await contracts.usdt.balanceOf(tradeAddress))).to.equal(usdc('90'));
			expect((await contracts.fees.gatheredServiceFees())).to.equal(usdc('5'));
			// sf 5% + 5%, so tvl was 90 USDT - profit is 100%
			const tvl = usdc('180').toString();
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });
			expect((await contracts.fees.gatheredServiceFees())).to.equal(usdc('18.5'));
			await contracts.fees.withdraw(feeDestination, usdc('10').toString(), { from: feeCollector });
			expect((await contracts.usdt.balanceOf(feeDestination)).toString()).to.equal(usdc('10').toString());
			expect((await contracts.fees.gatheredServiceFees()).toString()).to.equal(usdc('8.5').toString());
		});

		it('allows to withdraw fund\'s fees', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			// sf 5%, so tvl was 95 USDT - profit is 100%
			const tvl = usdc('190').toString();
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });

			const receipt = await contracts.fees.withdrawFund(FUND_ID, feeDestination, usdc('10'), { from: fundManager });

			expect((await contracts.usdt.balanceOf(feeDestination)).toString()).to.equals(usdc('10').toString());
			const balance = await contracts.fees.fundBalance(FUND_ID);
			expect(balance.toString()).to.equal(usdc('9.25').toString());
			const abi = 'event WithdrawalFund(uint256 indexed,address,address,uint256)';
			let [{ args: [fundId, destination, token, amount] }] = parseEventFromLogs(receipt, abi);
			expect(fundId.toString()).to.equal(FUND_ID.toString());
			expect(destination).to.equal(feeDestination);
			expect(token).to.equal(contracts.usdt.address);
			expect(amount.toString()).to.equal(usdc('10').toString());
		});

		it('decreases service gathered fees after withdrawal', async function () {
			await contracts.fees.setServiceFees(ether('0.05').toString(), ether('0.15').toString(), '0');
			await contracts.usdt.mint(contracts.fees.address, usdc('10'));
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			const availableFees = (await contracts.fees.gatheredServiceFees()).toString();

			await contracts.fees.withdraw(feeDestination, availableFees, { from: feeCollector });

			await expect(contracts.fees.withdraw(feeDestination, availableFees, { from: feeCollector }))
				.to.be.revertedWith('FE/AEB');
		});

		it('decreases fund\'s gathered fees after withdrawal', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('1000'), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
			await contracts.interaction.drip(FUND_ID, usdc('1900'), { from: trigger });
			await contracts.fees.withdrawFund(FUND_ID, feeDestination, usdc('100'), { from: fundManager });

			await expect(contracts.fees.withdrawFund(FUND_ID, feeDestination, usdc('100'), { from: fundManager }))
				.to.be.revertedWith('FE/AEB');
		});

		it('gathers correct fees performing batched drip', async function () {
			const investors = (await ethers.getSigners()).slice(1, 21);
			for (const investor of investors) {
				await contracts.usdt.mint(investor.address, usdc('100'));
				await contracts.usdt.approve(contracts.interaction.address, usdc('100'), { from: investor.address });
				await contracts.interaction.stake(FUND_ID, usdc('100'), { from: investor.address });
			}
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			for (const investor of investors) {
				const iTokenBalance = await itoken.balanceOf(investor.address);
				await itoken.approve(contracts.interaction.address, iTokenBalance, { from: investor.address });
				await contracts.interaction.unstake(FUND_ID, iTokenBalance, { from: investor.address });
			}
			await contracts.usdt.mint(tradeAddress, usdc('1900'));
			await contracts.interaction.drip(FUND_ID, usdc('3800'), { from: trigger });  // x2
			await contracts.interaction.drip(FUND_ID, usdc('3800'), { from: trigger });

			const fees = await contracts.fees.gatheredFees(FUND_ID);
			const sf = fees[1];
			const pf = fees[2];
			expect(sf.toString()).to.equal(usdc('100').toString());
			expect(pf.toString()).to.equal(usdc('285').toString());
		});


	});

	describe('PF- corner cases', async function () {
		let itoken: ERC20MockInstance;
		let tradeAddress: string;

		beforeEach(async function () {
			await createFund(contracts.fundFactory, signer1, { hwm: true, sf: '0' });
			itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID));
			await itoken.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer1 });
			await itoken.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer2 });
			tradeAddress = (await contracts.interaction.funds(FUND_ID))['0'];
		});

		it('should not change share amount when deposit and withdraw at the same report for 2 investors', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('1000'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });

			let iTokenBalance = await itoken.balanceOf(signer1);
			await contracts.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(5)), { from: signer1 });
			await contracts.interaction.stake(FUND_ID, usdc('200'), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, usdc('1000'), { from: trigger });

			const balance1 = await contracts.usdt.balanceOf(signer1);
			expect(balance1.toString()).to.equal(usdc('200').toString());

			let signer1Tvl = await contracts.interaction.userTVL(FUND_ID, usdc('1000'), signer1);
			let signer2Tvl = await contracts.interaction.userTVL(FUND_ID, usdc('1000'), signer2);
			expect(signer1Tvl.toString()).to.equal(usdc('800').toString());
			expect(signer2Tvl.toString()).to.equal(usdc('200').toString());
			expect(areNumbersClose(signer1Tvl, usdc('800'))).to.equal(true);
			expect(areNumbersClose(signer2Tvl, usdc('200'))).to.equal(true);
		});

		it('should subtract pf before calculating withdraw amounts for 2 investors', async function () {
			let balance = await contracts.usdt.balanceOf(signer2);
			expect(balance.toString()).to.equal(usdc('1000').toString());

			await contracts.interaction.stake(FUND_ID, usdc('900').toString(), { from: signer1 });
			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });

			await contracts.usdt.mint(tradeAddress, usdc('200').toString(), { from: deployer }); // +20% profit
			let iTokenBalance = await itoken.balanceOf(signer2);
			await contracts.interaction.unstake(FUND_ID, iTokenBalance.toString(), { from: signer2 });

			const receipt = await contracts.interaction.drip(FUND_ID, usdc('1200').toString(), { from: trigger });

			const pf = usdc('30').toString(); // 15% of 200 usdt
			const logs = parseEventFromLogs(receipt, 'event PfCharged(uint256 indexed,uint256)');
			expect(logs[0]).not.to.equal(undefined);
			expect(logs[0].args[1].toString()).to.equal(pf);

			balance = await contracts.usdt.balanceOf(signer2);
			expect(balance.toString()).to.equal(usdc('1017').toString());
		});

		it('PF should be gathered from new stakes, if there is no usdt on trading contract', async function () {
			await itoken.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer1 });
			await itoken.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer2 });

			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer1 });
			await contracts.interaction.stake(FUND_ID, usdc('900').toString(), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			await contracts.interaction.drip(FUND_ID, usdc('1000').toString(), { from: trigger });

			const tradeBalance = await contracts.usdt.balanceOf(tradeAddress);
			expect(tradeBalance.toString()).to.equal(usdc('1000').toString());

			// open position for the full amount of usdt
			await contracts.usdt.burn(tradeAddress, usdc('1000').toString(), { from: deployer });
			await contracts.stable.mint(tradeAddress, usdc('1500').toString(), { from: deployer });
			const tvl = usdc('1100').toString(); // +10% profit (100 USDT)

			await contracts.interaction.stake(FUND_ID, usdc('1000').toString(), { from: signer3 });
			const receipt = await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });

			const pf = usdc('15').toString(); // 15%
			const logs = parseEventFromLogs(receipt, 'event PfCharged(uint256 indexed,uint256)');
			expect(logs[0]).not.to.equal(undefined);
			expect(logs[0].args[1].toString()).to.equal(pf);

			const tvlAfterPf = usdc('2085').toString();
			const userTvl = await contracts.interaction.userTVL(FUND_ID, tvlAfterPf, signer3);
			expect(areNumbersClose(userTvl, usdc('1000'))).to.equal(true);
		});
	});

	describe('Manager fees', async function () {
		const manager = signer1;
		beforeEach(async function() {
			await createFund(contracts.fundFactory, manager, {
				manager: manager,
				managerShare: percentsToEth(10),
				pf: percentsToEth(50),
				sf: percentsToEth(5),
			});
			await contracts.fees.setServiceFees(ether('0.05'), ether('0.15'), '0');
		});

		it('no one is allowed to call setManagerShare directly (use DripOperator instead)', async function () {
			await expect(contracts.fees.setManagerShare(FUND_ID, percentsToEth(10), { from: manager })).to.revertedWith(
				"FE/AD"
			);
			await expect(contracts.fees.setManagerShare(FUND_ID, percentsToEth(10), { from: trigger })).to.revertedWith(
				"FE/AD"
			);
		});

		it('should not allow to withdraw if no balance', async function () {
			await expect(contracts.fees.withdrawManager(manager, usdc('10'), { from: manager })).to.revertedWith(
				"FE/AEB"
			);
		});

		it('only owner is allowed to withdraw fund\'s fees', async function () {
			await expect(contracts.fees.withdrawFund(FUND_ID, manager, usdc('10'), { from: signer2 })).to.revertedWith(
				"FE/SNO"
			);
			await expect(contracts.fees.withdrawFund(FUND_ID, manager, usdc('10'), { from: trigger })).to.revertedWith(
				"FE/SNO"
			);
		});

		it('should split fund fee into manager and owner shares', async function () {
			const initialBalance = await contracts.usdt.balanceOf(manager);

			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			// sf 10%, so tvl was 90 USDT - profit is 100%
			const tvl = usdc('180').toString();
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });
			expect(await contracts.fees.managerBalance(manager)).to.equal(usdc('5'));
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(usdc('45'));
			expect(await contracts.fees.gatheredServiceFees()).to.equal(usdc('18.5'));
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance);

			await contracts.fees.withdrawManager(manager, usdc('5'), { from: manager });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.add(new BN(usdc('5'))));

			await contracts.fees.withdrawFund(FUND_ID, manager, usdc('45'), { from: manager });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.add(new BN(usdc('50'))));

			await contracts.fees.withdraw(manager, usdc('18.5'), { from: deployer });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.add(new BN(usdc('68.5'))));

			expect(await contracts.fees.managerBalance(manager)).to.equal(usdc('0'));
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(usdc('0'));
			expect(await contracts.fees.gatheredServiceFees()).to.equal(usdc('0'));
		});

		it('manager is able to withdraw previous fees after changing manager in the fund', async function () {
			const initialBalance = await contracts.usdt.balanceOf(manager);

			await contracts.interaction.stake(FUND_ID, usdc('100').toString(), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			// sf 10%, so tvl was 90 USDT - profit is 100%
			const tvl = usdc('180').toString();
			await contracts.dripOperator.requestUpdateManager(FUND_ID, signer2, percentsToEth(100), 0, { from: manager });
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });

			await expect(contracts.fees.withdrawManager(signer2, usdc('5'), { from: signer2 })).to.revertedWith('FE/AEB');
			await expect(contracts.fees.withdrawManager(manager, usdc('5'), { from: signer2 })).to.revertedWith('FE/AEB');
			await contracts.fees.withdrawManager(manager, usdc('5'), { from: manager });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.add(new BN(usdc('5'))));
		});

		it('returns correct amounts of total fees collected', async function () {
			await createFund(contracts.fundFactory, manager, {
				id: FUND_ID_2.toString(),
				manager: manager,
				managerShare: percentsToEth(10),
				pf: percentsToEth(50),
				sf: percentsToEth(5),
			});
			for (const id of [FUND_ID, FUND_ID_2]) {
				await contracts.interaction.stake(id, usdc('100').toString(), { from: signer2 });
				await contracts.interaction.drip(id, '0', { from: trigger });
			}
			const tvl = usdc('180').toString();
			for (const id of [FUND_ID, FUND_ID_2]) {
				await contracts.interaction.drip(id, tvl, { from: trigger });
			}

			expect(await contracts.fees.totalFees([FUND_ID, FUND_ID_2], manager)).to.equal(usdc('100'));
		})
	});
});
