import { expect } from 'chai';
import { Contracts, setup } from './setup';
import { createFund, ether } from './utils';
import { ETH_PRICE, ETH_PRICE_IN_USDT, FUND_ID } from './constants';
import { TradeContract, TradeInstance } from '../typechain-types/contracts/main/Trade.sol/Trade';

const Trade: TradeContract = artifacts.require('Trade');

contract('TVLComputer', function ([deployer, signer1, signer2, signer3, signer4]) {
	let contracts: Contracts;
	let trade: TradeInstance;

	beforeEach(async function () {
		contracts = (await setup(deployer, signer1, signer2, signer3, signer4)).contracts;

		await contracts.whitelist.addToken(contracts.stable.address);
		await contracts.whitelist.addToken(contracts.decreasing.address);
		await contracts.whitelist.addToken(contracts.increasing.address);
		await contracts.ethPriceFeed.setLatestAnswer(ETH_PRICE);
		await contracts.tvlComputer.setPriceFeeds([contracts.stable.address], [contracts.ethPriceFeed.address], { from: deployer });
		await contracts.tvlComputer.setPriceFeeds([contracts.decreasing.address], [contracts.ethPriceFeed.address], { from: deployer });
		await contracts.tvlComputer.setPriceFeeds([contracts.increasing.address], [contracts.ethPriceFeed.address], { from: deployer });
	});

	describe('All tokens are whitelisted', () => {
		beforeEach(async function () {
			await createFund(contracts.fundFactory, signer1, {
				whitelistMask: '0x' + Buffer.from([0b111]).toString('hex'),
			});
			trade = await Trade.at((await contracts.interaction.funds(FUND_ID))['0']);
		});

		it('should return 0 tvl for just created fund', async function () {
			expect(await contracts.tvlComputer.getTVL(trade.address)).to.equal(0);
		});

		it('fund has 3 tokens and usdt', async function () {
			await contracts.stable.mint(trade.address, ether('1'));
			await contracts.increasing.mint(trade.address, ether('0.5'));
			await contracts.decreasing.mint(trade.address, ether('0.3'));
			await contracts.usdt.mint(trade.address, ETH_PRICE_IN_USDT.muln(20).divn(100));
			expect(await contracts.tvlComputer.getTVL(trade.address)).to.equal(ETH_PRICE_IN_USDT.muln(2));
		});
	});

	describe('1 of 3 tokens is whitelisted', () => {
		beforeEach(async function () {
			await createFund(contracts.fundFactory, signer1, {
				whitelistMask: '0x' + Buffer.from([0b001]).toString('hex'),
			});
			trade = await Trade.at((await contracts.interaction.funds(FUND_ID))['0']);
		});

		it('fund has 3 tokens and usdt', async function () {
			await contracts.stable.mint(trade.address, ether('1'));
			await contracts.increasing.mint(trade.address, ether('0.5'));
			await contracts.decreasing.mint(trade.address, ether('0.5'));
			await contracts.usdt.mint(trade.address, ETH_PRICE_IN_USDT);
			expect(await contracts.tvlComputer.getTVL(trade.address)).to.equal(ETH_PRICE_IN_USDT.muln(2));
		});
	});

});