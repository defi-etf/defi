const { expect } = require('chai');
const { ethers, network, upgrades, artifacts } = require('hardhat');
const {ether, BN, expectEvent} = require('@openzeppelin/test-helpers');
const {toWei} = require("web3-utils");
const { time, reset } = require("@nomicfoundation/hardhat-network-helpers");
const { parseEventFromLogs, createFund, blockFund } = require("./utils");
const { ONE_DAY, ONE_WEEK, ONE_MONTH, ONE_QUARTER, UPGRADE_PERIOD, MIN_UPGRADE_PERIOD, USDT_DECIMALS } = require("./constants");

const FundFactory = artifacts.require('FundFactory');
const ERC20Mock = artifacts.require('ERC20Mock');
const Feeder = artifacts.require('Feeder');
const Fees = artifacts.require('Fees');
const Trade = artifacts.require('Trade');
const SwapMock = artifacts.require('Swapper');
const Interaction = artifacts.require('Interaction');
const InvestPeriod = artifacts.require('InvestPeriod');
const DripOperator = artifacts.require('DripOperator');
const SubaccountRegistry = artifacts.require('SubaccountRegistry');
const PriceFeedMock = artifacts.require('PriceFeedMock');
const Upgrader = artifacts.require('Upgrader');
const Whitelist = artifacts.require('Whitelist');
const Registry = artifacts.require('Registry');
const TradeParamsUpdater = artifacts.require('TradeParamsUpdater');
const FUND_ID = new BN('0x7cFEBE0282600DA6e7E76E8Bd3909A2E631BED1E');

function usdc (n) {
    return new BN(toWei(n, USDT_DECIMALS === 6 ? 'mwei' : 'ether'));
}

contract('===FundFactory===', function ([deployer, signer1, signer2, trigger, signer3]) {
    beforeEach(async function () {
        await reset();
        this.registry = await Registry.new();
        this.whitelist = await Whitelist.new()
        this.feeder = await Feeder.new({from: deployer});
        await Upgrader.new(MIN_UPGRADE_PERIOD, UPGRADE_PERIOD)
        await TradeParamsUpdater.new(UPGRADE_PERIOD, UPGRADE_PERIOD / 2)
        //////////////////////////////
        /** 0x mock -------- **/
        //////////////////////////////
        this.usdt = await ERC20Mock.new("USDT", "USDT", USDT_DECIMALS, {from: deployer});
        this.stable = await ERC20Mock.new("ST", "ST", 18, {from: deployer});
        this.decreasing = await ERC20Mock.new("DE", "DE", 18, {from: deployer});
        this.increasing = await ERC20Mock.new("IN", "IN", 18, {from: deployer});
        this.swapper = await SwapMock.new(
            this.decreasing.address,
            this.increasing.address,
            this.stable.address,
            this.usdt.address,
            USDT_DECIMALS,
            {from: deployer}
        );

        ////////////////////////////////
        /** Deployments ------------ **/
        ////////////////////////////////
        // TODO: tests for USDT with 18 decimals
        const ZeroX = await ethers.getContractFactory('ZeroX')
		    .then(l => l.deploy())
        const AAVE = await ethers.getContractFactory('AAVE')
		    .then(l => l.deploy())
        const GMXV2 = await ethers.getContractFactory('GMXV2')
		    .then(l => l.deploy())
        const tradeFactory = await ethers.getContractFactory("Trade", {
            libraries: {
                ZeroX: ZeroX.address,
                AAVE: AAVE.address,
                GMXV2: GMXV2.address,
            }
        });
        this.fees = await Fees.new({from: deployer});
        this.beacon = await upgrades.deployBeacon(tradeFactory, { unsafeAllowLinkedLibraries: true });
        await this.beacon.deployed();
        const investPeriod = await InvestPeriod.new({from: deployer})
        this.interaction = await Interaction.new({from: deployer});
        this.fundFactory = await FundFactory.new({from: deployer});

        this.priceFeed = await PriceFeedMock.new(8, {from: deployer});
        this.dripOperator = await DripOperator.new({from: deployer});
        await SubaccountRegistry.new({from: deployer});
        await this.feeder.initialize({from: deployer});
        await this.interaction.initialize(investPeriod.address, {from: deployer})
        await this.fundFactory.initialize({from: deployer})
        await this.dripOperator.initialize(0, {from: deployer})

        await this.fees.initialize({from: deployer});
        //////////////////////////////
        /** Initial Setup -------- **/
        //////////////////////////////
        await this.usdt.mint(signer1, usdc("1000").toString());
        await this.usdt.mint(signer2, usdc("1000").toString());
        await this.usdt.mint(signer3, usdc("1000").toString());
        let balance = await this.usdt.balanceOf(signer1);
        expect(balance.toString()).to.equal(usdc("1000").toString());

        await this.usdt.approve(this.interaction.address, ether("100000").toString(), {from: signer1});
        await this.usdt.approve(this.interaction.address, ether("100000").toString(), {from: signer2});
        await this.usdt.approve(this.interaction.address, ether("100000").toString(), {from: signer3});

        await this.stable.mint(this.swapper.address, ether("1000").toString());
        await this.decreasing.mint(this.swapper.address, ether("1000").toString());
        await this.increasing.mint(this.swapper.address, ether("1000").toString());
        await this.usdt.approve(this.swapper.address, ether("100000").toString(), {from: signer1});
        await this.usdt.approve(this.swapper.address, ether("100000").toString(), {from: signer2});
        await this.usdt.approve(this.swapper.address, ether("100000").toString(), {from: signer3});
    });

    describe('Basic', async function () {
        it('create fund', async function () {
            let receipt = await createFund(this.fundFactory, signer1);
            await expectEvent(receipt, "FundCreated",
                {
                    manager: signer1,
                    hwm: true,
                    sf: "50000000000000000",
                    pf: "150000000000000000",
                    mf: "0",
                    period: ONE_WEEK.toString()
                })
        });

        it('fund info', async function () {
            await createFund(this.fundFactory, signer1);

            let info = await this.interaction.fundInfo(FUND_ID);
            expect(info[0]).not.to.equal(ethers.constants.AddressZero);
            expect(info[1].toString()).to.equal("0");
        });

        it('stake', async function () {
            await createFund(this.fundFactory, signer1);

            let iToken_address = await this.interaction.tokenForFund(FUND_ID);
            let itoken = await ERC20Mock.at(iToken_address);
            let balance = await itoken.balanceOf(signer1);
            expect(balance.toString()).to.equal("0");
            let itoken_name = await itoken.name();
            expect(itoken_name).to.equal("Defunds Token");

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});

            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("900").toString());
            balance = await itoken.balanceOf(signer1);
            expect(balance.toString()).to.equal("0");

            let status = await this.interaction.pendingDepositAndWithdrawals(FUND_ID, signer1);
            expect(status[0].toString()).to.equal(usdc("95").toString());
            expect(status[1].toString()).to.equal("0");
        });

        it('sets correct nextPeriod with weekly report period', async function() {
            const expectedNextPeriod = Date.UTC(1970, 0, 5) / 1000 // first monday after 1.01.1970
            await createFund(this.fundFactory, signer1)

            expect((await this.interaction.nextPeriod(FUND_ID)).toString()).to.be.equal(expectedNextPeriod.toString())
        })

        it('sets correct nextPeriod with monthly report period', async function() {
            const expectedPeriod = Date.UTC(1970, 1, 1) / 1000
            await createFund(this.fundFactory, signer1, {
                investPeriod: ONE_MONTH
            })

            expect((await this.interaction.nextPeriod(FUND_ID))).to.be.equal(expectedPeriod.toString())
        })

        it('sets correct nextPeriod with quarterly report period', async function() {
            const expectedPeriod = Date.UTC(1970, 3, 1) / 1000
            await createFund(this.fundFactory, signer1, {
                investPeriod: ONE_QUARTER
            })

            expect((await this.interaction.nextPeriod(FUND_ID))).to.be.equal(expectedPeriod.toString())
        })

        it('doesnt allow to create fund with weekly report period and indent more than 3 days', async function () {
            await expect(createFund(this.fundFactory, signer1, {
                investPeriod: ONE_WEEK,
                indent: ONE_DAY * 3 + 1
            })).to.revertedWith('FF/ITS')
        })

        it('allows to create fund with monthly report period and indent less than or equal to 7 days', async function () {
            await expect(createFund(this.fundFactory, signer1, {
                investPeriod: ONE_MONTH,
                indent: ONE_DAY * 7
            })).to.not.revertedWith('FF/ITS')
        })

        it('doesnt allow to create fund with monthly report period and indent more than 7 days', async function () {
            await expect(createFund(this.fundFactory, signer1, {
                investPeriod: ONE_MONTH,
                indent: ONE_DAY * 7 + 1
            })).to.revertedWith('FF/ITS')
        })

        it('doesnt allow to create fund with quarterly report period and indent more than 7 days', async function () {
            await expect(createFund(this.fundFactory, signer1, {
                investPeriod: ONE_QUARTER,
                indent: ONE_DAY * 7 + 1
            })).to.revertedWith('FF/ITS')
        })
    });

    describe('Stake/unstake', async function () {
        beforeEach(async function () {
            await createFund(this.fundFactory, signer1);
            let iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer2});

            this.trading_address = (await this.interaction.funds(FUND_ID))["trade"];
        });

        it('should mint ITKN in x10 amount with 18 decimals during the first stake', async function () {
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});
            const itknBalance = await this.itoken.balanceOf(signer1);
            expect(itknBalance.toString()).to.equal(ether("950").toString());
        });

        it('cancel stake', async function () {
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});

            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("900").toString());

            await expect(
                this.interaction.cancelDeposit(FUND_ID, {from: signer2})
            ).to.be.revertedWith("F/ND");

            await this.interaction.cancelDeposit(FUND_ID, {from: signer1});
            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("995").toString());
        });

        it('drip stake', async function () {
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(usdc("0").toString());

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});

            const TradeTVL = usdc("0").toString();
            // transfer funds to trading contract
            await expect(this.interaction.drip(FUND_ID, TradeTVL, {from: signer1})).to.be.revertedWith("I/AD");
            await this.interaction.drip(FUND_ID, TradeTVL, {from: trigger});

            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(usdc("95").toString());

            let iTokenBalance = await this.itoken.balanceOf(signer1);
            expect(iTokenBalance.toString()).to.equal(ether("950").toString());

            let status = await this.interaction.pendingDepositAndWithdrawals(FUND_ID, signer1);
            expect(status[0].toString()).to.equal("0");
            expect(status[1].toString()).to.equal("0");

            // Second stake
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, tradeBalance.toString(), {from: trigger});
            iTokenBalance = await this.itoken.balanceOf(signer1);
            expect(iTokenBalance.toString()).to.equal(ether("1900").toString());
        });

        it('cancel unstake', async function () {
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});

            const TradeTVL = usdc("1000").toString();
            await this.interaction.drip(FUND_ID, TradeTVL, {from: trigger});
            await this.interaction.unstake(FUND_ID, ether("950").toString(), {from: signer1});
            let iTokenBalance = await this.itoken.balanceOf(signer1);
            expect(iTokenBalance.toString()).to.equal("0");

            let status = await this.interaction.pendingDepositAndWithdrawals(FUND_ID, signer1);
            expect(status[0].toString()).to.equal(usdc("0").toString());
            expect(status[1].toString()).to.equal(ether("950").toString());

            await expect(
                this.interaction.cancelWithdraw(FUND_ID, {from: signer2})
            ).to.be.revertedWith("F/NW");
            await this.interaction.cancelWithdraw(FUND_ID, {from: signer1});

            iTokenBalance = await this.itoken.balanceOf(signer1);
            expect(iTokenBalance.toString()).to.equal(ether("950").toString());
            status = await this.interaction.pendingDepositAndWithdrawals(FUND_ID, signer1);
            expect(status[0].toString()).to.equal(usdc("0").toString());
            expect(status[1].toString()).to.equal(ether("0").toString());
        });

        it('unstake - full scenario with 1 investor', async function () {
            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("1000").toString());

            await this.interaction.stake(FUND_ID, usdc("900").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, "0", {from: trigger});

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, usdc("855").toString(), {from: trigger}); // 900 - 5%

            usdtBalanceTrader = await this.usdt.balanceOf(this.trading_address);
            expect(usdtBalanceTrader.toString()).to.equal(usdc("950").toString()); // 95 + 855
            await this.usdt.mint(this.trading_address, usdc("95").toString(), {from: deployer}); // +10% profit

            let iTokenBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance.toString(), {from: signer1});
            iTokenBalance = await this.itoken.balanceOf(signer1);
            expect(iTokenBalance.toString()).to.equal('0');

            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal('0');

            const tvl = usdc("1030.75").toString();
            const pf = usdc("14.25").toString(); // 15% of 95 usdt
            const totalTvl = usdc("1045").toString();

            const tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(totalTvl);

            await expect(
              this.interaction.drip(FUND_ID, totalTvl, {from: signer1})
            ).to.be.revertedWith("I/AD");
            const receipt = await this.interaction.drip(FUND_ID, totalTvl, {from: trigger});
            const logs = parseEventFromLogs(receipt, "event PfCharged(uint256 indexed,uint256)");
            const pfEvent = logs[0];
            expect(pfEvent).not.to.equal(undefined);
            expect(pfEvent.args[1].toString()).to.equal(pf);

            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(tvl);
            usdtBalanceFeeder = await this.usdt.balanceOf(this.feeder.address);
            expect(usdtBalanceFeeder.toString()).to.equal('0');
        });

        it('both stake and unstake in a single report', async function () {
            let balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});
            let tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(usdc("95").toString());

            const itknBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, itknBalance.div(new BN(2)).toString(), {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, usdc("95").toString(), {from: trigger});

            balance = await this.usdt.balanceOf(signer1);
            tradeBalance = await this.usdt.balanceOf(this.trading_address);

            expect(balance.toString()).to.equal(usdc("847.5").toString());
            expect(tradeBalance.toString()).to.equal(usdc("142.5").toString());
        });

        it('2 investors - stake then unstake different amounts', async function () {
            // initially signer1 and signer2 have 1000 USDT balance
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("200").toString(), {from: signer2});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            const tvl = usdc("285").toString(); // 95 + 95 (sf is 5%)
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(tvl);

            const iTokenBalance1 = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance1.toString(), {from: signer1});
            const iTokenBalance2 = await this.itoken.balanceOf(signer2);
            await this.interaction.unstake(FUND_ID, iTokenBalance2.div(new BN(2)).toString(), {from: signer2});

            await this.interaction.drip(FUND_ID, tvl, {from: trigger});

            balance1 = await this.usdt.balanceOf(signer1);
            balance2 = await this.usdt.balanceOf(signer2);
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(balance1.toString()).to.equal(usdc("995").toString());
            expect(balance2.toString()).to.equal(usdc("895").toString());
            expect(tradeBalance.toString()).to.equal(usdc("95").toString());
        });

        it('2 investors 1 partial unstake (half of tokens)', async function () {
            // initially signer1 and signer2 have 1000 USDT balance
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer2});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            const tvl = usdc("190").toString(); // 95 + 95 (sf is 5%)
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(tvl);
            const iTokenBalance1 = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance1.div(new BN(2)).toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, tvl, {from: trigger});

            balance1 = await this.usdt.balanceOf(signer1);
            balance2 = await this.usdt.balanceOf(signer2);
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(balance1.toString()).to.equal(usdc("947.5").toString());
            expect(balance2.toString()).to.equal(usdc("900").toString());
            expect(tradeBalance.toString()).to.equal(usdc("142.5").toString());
        });

        it('2 investors, both unstake half of tokens', async function () {
            // initially signer1 and signer2 have 1000 USDT balance
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer2});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            const tvl = usdc("190").toString(); // 95 + 95 (sf is 5%)
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(tvl);
            const iTokenBalance1 = await this.itoken.balanceOf(signer1);
            const iTokenBalance2 = await this.itoken.balanceOf(signer2);
            await this.interaction.unstake(FUND_ID, iTokenBalance1.div(new BN(2)).toString(), {from: signer1});
            await this.interaction.unstake(FUND_ID, iTokenBalance2.div(new BN(2)).toString(), {from: signer2});
            await this.interaction.drip(FUND_ID, tvl, {from: trigger});

            balance1 = await this.usdt.balanceOf(signer1);
            balance2 = await this.usdt.balanceOf(signer2);
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(balance1.toString()).to.equal(usdc("947.5").toString());
            expect(balance2.toString()).to.equal(usdc("947.5").toString());
            expect(tradeBalance.toString()).to.equal(usdc("95").toString());
        });

        it('2 investors 1 full unstake', async function () {
            // initially signer1 and signer2 have 1000 USDT balance
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer2});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            const tvl = usdc("190").toString(); // 95 + 95 (sf is 5%)
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(tvl);
            const iTokenBalance1 = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance1.toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, tvl, {from: trigger});

            balance1 = await this.usdt.balanceOf(signer1);
            balance2 = await this.usdt.balanceOf(signer2);
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(balance1.toString()).to.equal(usdc("995").toString());
            expect(balance2.toString()).to.equal(usdc("900").toString());
            expect(tradeBalance.toString()).to.equal(usdc("95").toString());
        });

        it('full unstake 2 investors', async function () {
            // initially signer1 and signer2 have 1000 USDT balance
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer2});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            const tvl = usdc("190").toString(); // 95 + 95 (sf is 5%)
            tradeBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradeBalance.toString()).to.equal(tvl);
            const iTokenBalance1 = await this.itoken.balanceOf(signer1);
            const iTokenBalance2 = await this.itoken.balanceOf(signer2);
            await this.interaction.unstake(FUND_ID, iTokenBalance1.toString(), {from: signer1});
            await this.interaction.unstake(FUND_ID, iTokenBalance2.toString(), {from: signer2});
            await this.interaction.drip(FUND_ID, tvl, {from: trigger});

            balance1 = await this.usdt.balanceOf(signer1);
            balance2 = await this.usdt.balanceOf(signer2);
            expect(balance1.toString()).to.equal(usdc("995").toString());
            expect(balance2.toString()).to.equal(usdc("995").toString());
        });

        it('cancel stake multiple', async function () {
            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("1000").toString());

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.cancelDeposit(FUND_ID, {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});

            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("895").toString());

            usdtBalanceFeeder = await this.usdt.balanceOf(this.feeder.address);
            expect(usdtBalanceFeeder.toString()).to.equal(usdc("95").toString());

            const TradeTVL = usdc("95").toString();
            await this.interaction.drip(FUND_ID, TradeTVL, {from: trigger});

            usdtBalanceTrader = await this.usdt.balanceOf(this.trading_address);
            expect(usdtBalanceTrader.toString()).to.equal(usdc("95").toString());
        });

        it('pendingTvl should return mustBePaid and withdraw equal to tvl, if all USDT is in position', async function () {
            const balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});
            const tvl = usdc("95").toString();

            // all USDT is in position
            await this.usdt.burn(this.trading_address, usdc("95").toString(), {from: deployer});
            await this.stable.mint(this.trading_address, usdc("142.5").toString(), {from: deployer});

            let iTokenBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance, {from: signer1});
            const tvls = await this.interaction.pendingTvl([FUND_ID], [tvl], 0);
            const pending = tvls[0];
            expect(pending.mustBePaid.toString()).to.equal(tvl);
            expect(pending.withdraw.toString()).to.equal(tvl);
        });

        it('stakes/unstakes should not affect tokenRate', async function () {
            expect(await this.interaction.tokenRate(FUND_ID, '0')).to.equal('0');
            const balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            let tvl = usdc('95').toString();
            const rate = usdc('0.1').toString();

            let tokenRate = await this.interaction.tokenRate(FUND_ID, tvl);
            expect(tokenRate.toString()).to.equal(rate);

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, tvl, {from: trigger});

            tvl = usdc('190').toString();
            tokenRate = await this.interaction.tokenRate(FUND_ID, tvl);
            expect(tokenRate.toString()).to.equal(rate);

            let iTokenBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance, {from: signer1});
            await this.interaction.drip(FUND_ID, tvl, {from: trigger});

            tokenRate = await this.interaction.tokenRate(FUND_ID, '0');
            expect(tokenRate.toString()).to.equal('0');
        });

        it('leaves zero allowance from feeder to fees after drip', async function () {
            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, usdc("100"), {from: trigger});

            const allowance = await this.usdt.allowance(this.feeder.address, this.fees.address)
            expect(allowance.toString()).to.equal('0')
        })

        // TODO: cannot cancel withdrawal during report
    });

    describe('Dripping', async function () {
        it('sets correct weekly nextPeriod', async function () {
            const expectedPeriod = Date.UTC(1970, 0, 12) / 1000 // 12.01.1970
            await createFund(this.fundFactory, signer1, {
                investPeriod: ONE_WEEK
            })
            await this.interaction.stake(FUND_ID, "1000", {from: signer2});
            await time.increaseTo(Date.UTC(1970, 0, 5, 4, 30) / 1000)

            await this.interaction.drip(FUND_ID, "0", {from: trigger});

            expect((await this.interaction.nextPeriod(FUND_ID))).to.be.equal(expectedPeriod.toString())
        });

        it('sets correct monthly nextPeriod', async function () {
            const expectedPeriod = Date.UTC(1970, 2, 1) / 1000
            await createFund(this.fundFactory, signer1, {
                investPeriod: ONE_MONTH
            })
            await this.interaction.stake(FUND_ID, "1000", {from: signer2});
            await time.increaseTo(Date.UTC(1970, 1, 1) / 1000)

            await this.interaction.drip(FUND_ID, "0", {from: trigger});

            expect((await this.interaction.nextPeriod(FUND_ID))).to.be.equal(expectedPeriod.toString())
        });

        it('sets correct quarterly nextPeriod', async function () {
            const expectedPeriod = Date.UTC(1970, 6, 1) / 1000 // 1.07.1970
            await createFund(this.fundFactory, signer1, {
                investPeriod: ONE_QUARTER
            })
            await this.interaction.stake(FUND_ID, "1000", {from: signer2});
            await time.increaseTo(Date.UTC(1970, 3, 1) / 1000)

            await this.interaction.drip(FUND_ID, "0", {from: trigger});

            expect((await this.interaction.nextPeriod(FUND_ID))).to.be.equal(expectedPeriod.toString())
        });

        it('sets correct nextPeriod after processing withdrawals', async function() {
            const expectedPeriod = Date.UTC(1970, 0, 19) / 1000;
            await createFund(this.fundFactory, signer1, {
                investPeriod: ONE_WEEK
            });
            let iTokenAddress = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iTokenAddress);
            await this.interaction.stake(FUND_ID, "1", {from: signer2});
            await time.increaseTo(Date.UTC(1970, 0, 5) / 1000);
            await this.interaction.drip(FUND_ID, "0", {from: trigger});

            await time.increaseTo(Date.UTC(1970, 0, 12) / 1000)
            const iTokenBalance = (await this.itoken.balanceOf(signer2)).toString();
            await this.itoken.approve(this.interaction.address, iTokenBalance, {from: signer2});
            await this.interaction.unstake(FUND_ID, iTokenBalance, {from: signer2});
            const receipt = await this.interaction.drip(FUND_ID, "1", {from: trigger});

            expect((await this.interaction.nextPeriod(FUND_ID))).to.be.equal(expectedPeriod.toString());
            const logs = parseEventFromLogs(receipt, "event NextPeriod(uint256 indexed,uint256)");
            expect(logs[0].args[0].toString()).to.equal(FUND_ID);
            expect(logs[0].args[1].toString()).to.equal(expectedPeriod.toString())
        })

        describe('Batched drip', async function() {
            beforeEach(async function() {
                await createFund(this.fundFactory, deployer, {
                    investPeriod: ONE_WEEK,
                    sf: 0,
                    indent: ONE_DAY * 2
                })
                let iToken_address = await this.interaction.tokenForFund(FUND_ID);
                this.itoken = await ERC20Mock.at(iToken_address);
                // cleaning usdt balances
                const investors = await ethers.getSigners()
                for (const investor of investors) {
                    const usdtBalance = await this.usdt.balanceOf(investor.address)
                    await this.usdt.burn(investor.address, usdtBalance)
                    await this.usdt.mint(investor.address, usdc('100').toString())
                    await this.usdt.approve(this.interaction.address, usdc('100').toString(), {from: investor.address});
                }
            })

            it('processes batched drip with deposits and partial withdrawals and increased tvl', async function () {
                const { trade } = await this.interaction.funds(FUND_ID)
                const [_, ...investors] = await ethers.getSigners()
                // first report
                const firstReportInvestors = investors.slice(0, 30)
                const secondReportInvestors = investors.slice(30, 61)
                for (const investor of firstReportInvestors) {
                    await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: investor.address});
                }
                await this.interaction.drip(FUND_ID, "0", {from: trigger});
                await this.interaction.drip(FUND_ID, "0", {from: trigger});

                // second report
                // tvl: 6000 (+100%), pf: 540
                // withdrawals: (tvl - pf) / 2 = 2775
                // token rate: 1.85
                // deposits: 30 * 100
                await this.usdt.mint(trade, await this.usdt.balanceOf(trade))
                for (const investor of firstReportInvestors) {
                    const iTokenBalance = await this.itoken.balanceOf(investor.address);
                    await this.itoken.approve(this.interaction.address, iTokenBalance, {from: investor.address});
                    await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)), {from: investor.address});
                }
                for (const investor of secondReportInvestors) {
                    await this.interaction.stake(FUND_ID, usdc("92.5").toString(), {from: investor.address});
                }
                await this.interaction.drip(FUND_ID, usdc("6000").toString(), {from: trigger});
                await this.interaction.drip(FUND_ID, usdc("6000").toString(), {from: trigger});
                await this.interaction.drip(FUND_ID, usdc("6000").toString(), {from: trigger});

                for (const investor of firstReportInvestors) {
                    const usdtBalance = await this.usdt.balanceOf(investor.address)
                    expect(usdtBalance.toString()).to.equal(usdc('92.5').toString())
                    const itokenBalance = await this.itoken.balanceOf(investor.address)
                    expect(itokenBalance.toString()).to.equal(ether('500').toString())
                }
                for (const investor of secondReportInvestors) {
                    const usdtBalance = await this.usdt.balanceOf(investor.address)
                    expect(usdtBalance.toString()).to.equal(usdc('7.5'))
                    const itokenBalance = await this.itoken.balanceOf(investor.address)
                    expect(itokenBalance.toString()).to.equal(ether('500').toString())
                }
                const tradeBalance = await this.usdt.balanceOf(trade)
                expect(tradeBalance.toString()).to.equal(usdc('5550').toString())
            })

            it('processes batched drip with deposits and partial withdrawals and decreased tvl', async function () {
                const { trade } = await this.interaction.funds(FUND_ID)
                const [_, ...investors] = await ethers.getSigners()
                // first report
                const firstReportInvestors = investors.slice(0, 30)
                const secondReportInvestors = investors.slice(30, 61)
                for (const investor of firstReportInvestors) {
                    await this.interaction.stake(FUND_ID, usdc("100"), {from: investor.address});
                }
                await time.increaseTo(Date.UTC(1970, 0, 5) / 1000);
                await this.interaction.drip(FUND_ID, "0", {from: trigger});
                await this.interaction.drip(FUND_ID, "0", {from: trigger});

                // second report
                // tvl: 1500 (-50%)
                // withdrawals: 30 * (100 * 0.5 (tvl raise) / 2) = 750
                // deposits: 30 * 100
                await this.usdt.burn(trade, (await this.usdt.balanceOf(trade)).div(new BN(2)))
                for (const investor of firstReportInvestors) {
                    const iTokenBalance = await this.itoken.balanceOf(investor.address);
                    await this.itoken.approve(this.interaction.address, iTokenBalance, {from: investor.address});
                    await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)), {from: investor.address});
                }
                for (const investor of secondReportInvestors) {
                    await this.interaction.stake(FUND_ID, usdc("100"), {from: investor.address});
                }
                await time.increaseTo(Date.UTC(1970, 0, 12) / 1000);
                await this.interaction.drip(FUND_ID, usdc("1500"), {from: trigger});
                await this.interaction.drip(FUND_ID, usdc("1500"), {from: trigger});
                await this.interaction.drip(FUND_ID, usdc("1500"), {from: trigger});
                for (const investor of firstReportInvestors) {
                    const usdtBalance = await this.usdt.balanceOf(investor.address)
                    expect(usdtBalance.toString()).to.equal(usdc('25'))
                    const itokenBalance = await this.itoken.balanceOf(investor.address)
                    expect(itokenBalance.toString()).to.equal(ether('500').toString())
                }
                for (const investor of secondReportInvestors) {
                    const usdtBalance = await this.usdt.balanceOf(investor.address)
                    expect(usdtBalance.toString()).to.equal('0')
                    const itokenBalance = await this.itoken.balanceOf(investor.address)
                    expect(itokenBalance.toString()).to.equal(ether('2000').toString())
                }
                const tradeBalance = await this.usdt.balanceOf(trade)
                expect(tradeBalance.toString()).to.equal(usdc('3750').toString())
            })

            it('rejects new investments if drip is in progress', async function () {
                const investors = (await ethers.getSigners()).slice(1, 22);
                for (const investor of investors) {
                    await this.interaction.stake(FUND_ID, "1", {from: investor.address});
                }
                await this.interaction.drip(FUND_ID, "0", {from: trigger});

                await expect(
                    this.interaction.stake(FUND_ID, "1", {from: signer1})
                ).to.be.revertedWith("I/DIP");
            })

            it('rejects new withdrawals if drip is in progress', async function () {
                const investors = (await ethers.getSigners()).slice(1, 22);
                for (const investor of investors) {
                    await this.interaction.stake(FUND_ID, "1", {from: investor.address});
                }
                await this.interaction.drip(FUND_ID, "0", {from: trigger});

                const iTokenBalance = await this.itoken.balanceOf(investors[0].address);
                await this.itoken.approve(this.interaction.address, iTokenBalance, {from: investors[0].address});
                await expect(
                    this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)), {from: investors[0].address})
                ).to.be.revertedWith("I/DIP");
            })

            it('rejects new investment cancellations if drip in progress', async function () {
                const investors = (await ethers.getSigners()).slice(1, 22);
                for (const investor of investors) {
                    await this.interaction.stake(FUND_ID, "1", {from: investor.address});
                }
                await this.interaction.drip(FUND_ID, "0", {from: trigger});

                const lastInvestor = investors[0]
                await expect(this.interaction.cancelDeposit(FUND_ID, {from: lastInvestor.address}))
                    .to.be.revertedWith("I/DIP");
            })

            it('rejects withdrawal cancellations if drip in progress', async function () {
                const investors = (await ethers.getSigners()).slice(1, 22);
                for (const investor of investors) {
                    await this.interaction.stake(FUND_ID, "1", {from: investor.address});
                }
                await this.interaction.drip(FUND_ID, "0", {from: trigger});
                await this.interaction.drip(FUND_ID, "0", {from: trigger});

                for (const investor of investors) {
                    const iTokenBalance = await this.itoken.balanceOf(investors[0].address);
                    await this.itoken.approve(this.interaction.address, iTokenBalance, {from: investor.address});
                    await this.interaction.unstake(FUND_ID, "1", {from: investor.address});
                }
                await this.interaction.drip(FUND_ID, "21", {from: trigger});
                const lastInvestor = investors[0]
                await expect(this.interaction.cancelWithdraw(FUND_ID, {from: lastInvestor.address}))
                    .to.be.revertedWith("I/DIP");
            })
        })
    })

    describe('Indent', async function() {
        it('should process withdraw application in the next report, if it was applied during indent', async function() {
            const indent = String(ONE_DAY * 2);
            await createFund(this.fundFactory, signer1, { indent, sf: '0' });
            const iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});
            this.trading_address = (await this.interaction.funds(FUND_ID))["trade"];

            let balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());
            await this.interaction.stake(FUND_ID, usdc("1000").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            await time.increase(2 * ONE_DAY); // saturday

            let iTokenBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)).toString(), {from: signer1});

            await this.interaction.drip(FUND_ID, usdc("1000").toString(), {from: trigger});
            balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc('0').toString());
            expect((await this.usdt.balanceOf(this.trading_address)).toString()).to.equal(usdc('1000').toString());

            await time.increase(2 * ONE_DAY);

            const users = await this.feeder.userWaitingForWithdrawal(FUND_ID);
            expect(users.length).to.equal(1);
            expect(users[0]).to.equal(signer1);
            await this.interaction.drip(FUND_ID, usdc("1000").toString(), {from: trigger});
            balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc('500').toString());
            expect((await this.usdt.balanceOf(this.trading_address)).toString()).to.equal(usdc('500').toString());
            const result = await this.interaction.pendingDepositAndWithdrawals(FUND_ID, signer1)
            // withdrawals
            expect(result[1].toString()).to.equal('0')
            // indented withdrawals
            expect(result[2].toString()).to.equal('0')

            await time.increase(ONE_WEEK)
            await this.interaction.drip(FUND_ID, usdc("500").toString(), {from: trigger});
            balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc('500').toString());
        });

        it('should process 2 indented withdraw applications', async function() {
            const indent = String(2 * ONE_DAY);
            await createFund(this.fundFactory, signer1, { indent, sf: '0' });
            const iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});
            this.trading_address = (await this.interaction.funds(FUND_ID))["trade"];

            let balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());
            await this.interaction.stake(FUND_ID, usdc("1000").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            await time.increase(2 * ONE_DAY); // saturday

            let iTokenBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)).toString(), {from: signer1});
            await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)).toString(), {from: signer1});

            await this.interaction.drip(FUND_ID, usdc("1000").toString(), {from: trigger});
            balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc('0').toString());
            const tradingBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradingBalance.toString()).to.equal(usdc('1000').toString());

            await time.increase(2 * ONE_DAY);

            const users = await this.feeder.userWaitingForWithdrawal(FUND_ID);
            expect(users.length).to.equal(1);
            expect(users[0]).to.equal(signer1);
            await this.interaction.drip(FUND_ID, usdc("1000").toString(), {from: trigger});
            balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc('1000').toString());
        });

        it('should process both indented and not indented applications', async function() {
            const indent = String(2 * ONE_DAY);
            await createFund(this.fundFactory, signer1, { indent, sf: '0' });
            const iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});
            this.trading_address = (await this.interaction.funds(FUND_ID))["trade"];

            let balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());
            await this.interaction.stake(FUND_ID, usdc("1000").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});
            let iTokenBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)).toString(), {from: signer1});

            await time.increase(2 * ONE_DAY); // saturday

            await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)).toString(), {from: signer1});

            await this.interaction.drip(FUND_ID, usdc("1000").toString(), {from: trigger});
            balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc('500').toString());
            const tradingBalance = await this.usdt.balanceOf(this.trading_address);
            expect(tradingBalance.toString()).to.equal(usdc('500').toString());

            await time.increase(2 * ONE_DAY);

            const users = await this.feeder.userWaitingForWithdrawal(FUND_ID);
            expect(users.length).to.equal(1);
            expect(users[0]).to.equal(signer1);
            await this.interaction.drip(FUND_ID, usdc("500").toString(), {from: trigger});
            balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc('1000').toString());
        });

        it('should cancel both indented and not indented applications', async function() {
            const indent = String(2 * ONE_DAY);
            await createFund(this.fundFactory, signer1, { indent, sf: '0' });
            const iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});
            this.trading_address = (await this.interaction.funds(FUND_ID))["trade"];

            let balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());
            await this.interaction.stake(FUND_ID, usdc("1000").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});
            const iTokenBalance = await this.itoken.balanceOf(signer1);
            await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)).toString(), {from: signer1});

            await time.increase(2 * ONE_DAY); // saturday

            await this.interaction.unstake(FUND_ID, iTokenBalance.div(new BN(2)).toString(), {from: signer1});
            await this.interaction.cancelWithdraw(FUND_ID, {from: signer1});
            expect((await this.itoken.balanceOf(signer1)).toString()).to.equal(iTokenBalance.toString());
        });

        it('gives correct shares while drip when there is indented withdrawals', async function() {
            const indent = String(2 * ONE_DAY);
            await createFund(this.fundFactory, signer1, { indent, sf: '0' });
            const iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.interaction.stake(FUND_ID, usdc("1000").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, '0', {from: trigger});

            await time.increase(2 * ONE_DAY); // saturday
            await this.itoken.approve(this.interaction.address, ether("5000"), {from: signer1})
            await this.interaction.unstake(FUND_ID, ether("5000"), {from: signer1});
            await this.interaction.stake(FUND_ID, usdc("1000"), {from: signer2})

            await time.increase(2 * ONE_DAY);
            await this.interaction.drip(FUND_ID, usdc("1000"), {from: trigger});
            expect((await this.itoken.balanceOf(signer1)).toString()).to.equal(ether("5000").toString())
            expect((await this.itoken.balanceOf(signer2)).toString()).to.equal(ether("10000").toString())
        })

        describe('Indent is disabled in closed/blocked funds', async function() {
            let trading;
            let itoken;
            beforeEach(async function() {
                await this.usdt.burn(signer2, await this.usdt.balanceOf(signer2))
                await this.usdt.mint(signer2, usdc('1'))
                await time.increaseTo(Date.UTC(1970, 0, 5) / 1000) // first monday after 1.01.1970
                await createFund(this.fundFactory, signer1, {
                    investPeriod: ONE_WEEK,
                    indent: 2 * ONE_DAY,
                    sf: 0,
                    pf: 0,
                })
                const iToken_address = await this.interaction.tokenForFund(FUND_ID);
                itoken = await ERC20Mock.at(iToken_address);
                await this.interaction.stake(FUND_ID, usdc("1").toString(), {from: signer2});
                await this.interaction.drip(FUND_ID, '0', {from: trigger});
                await time.increase(ONE_DAY * 5)

                const tradingAddress = (await this.interaction.funds(FUND_ID))["0"];
                trading = await Trade.at(tradingAddress);
            });

            async function checkIndentDisabled() {
                await itoken.approve(this.interaction.address, ether('10'), {from: signer2})
                await this.interaction.unstake(FUND_ID, ether('10'), {from: signer2})
                await this.interaction.drip(FUND_ID, usdc('1'), {from: trigger});
                expect((await this.usdt.balanceOf(signer2)).toString()).to.equal(usdc('1'))
            }

            it('processes indented withdrawals immediately if fund is closed', async function() {
                await trading.setState(0, {from: signer1});
                await checkIndentDisabled.call(this);
            });

            it('processes indented withdrawals immediately if fund is banned', async function() {
                await blockFund(this.dripOperator, this.interaction, trading, signer1, trigger)

                await checkIndentDisabled.call(this);
            });
        });
    });

    describe('Trade utils', async function () {
        beforeEach(async function () {
            await createFund(this.fundFactory, signer1);
            let iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});

            this.trading_address = (await this.interaction.funds(FUND_ID))["trade"];
        });

        it('swapper mock', async function () {
            await this.swapper.swap(this.usdt.address, this.stable.address, usdc("1").toString(), {from: signer1});
            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("999").toString());
            stableBalance = await this.stable.balanceOf(signer1);
            expect(stableBalance.toString()).to.equal(ether("1.5").toString());

            await this.swapper.swap(this.usdt.address, this.increasing.address, usdc("1").toString(), {from: signer1});
            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("998").toString());
            increasingBalance = await this.increasing.balanceOf(signer1);
            expect(increasingBalance.cmp(ether("99"))).to.equal(1);
            expect(increasingBalance.cmp(ether("100"))).to.equal(-1);

            await this.swapper.swap(this.usdt.address, this.decreasing.address, usdc("1").toString(), {from: signer1});
            usdtBalance = await this.usdt.balanceOf(signer1);
            expect(usdtBalance.toString()).to.equal(usdc("997").toString());
            decreasingBalance = await this.decreasing.balanceOf(signer1);
            expect(decreasingBalance.cmp(ether("100"))).to.equal(1);
            expect(decreasingBalance.cmp(ether("101"))).to.equal(-1);

            await time.increase(ONE_WEEK);

            await this.swapper.swap(this.usdt.address, this.increasing.address, usdc("1").toString(), {from: signer2});
            increasingBalance = await this.increasing.balanceOf(signer2);
            expect(increasingBalance.cmp(ether("97"))).to.equal(1);
            expect(increasingBalance.cmp(ether("98"))).to.equal(-1);

            await this.swapper.swap(this.usdt.address, this.decreasing.address, usdc("1").toString(), {from: signer2});
            decreasingBalance = await this.decreasing.balanceOf(signer2);
            expect(decreasingBalance.cmp(ether("300"))).to.equal(1);
            expect(decreasingBalance.cmp(ether("301"))).to.equal(-1);
        });
    });

    describe('Trading', async function () {
        var tokenA
        const tokenB = "0x2f2a2543B76A4166549F7aaB2e75Bef0aefC5B0f"
        const tokenC = "0xFF970A61A04b1cA14834A43f5dE4533eBDDB5CC8"
        var trading

        beforeEach(async function () {
            await this.whitelist.addToken(tokenB, {from: deployer})
            await this.whitelist.addToken(tokenC, {from: deployer})
            await createFund(this.fundFactory, signer1, {
                whitelistMask: '0x' + Buffer.from([0b11]).toString('hex')
            })
            tokenA = await ERC20Mock.new("A", "A", USDT_DECIMALS, {from: deployer});
            let iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});
            this.trading_address = (await this.interaction.funds(FUND_ID))["trade"];
            trading = await Trade.at(this.trading_address);
        });

        it('assets sizes', async function () {
            const trading = await Trade.at(this.trading_address);
            let sizes = await trading.getAssetsSizes([this.usdt.address, this.stable.address]);
            expect(sizes[0].toString()).to.equal('0')
            expect(sizes[1].toString()).to.equal('0');

            await this.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
            await this.interaction.drip(FUND_ID, "0", {from: trigger});

            sizes = await trading.getAssetsSizes([this.usdt.address, this.stable.address]);
            expect(sizes[0].toString()).to.equal(usdc("95").toString())
            expect(sizes[1].toString()).to.equal('0');

            // TODO: swap usdt => stable
        });
    });

    describe('Estimate withdrawal amount', async function () {
        beforeEach(async function() {
            await createFund(this.fundFactory, signer1, { hwm: true, sf: '0' });
            let iToken_address = await this.interaction.tokenForFund(FUND_ID);
            this.itoken = await ERC20Mock.at(iToken_address);
            await this.itoken.approve(this.interaction.address, ether("1000000").toString(), {from: signer1});

            await this.interaction.stake(FUND_ID, usdc("1000"), {from: signer1});
            await this.interaction.drip(FUND_ID, "0", {from: trigger});
        });

        it('Should equal to real withdraw amount for full withdraw', async function() {
            let iTokenBalance = await this.itoken.balanceOf(signer1);
            const estimatedWithdrawAmount = await this.interaction.estimatedWithdrawAmount(FUND_ID, usdc("1000"), iTokenBalance);
            expect(estimatedWithdrawAmount.toString()).to.equal(usdc("1000").toString());
            await this.interaction.unstake(FUND_ID, iTokenBalance, {from: signer1});
            await this.interaction.drip(FUND_ID, usdc("1000"), {from: trigger});

            const balance = await this.usdt.balanceOf(signer1);
            expect(balance.toString()).to.equal(usdc("1000").toString());
        });

        // TODO: estimate for partial withdraw
        // TODO: estimate when PF is not 0
    })
});
