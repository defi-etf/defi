
import { TradeInstance } from "../typechain-types/contracts/main/Trade.sol/Trade"
import BN from 'bn.js'
import { time } from '@nomicfoundation/hardhat-network-helpers'
import { expect } from 'chai'
import { ethers, upgrades } from "hardhat"
import { FUND_ID, MIN_UPGRADE_PERIOD, ONE_DAY, ONE_WEEK, UPGRADE_PERIOD } from './constants';
import { Contracts, setup } from "./setup"
import { createFund, ether, usdc } from './utils';
const ERC20Mock = artifacts.require('ERC20Mock')

contract('Upgrader', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
    let contracts: Contracts
    let newImplementation: TradeInstance

    beforeEach(async function() {
        contracts = (await setup(deployer, signer1, signer2, trigger, signer3)).contracts
        const ZeroX = await ethers.getContractFactory('ZeroX').then(l => l.deploy())
        const AAVE = await ethers.getContractFactory('AAVE').then(l => l.deploy())
        const GMXV2 = await ethers.getContractFactory('GMXV2').then(l => l.deploy())
        newImplementation = await ethers.getContractFactory('Trade', {
            libraries: {
                ZeroX: ZeroX.address,
                AAVE: AAVE.address,
                GMXV2: GMXV2.address
            }
        }).then(t => t.deploy())
        await contracts.usdt.burn(signer1, await contracts.usdt.balanceOf(signer1))
        await contracts.usdt.mint(signer1, usdc('1'))
    })

    it('upgrades trade contracts.beacon after the upgrade period', async function () {
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        await time.increase(UPGRADE_PERIOD)
        await contracts.upgrader.upgrade()

        expect(await contracts.beacon.implementation()).to.equal(newImplementation.address)
    })

    it('cleans current upgrade after execution', async function() {
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        await time.increase(UPGRADE_PERIOD)
        await contracts.upgrader.upgrade()

        expect((await contracts.upgrader.pendingUpgrades()).length).to.equal(0)
        expect((await contracts.upgrader.nextUpgradeDate()).toString()).to.equal('0')
    })

    it('upgrades multiple contracts at once', async function() {
        const factory = await ethers.getContractFactory('UpgradeableMock')
        let implementationA2 = await factory.deploy()
        let implementationB2 = await factory.deploy()
        const proxyA = await upgrades.deployProxy(factory)
        const proxyB = await upgrades.deployProxy(factory)
        await contracts.upgrader.requestUpgrade([
            { destination: proxyA.address, implementation: implementationA2.address },
            { destination: proxyB.address, implementation: implementationB2.address },
        ])
        await time.increase(UPGRADE_PERIOD)
        await contracts.upgrader.upgrade()

        expect(await upgrades.erc1967.getImplementationAddress(proxyA.address)).to.equal(implementationA2.address)
        expect(await upgrades.erc1967.getImplementationAddress(proxyB.address)).to.equal(implementationB2.address)
    })

    it('emits event when upgrade requested', async function () {
        const receipt = await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        const logs = receipt.logs

        expect(logs[0].args[0].length).to.equal(1)
        expect(logs[0].args[0][0].destination).to.equal(contracts.beacon.address)
        expect(logs[0].args[0][0].implementation).to.equal(newImplementation.address)
        expect(logs[0].args[1].toString()).to.equal(UPGRADE_PERIOD.toString())
    })

    it('doesnt allow to request upgrade with incorrect address', async function() {
        await expect(contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: ethers.constants.AddressZero
        }])).to.revertedWith('TU/II')
    })

    it('doesnt allow to requset upgrade from other addreess than the owner', async function () {
        await expect(contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }], {from: signer1}))
            .to.revertedWith('Ownable: caller is not the owner')
    })

    it('doesnt allow to upgrade from other address than the owner', async function () {
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        await time.increase(UPGRADE_PERIOD)

        await expect(contracts.upgrader.upgrade({from: signer1}))
            .to.revertedWith('Ownable: caller is not the owner')
    })

    it('doesnt allow to upgrade before upgrade period ends', async function () {
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])

        await expect(contracts.upgrader.upgrade())
            .to.revertedWith('TU/UPNE')
    })

    it('doesnt allow to upgrade untill there is unprocessed withdrawals', async function() {
        await createFund(contracts.fundFactory, signer1, {
            investPeriod: ONE_WEEK,
            indent: 0,
        });
        const itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID))
        await contracts.usdt.approve(contracts.interaction.address, 1, {from: signer1})
        await contracts.interaction.stake(FUND_ID, 1, {from: signer1})
        await contracts.interaction.drip(FUND_ID, 0, {from: trigger})
        await itoken.approve(contracts.interaction.address, 10, {from: signer1})
        await contracts.interaction.unstake(FUND_ID, 10, {from: signer1})

        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        await time.increase(UPGRADE_PERIOD)

        await expect(contracts.upgrader.upgrade())
            .to.revertedWith('TU/UW')
    })

    it('disables indent for withdrawals if upgrade date is earlier thant fund report date', async function() {
        await time.increase(ONE_DAY * 4) // moving to the first monday
        await createFund(contracts.fundFactory, signer1, {
            investPeriod: ONE_WEEK,
            indent: 2 * ONE_DAY,
            sf: '0',
        });
        await time.increase(ONE_WEEK - ONE_DAY)
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        await time.increase(ONE_DAY)
        const itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID))
        await contracts.usdt.approve(contracts.interaction.address, usdc('1'), {from: signer1})
        await contracts.interaction.stake(FUND_ID, usdc('1'), {from: signer1})
        await contracts.interaction.drip(FUND_ID, usdc('1'), {from: trigger})
        await time.increase(ONE_WEEK)
        await contracts.interaction.drip(FUND_ID, usdc('1'), {from: trigger})
        await time.increase(5 * ONE_DAY)
        await itoken.approve(contracts.interaction.address, ether('10'), {from: signer1})
        await contracts.interaction.unstake(FUND_ID, ether('10'), {from: signer1})

        await time.increase(ONE_DAY)
        await contracts.interaction.drip(FUND_ID, usdc('1'), {from: trigger})
        
        await expect(contracts.upgrader.upgrade()).to.not.revertedWith('TU/UW')
        expect((await contracts.usdt.balanceOf(signer1)).toString()).to.equal(usdc('1'))
    })

    it('doesnt allow to unstake untill upgrade is finished', async function() {
        await createFund(contracts.fundFactory, signer1, {
            investPeriod: ONE_WEEK,
            indent: 0,
        });
        const itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID))
        await contracts.usdt.approve(contracts.interaction.address, 1, {from: signer1})
        await contracts.interaction.stake(FUND_ID, 1, {from: signer1})
        await contracts.interaction.drip(FUND_ID, 0, {from: trigger})
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])

        await time.increase(UPGRADE_PERIOD)
        await itoken.approve(contracts.interaction.address, 10, {from: signer1})

        await expect(contracts.interaction.unstake(FUND_ID, 10, {from: signer1}))
            .to.revertedWith('I/UIP')
    })

    it('allows to unstake after upgrade', async function() {
        await createFund(contracts.fundFactory, signer1, {
            investPeriod: ONE_WEEK,
            indent: 0,
        });
        const itoken = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID))
        await contracts.usdt.approve(contracts.interaction.address, 1, {from: signer1})
        await contracts.interaction.stake(FUND_ID, 1, {from: signer1})
        await contracts.interaction.drip(FUND_ID, 0, {from: trigger})
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        await time.increase(UPGRADE_PERIOD)
        await contracts.upgrader.upgrade()
        await itoken.approve(contracts.interaction.address, 10, {from: signer1})

        await expect(contracts.interaction.unstake(FUND_ID, 10, {from: signer1}))
            .to.not.revertedWith('I/UIP')
    })

    it('cancels upgrade', async function() {
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
      
        await contracts.upgrader.cancelUpgrade()

        expect((await contracts.upgrader.nextUpgradeDate()).toString()).to.equal('0')
        expect((await contracts.upgrader.pendingUpgrades()).length).to.equal(0)
    })

    it('doesnt allow to cancel upgrade from other address than owner', async function() {
        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
      
        await expect(contracts.upgrader.cancelUpgrade({from: signer1}))
            .to.revertedWith('Ownable: caller is not the owner')
    })

    it('allows to set upgrade period', async function() {
        const newUpgradePeriod = UPGRADE_PERIOD * 2
        await contracts.upgrader.setUpgradePeriod(newUpgradePeriod)

        await contracts.upgrader.requestUpgrade([{
            destination: contracts.beacon.address, implementation: newImplementation.address
        }])
        await time.increase(newUpgradePeriod)

        await expect(contracts.upgrader.upgrade())
            .to.not.revertedWith('TU/UPNE')
    })

    it('doesnt allow to set upgrade period from other address than owner', async function() {
        await expect(contracts.upgrader.setUpgradePeriod(0, {from: signer1}))
            .to.revertedWith('Ownable: caller is not the owner')
    })

    it('doesnt allow to set upgrade period less than minimal', async function () {
        await expect(contracts.upgrader.setUpgradePeriod(new BN(MIN_UPGRADE_PERIOD).subn(1)))
            .to.revertedWith('TU/UPS')
    })
})
