import { Contracts, setup } from './setup';
import { FundOpts, blockFund, createFund, ether, usdc } from './utils';
import { ETH_PRICE, EXECUTION_FEE, FUND_ID, FundState, GAS_PRICE, ONE_DAY, ONE_WEEK } from './constants';
import { expect } from 'chai';
import BN from 'bn.js';
import { ERC20Instance } from '../typechain-types/@openzeppelin/contracts/token/ERC20/ERC20';
import { reset, time } from '@nomicfoundation/hardhat-network-helpers';
import { ethers } from 'hardhat';
import { TradeInstance } from '../typechain-types/contracts/main/Trade.sol/Trade';

const ERC20Mock = artifacts.require('ERC20Mock');
const Trade = artifacts.require('Trade');

contract('Interaction', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
	let contracts: Contracts;
	let itkn: ERC20Instance;
	let trade: TradeInstance;

	async function setupFund(opts: Partial<FundOpts> = {}) {
		await createFund(contracts.fundFactory, signer1, opts);
		itkn = await ERC20Mock.at(await contracts.interaction.tokenForFund(opts.id ?? FUND_ID));
		await contracts.usdt.approve(contracts.interaction.address, usdc('1000'));
		await itkn.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer1 });
		trade = await Trade.at((await contracts.interaction.funds(opts.id ?? FUND_ID))['0']);
	}

	beforeEach(async function () {
		await reset();
		const init = await setup(deployer, signer1, signer2, trigger, signer3);
		contracts = init.contracts;
	});

	describe('Dripping', async function() {
		it('subtracts funds from deposits if there is not enough to cover withdrawals', async function () {
			await setupFund({ sf: '0' })
			const trade = (await contracts.interaction.funds(FUND_ID))['0']
			const [_, ...investors] = await ethers.getSigners()
			for (const investor of investors) {
				await contracts.usdt.burn(investor.address, await contracts.usdt.balanceOf(investor.address))
				await contracts.usdt.mint(investor.address, usdc("100"))
				await contracts.usdt.approve(contracts.interaction.address, usdc("100"), {from: investor.address})
			}
			// first report
			const firstReportInvestors = investors.slice(0, 30)
			const secondReportInvestors = investors.slice(30, 61)
			for (const investor of firstReportInvestors) {
				await contracts.interaction.stake(FUND_ID, usdc("100"), {from: investor.address});
			}
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});

			// second report
			// tvl: 3000 but half of tvl locked in positions
			// withdrawals: 30 * 100
			// deposits: 30 * 100
			await contracts.usdt.burn(trade, (await contracts.usdt.balanceOf(trade)).div(new BN(2)))
			for (const investor of firstReportInvestors) {
				const iTokenBalance = await itkn.balanceOf(investor.address);
				await itkn.approve(contracts.interaction.address, iTokenBalance, {from: investor.address});
				await contracts.interaction.unstake(FUND_ID, iTokenBalance, {from: investor.address});
			}
			for (const investor of secondReportInvestors) {
				await contracts.interaction.stake(FUND_ID, usdc("100"), {from: investor.address});
			}
			await contracts.interaction.drip(FUND_ID, usdc("3000"), {from: trigger});
			await contracts.interaction.drip(FUND_ID, usdc("3000"), {from: trigger});
			await contracts.interaction.drip(FUND_ID, usdc("3000"), {from: trigger});
			for (const investor of firstReportInvestors) {
				const usdtBalance = await contracts.usdt.balanceOf(investor.address)
				expect(usdtBalance.toString()).to.equal(usdc('100'))
				const itokenBalance = await itkn.balanceOf(investor.address)
				expect(itokenBalance.toString()).to.equal(ether('0').toString())
			}
			for (const investor of secondReportInvestors) {
				const usdtBalance = await contracts.usdt.balanceOf(investor.address)
				expect(usdtBalance.toString()).to.equal('0')
				const itokenBalance = await itkn.balanceOf(investor.address)
				expect(itokenBalance.toString()).to.equal(ether('1000').toString())
			}
			const tradeBalance = await contracts.usdt.balanceOf(trade)
			expect(tradeBalance.toString()).to.equal(usdc('1500').toString())
		})
	})

	describe('Unstake', async function () {
		it('unstake from closed fund with execution fee', async function () {
			await setupFund()
			await contracts.ethPriceFeed.setLatestAnswer(ETH_PRICE);
			await contracts.interaction.stake(FUND_ID, usdc('1000'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
			await trade.setState(0, { from: signer1 });

			await contracts.interaction.unstake(FUND_ID, await itkn.balanceOf(signer1), { from: signer1 });
			const tvl = usdc('950').sub(EXECUTION_FEE);
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });

			expect(await itkn.balanceOf(signer1)).to.equal(ether('0'));
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(tvl.sub(EXECUTION_FEE));
		});

		it('allows to cancel indented withdrawals', async function() {
			await setupFund({
				investPeriod: ONE_WEEK,
				indent: ONE_DAY
			})
			await contracts.interaction.stake(FUND_ID, usdc('1000'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
			await itkn.approve(contracts.interaction.address, await itkn.balanceOf(signer1), { from: signer1 })
			await time.increaseTo(Date.UTC(1970, 0, 4) / 1000); // first sunday
			await contracts.interaction.unstake(FUND_ID, await itkn.balanceOf(signer1), { from: signer1 });
			await contracts.interaction.cancelWithdraw(FUND_ID, { from: signer1 })

			const { 1: withdrawals, 2: indentedWithdrawals } = await contracts.interaction.pendingDepositAndWithdrawals(1, signer1)
			expect(withdrawals.toString()).to.equal('0')
			expect(indentedWithdrawals.toString()).to.equal('0')
		})
	});

	describe('Pending tvl', async function () {
		beforeEach(setupFund)

		it('should work for case with 0 tvl and existing pending deposit', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 });

			const tvl = await contracts.interaction.pendingTvl([FUND_ID], [0], GAS_PRICE);
			expect(tvl[0].mustBePaid).to.equal('0');
		});

		it('should work for case with 0 tvl and existing small pending deposit', async function () {
			await contracts.interaction.stake(FUND_ID, EXECUTION_FEE.divn(2), { from: signer1 });

			const tvl = await contracts.interaction.pendingTvl([FUND_ID], [0], GAS_PRICE);
			expect(tvl[0].mustBePaid).to.equal('0');
		});
	});

	describe('Private funds', async function () {
		it('should allow to invest to private fund only for fund\'s owner', async function () {
			await createFund(contracts.fundFactory, signer1, { isPrivate: true });
			await expect(contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer2 })).to.revertedWith("I/FIP");
			await expect(contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 })).not.to.be.reverted;
		});

		it('owner should be able to change fund\'s privacy settings', async function () {
			await createFund(contracts.fundFactory, signer1, { manager: signer2, isPrivate: false });
			expect(await contracts.interaction.isPrivate(FUND_ID)).to.equal(false);
			await expect(contracts.interaction.setIsPrivate(FUND_ID, true, { from: trigger })).to.revertedWith('I/AD');
			await expect(contracts.interaction.setIsPrivate(FUND_ID, true, { from: signer2 })).to.revertedWith('I/AD');
			await contracts.interaction.setIsPrivate(FUND_ID, true, { from: signer1 });
		});

		it('investor is still able to unstake after fund became private', async function () {
			await setupFund({ manager: signer2, isPrivate: false, sf: '0' });

			const tvl = usdc('100');
			const initialBalance = await contracts.usdt.balanceOf(signer1);
			await contracts.interaction.stake(FUND_ID, tvl, { from: signer1 });
			await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
			await contracts.interaction.setIsPrivate(FUND_ID, true, { from: signer1 });

			await contracts.interaction.unstake(FUND_ID, await itkn.balanceOf(signer1), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance);
		});
	});

	describe('Closed funds', async function () {
		

		it('should not allow to invest in closed or blocked fund', async function () {
			await setupFund({ manager: signer2 });
			await trade.setState(FundState.Closed, { from: signer1 });
			await expect(contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 })).to.revertedWith('I/FC');
			await blockFund(contracts.dripOperator, contracts.interaction, trade, signer1, trigger)
			await expect(contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 })).to.revertedWith('I/FC');
		});

		async function checkWithdraw(closeFund: () => Promise<unknown>): Promise<void> {
			const initialBalance = await contracts.usdt.balanceOf(signer1);
			const tvl = usdc('100');
			await contracts.interaction.stake(FUND_ID, tvl, { from: signer1 });
			await contracts.interaction.drip(FUND_ID, 0, { from: trigger });

			await closeFund();

			await contracts.interaction.unstake(FUND_ID, await itkn.balanceOf(signer1), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance);
		}

		it('should be still possible to withdraw, even after fund was closed', async function () {
			await setupFund({ manager: signer2, sf: '0' });
			await checkWithdraw(() => trade.setState(FundState.Closed, { from: signer1 }));
		});

		it('should be still possible to withdraw, even after fund was blocked', async function () {
			await setupFund({ manager: signer2, sf: '0' });
			await checkWithdraw(() => blockFund(contracts.dripOperator, contracts.interaction, trade, signer1, trigger));
		});
	});
});