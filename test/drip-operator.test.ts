import { expect } from 'chai';
import { Contracts, setup } from './setup';
import { ETH_PRICE, EXECUTION_FEE, FUND_ID, GAS_PRICE, REPORT_DELAY } from './constants';
import { createFund, ether, FundOpts, parseEventFromLogs, usdc, expectTrailingStop } from './utils';
import { ERC20Instance } from '../typechain-types/@openzeppelin/contracts/token/ERC20/ERC20';
import { ethers } from 'hardhat';
import { time } from '@nomicfoundation/hardhat-network-helpers';
import { TradeInstance } from '../typechain-types/contracts/main/Trade.sol/Trade';

const ERC20Mock = artifacts.require('ERC20Mock');
const Trade = artifacts.require('Trade');

contract('DripOperator', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
	let contracts: Contracts;
	let tradingAddress: string;
	let itkn: ERC20Instance;

	async function init(reportDelay: number, fundOpts: FundOpts = {}, ethPrice: number = ETH_PRICE) {
		const init = await setup(deployer, signer1, signer2, trigger, signer3, reportDelay);
		contracts = init.contracts;
		await createFund(contracts.fundFactory, signer1, { sf: '0', pf: '100000000000000000', ...fundOpts });
		tradingAddress = (await contracts.interaction.funds(FUND_ID))['0'];
		await contracts.ethPriceFeed.setLatestAnswer(ethPrice);
		itkn = await ERC20Mock.at(await contracts.interaction.tokenForFund(FUND_ID));
		await itkn.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer1 });
		await itkn.approve(contracts.interaction.address, ether('1000000').toString(), { from: signer2 });
		const investors = (await ethers.getSigners()).slice(1, 22);
		for (const investor of investors) {
			await contracts.usdt.mint(investor.address, ether('10000000000000'), { from: deployer });
			await contracts.usdt.approve(contracts.interaction.address, ether('10000000000000'), { from: investor.address });
			await itkn.approve(contracts.interaction.address, ether('10000000000000'), { from: investor.address });
		}
	}

	describe('Report Delay', async function() {
		beforeEach(async function () {
			await init(REPORT_DELAY);
		});

		it('should revert if not enough time passed during the last report', async function () {
			await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
			await expect(contracts.interaction.drip(FUND_ID, 0, { from: trigger })).to.be.revertedWith("DO/DNE");
		});

		it('should report normally after report delay expired', async function () {
			await contracts.interaction.drip(FUND_ID, 0, { from: trigger });
			await time.increase(REPORT_DELAY);
			await expect(contracts.interaction.drip(FUND_ID, 0, { from: trigger })).not.to.be.reverted;
		});
	});

	describe('HWM', async function () {
		it('HWM is on - performance fee should not be gathered, if token rate is less than stored HWM value', async function () {
			await init(0, { hwm: true });

			await contracts.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});
			await contracts.usdt.mint(tradingAddress, usdc("100").toString(), {from: deployer}); // +100% profit
			await contracts.interaction.drip(FUND_ID, usdc("200").toString(), {from: trigger});
			await contracts.usdt.burn(tradingAddress, usdc("100").toString(), {from: deployer}); // lose all profit
			await contracts.interaction.drip(FUND_ID, usdc("100").toString(), {from: trigger});
			await contracts.usdt.mint(tradingAddress, usdc("50").toString(), {from: deployer}); // again +50% profit

			const receipt = await contracts.interaction.drip(FUND_ID, usdc("150").toString(), {from: trigger});
			const logs = parseEventFromLogs(receipt, "event PfCharged(uint256 indexed,uint256)");
			expect(logs[0]).to.equal(undefined);
		});

		it('HWM is off - performance fee is only dependent from previous report results', async function () {
			await init(0, { hwm: false, pf: '150000000000000000' }, 0);

			await contracts.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});
			await contracts.usdt.mint(tradingAddress, usdc("100").toString(), {from: deployer}); // +100% profit
			await contracts.interaction.drip(FUND_ID, usdc("200").toString(), {from: trigger});
			await contracts.usdt.burn(tradingAddress, usdc("100").toString(), {from: deployer}); // lose all profit
			await contracts.interaction.drip(FUND_ID, usdc("100").toString(), {from: trigger});
			await contracts.usdt.mint(tradingAddress, usdc("50").toString(), {from: deployer}); // again +50% profit

			const receipt = await contracts.interaction.drip(FUND_ID, usdc("150").toString(), {from: trigger});
			const logs = parseEventFromLogs(receipt, "event PfCharged(uint256 indexed,uint256)");
			expect(logs[0]).not.to.equal(undefined);
			expect(logs[0].args[1].toString()).to.equal(usdc("7.5").toString()); // 15% of 50 USDT
		});

		it('HWM should be saved before charging EF', async function () {
			await init(0, { hwm: false });

			await contracts.interaction.stake(FUND_ID, usdc("100"), {from: signer1});
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});
			expect(await contracts.interaction.hwmValue(FUND_ID)).to.equal(usdc('0.1'));
		});

		it('HWM is on - value is never below 0%', async function () {
			await init(0, { hwm: true });

			await contracts.interaction.stake(FUND_ID, usdc("100"), {from: signer1});
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});
			expect(await contracts.interaction.hwmValue(FUND_ID)).to.equal(usdc('0.1'));
			await contracts.interaction.drip(FUND_ID, usdc("100").sub(EXECUTION_FEE), {from: trigger});
			await contracts.interaction.drip(FUND_ID, usdc("100").sub(EXECUTION_FEE.muln(2)), {from: trigger});
			await contracts.interaction.drip(FUND_ID, usdc("100").sub(EXECUTION_FEE.muln(3)), {from: trigger});
			expect(await contracts.interaction.hwmValue(FUND_ID)).to.equal(usdc('0.1'));
		});
	});

	describe('Charge gas cost from fund', async function () {
		beforeEach(async function () {
			await init(0);
		});

		it('should drip normally if tvl < gas cost', async function () {
			await contracts.interaction.drip(FUND_ID, EXECUTION_FEE.subn(2), { from: trigger });
			await expect(contracts.interaction.drip(FUND_ID, EXECUTION_FEE.subn(2), { from: trigger })).not.to.be.reverted;
			await expect(contracts.interaction.drip(FUND_ID, usdc('0'), { from: trigger })).not.to.be.reverted;
		});

		it('should gather executionFee from deposits if tvl is 0', async function () {
			const tvl = usdc('1000');
			await contracts.interaction.stake(FUND_ID, tvl, { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			const balance = await contracts.usdt.balanceOf(tradingAddress);
			expect(balance).to.equal(tvl.sub(EXECUTION_FEE));
		});

		it('should gather executionFee from deposits, even if tvl is not 0', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('1000'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });

			const balance = await contracts.usdt.balanceOf(tradingAddress);
			expect(balance.toString()).to.equal(usdc('1000').sub(EXECUTION_FEE));
		});

		it('should split executionFee both on old and new investors', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });

			await contracts.interaction.stake(FUND_ID, usdc('100').sub(EXECUTION_FEE), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, usdc('100').sub(EXECUTION_FEE), { from: trigger });

			const finalTvl = usdc('200').sub(EXECUTION_FEE.muln(3));
			const signer1Tvl = usdc('100').sub(EXECUTION_FEE.muln(15).divn(10));
			const signer2Tvl = usdc('100').sub(EXECUTION_FEE.muln(15).divn(10));
			expect(await contracts.interaction.userTVL(FUND_ID, finalTvl, signer1)).to.equal(signer1Tvl);
			expect(await contracts.interaction.userTVL(FUND_ID, finalTvl, signer2)).to.equal(signer2Tvl);
		});

		it('should gather executionFee multiple times', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			await contracts.interaction.drip(FUND_ID, usdc('100'), { from: trigger });
			await contracts.interaction.drip(FUND_ID, usdc('100'), { from: trigger });
			await contracts.interaction.drip(FUND_ID, usdc('100'), { from: trigger });

			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, usdc('100'), { from: trigger });

			const balance = await contracts.usdt.balanceOf(tradingAddress);
			const gatheredFees = await contracts.fees.gatheredServiceFees();
			const executionFee = EXECUTION_FEE.muln(5);
			expect(balance).to.equal(usdc('200').sub(executionFee));
			expect(gatheredFees).to.equal(executionFee);
		});

		it('should take EF normally both from deposits and withdrawals', async function () {
			const initialBalance = await contracts.usdt.balanceOf(signer1);
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 });
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });

			const tvl = usdc('200').sub(EXECUTION_FEE);
			expect(await contracts.fees.gatheredServiceFees()).to.equal(EXECUTION_FEE);
			expect(await contracts.usdt.balanceOf(tradingAddress)).to.equal(tvl);

			const itknBalance = await itkn.balanceOf(signer1);
			await contracts.interaction.unstake(FUND_ID, itknBalance, { from: signer1 });
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer2 });
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });

			expect(await contracts.fees.gatheredServiceFees()).to.equal(EXECUTION_FEE.muln(2));
			expect(await contracts.usdt.balanceOf(contracts.feeder.address)).to.equal(0);
			expect(await contracts.usdt.balanceOf(tradingAddress)).to.equal(tvl);
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.sub(EXECUTION_FEE));
		});

		it('should be gathered after pf', async function () {
			const investmentAmount = usdc('100');
			await contracts.interaction.stake(FUND_ID, investmentAmount, { from: signer1 });
			await contracts.interaction.stake(FUND_ID, investmentAmount, { from: signer2 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			const tvl = usdc('200').sub(EXECUTION_FEE);
			expect(await contracts.usdt.balanceOf(tradingAddress)).to.equal(tvl);

			const newTvl = usdc('400');
			await contracts.usdt.mint(tradingAddress, newTvl.sub(tvl));
			expect(await contracts.usdt.balanceOf(tradingAddress)).to.equal(newTvl);
			const receipt = await contracts.interaction.drip(FUND_ID, newTvl, { from: trigger });

			const expectedPf = usdc('20'); // 10% of 200$
			const pfEvent = parseEventFromLogs(receipt, "event PfCharged(uint256 indexed,uint256)")[0];
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(expectedPf);
			expect(await contracts.usdt.balanceOf(tradingAddress)).to.equal(newTvl.sub(expectedPf).sub(EXECUTION_FEE));
			expect(pfEvent).not.to.equal(undefined);
			expect(pfEvent.args[1].toString()).to.equal(expectedPf);
			expect(await contracts.fees.gatheredServiceFees()).to.equal(EXECUTION_FEE.muln(2));
			expect(await contracts.fees.fundBalance(FUND_ID)).to.equal(expectedPf);
		});

		it('should be included in mustBePaid, if not enough usdt on balance', async function () {
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			const tvl = await contracts.usdt.balanceOf(tradingAddress);
			await contracts.stable.mint(tradingAddress, usdc('10000000'));
			await contracts.usdt.burn(tradingAddress, tvl);
			const tvls = await contracts.interaction.pendingTvl([FUND_ID], [usdc('100')], GAS_PRICE);
			const mustBePaid = tvls[0].mustBePaid;
			expect(mustBePaid).to.equal(EXECUTION_FEE);
		});

		it('should not be included in mustBePaid for 0 tvl and deposits', async function () {
			const tvls = await contracts.interaction.pendingTvl([FUND_ID], [0], GAS_PRICE);
			const mustBePaid = tvls[0].mustBePaid;
			expect(mustBePaid).to.equal('0');
		});

		it('should gather execution fee in case of partial withdraw', async function () {
			const initialBalance = await contracts.usdt.balanceOf(signer1);
			await contracts.interaction.stake(FUND_ID, usdc('100'), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });

			const iTokenBalance = await itkn.balanceOf(signer1);
			await contracts.interaction.unstake(FUND_ID, iTokenBalance.divn(2), { from: signer1 });
			await contracts.interaction.drip(FUND_ID, usdc('100').sub(EXECUTION_FEE), { from: trigger });
			expect(await contracts.fees.gatheredServiceFees()).to.equal(EXECUTION_FEE.muln(2));
			const remainedTvl = usdc('50').sub(EXECUTION_FEE);
			expect(await contracts.usdt.balanceOf(tradingAddress)).to.equal(remainedTvl);
			expect(await contracts.interaction.userTVL(FUND_ID, remainedTvl, signer1)).to.equal(remainedTvl);
			expect(await contracts.usdt.balanceOf(signer1)).to.equal(initialBalance.sub(usdc('50')).sub(EXECUTION_FEE));
		});

		it('should gather execution fee in case of full withdraw', async function () {
			const investors = (await ethers.getSigners()).slice(1, 22);
			for (const investor of investors) {
				await contracts.interaction.stake(FUND_ID, usdc('100'), { from: investor.address });
			}
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });

			const tvl = usdc('2100').sub(EXECUTION_FEE.muln(2));
			const tradingBalance = await contracts.usdt.balanceOf(tradingAddress);
			expect(tradingBalance).to.equal(tvl);
			expect(await contracts.fees.gatheredServiceFees()).to.equal(EXECUTION_FEE.muln(2))
			await contracts.stable.mint(tradingAddress, tvl);
			await contracts.usdt.burn(tradingAddress, tvl);

			let tvls = await contracts.interaction.pendingTvl([FUND_ID], [tvl], GAS_PRICE);
			let mustBePaid = tvls[0].mustBePaid;
			expect(mustBePaid).to.equal(EXECUTION_FEE);

			for (const investor of investors) {
				const itknBalance = await itkn.balanceOf(investor.address);
				await contracts.interaction.unstake(FUND_ID, itknBalance, { from: investor.address });
			}

			tvls = await contracts.interaction.pendingTvl([FUND_ID], [tvl], GAS_PRICE);
			mustBePaid = tvls[0].mustBePaid;
			const toWithdraw = tvls[0].withdraw;
			expect(mustBePaid).to.equal(tvl);
			expect(toWithdraw).to.equal(tvl.sub(EXECUTION_FEE.muln(2)));

			// force close position in stable token
			await contracts.stable.burn(tradingAddress, tvl);
			await contracts.usdt.mint(tradingAddress, tvl);

			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });
			await contracts.interaction.drip(FUND_ID, tvl, { from: trigger });

			expect(await contracts.fees.gatheredServiceFees()).to.equal(EXECUTION_FEE.muln(4));
		});

		it('should gather executionFee 4 times if there are 4 batches', async function () {
			const investors = (await ethers.getSigners()).slice(1, 22);
			for (const investor of investors) {
				await contracts.interaction.stake(FUND_ID, usdc('100'), { from: investor.address });
			}

			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			for (const investor of investors) {
				const iTokenBalance = await itkn.balanceOf(investor.address);
				await contracts.interaction.unstake(FUND_ID, iTokenBalance, { from: investor.address });
			}
			await contracts.usdt.mint(tradingAddress, usdc('100'));
			await contracts.interaction.drip(FUND_ID, usdc('2100'), { from: trigger });
			await contracts.interaction.drip(FUND_ID, usdc('2100'), { from: trigger });

			const gatheredFees = await contracts.fees.gatheredServiceFees();
			expect(gatheredFees).to.equal(EXECUTION_FEE.muln(4));
		});
	});

	describe('Fund manager', async function () {
		let trade: TradeInstance;
		beforeEach(async function () {
			await init(0, { manager: signer2 });
			const tradeAddress = (await contracts.interaction.funds(FUND_ID))[0];
			trade = await Trade.at(tradeAddress);
		});

		it('should be able to request changing manager and his share, change is applied after the report', async function () {
			await contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, ether('1'), 0, { from: signer1 });
			expect(await trade.manager()).to.equal(signer2);
			expect(await contracts.fees.managerShare(FUND_ID)).to.equal(0);
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger });
			expect(await trade.manager()).to.equal(signer1);
			expect(await contracts.fees.managerShare(FUND_ID)).to.equal(ether('1'));
		});

		it('allows to instantly tradelock new manager', async function () {
			const tradeLock = await time.latest()
			await contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, ether('1'), tradeLock, { from: signer1 });
			
			expect(await trade.isLockedUntil(signer2)).to.equal(tradeLock)
		});

		it('not allowed to request invalid manager share', async function () {
			await expect(
				contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, ether('10'), 0, { from: signer1 })
			).to.be.revertedWith('DO/IMS');
			await expect(
				contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, ether('1.000001'), 0, { from: signer1 })
			).to.be.revertedWith('DO/IMS');
			await expect(
				contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, ether('1'), 0, { from: signer1 })
			).not.to.revertedWith('DO/IMS');
			await expect(
				contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, 0, 0, { from: signer1 })
			).not.to.revertedWith('DO/IMS');
		});

		it('only owner can request manager change', async function () {
			await expect(
				contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, 0, 0, { from: signer2 })
			).to.be.revertedWith('DO/AD');
			await expect(
				contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, 0, 0, { from: trigger })
			).to.be.revertedWith('DO/AD');
			await expect(
				contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, 0, 0, { from: signer1 })
			).not.to.revertedWith('DO/AD');
		});

		it('owner can cancel the changing manager request', async function () {
			await contracts.dripOperator.requestUpdateManager(FUND_ID, signer1, 0, 0, { from: signer1 });
			await expect(contracts.dripOperator.cancelUpdateManager(FUND_ID, { from: signer2 })).to.be.revertedWith('DO/AD');
			await expect(contracts.dripOperator.cancelUpdateManager(FUND_ID, { from: trigger })).to.be.revertedWith('DO/AD');
			await expect(contracts.dripOperator.cancelUpdateManager(FUND_ID, { from: signer1 })).not.to.revertedWith('DO/AD');
		});
	});

	describe('Trailing stop', async function () {
		let trade: TradeInstance;
		beforeEach(async function () {
			await init(0, { manager: signer2 });
			const tradeAddress = (await contracts.interaction.funds(FUND_ID))[0];
			trade = await Trade.at(tradeAddress);
		});

		it('allows set trailing stop, change is applied after the report', async function () {
			await contracts.dripOperator.requestUpdateTrailingStop(
				FUND_ID,
				true,
				ether('0.1'),
				true,
				ether('0.1'),
				{ from: signer1 }
			)
			await expectTrailingStop(trade, false, ether('0'), ether('0'), false, ether('0'), ether('0'))
			await contracts.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger })
			await expectTrailingStop(trade, true, ether('0.1'), ether('0.09'), true, ether('0.1'), ether('0.09'))
		});

		it('doesnt allow to request trailing stop update if sender is not an owner', async function () {
			await expect(
				contracts.dripOperator.requestUpdateTrailingStop(
					FUND_ID,
					true,
					ether('0.1'),
					true,
					ether('0.1'),
					{ from: signer2 }
				)
			).to.revertedWith('DO/AD')
		});

		it('allows to cancel trailing stop update', async function () {
			await contracts.dripOperator.requestUpdateTrailingStop(
				FUND_ID,
				true,
				ether('0.1'),
				true,
				ether('0.1'),
				{ from: signer1 }
			)
			
			await contracts.dripOperator.cancelUpdateTrailingStop(FUND_ID, { from: signer1 })
			await contracts.interaction.drip(FUND_ID, '0', { from: trigger })

			await expectTrailingStop(trade, false, ether('0'), ether('0'), false, ether('0'), ether('0'))
		})

		it('doesnt allow to cancel trailing stop update if sender is not an owner', async function () {
			await contracts.dripOperator.requestUpdateTrailingStop(
				FUND_ID,
				true,
				ether('0.1'),
				true,
				ether('0.1'),
				{ from: signer1 }
			)

			await expect(
				contracts.dripOperator.cancelUpdateTrailingStop(FUND_ID, { from: signer2 })
			).to.revertedWith('DO/AD')
		});

		it('moves trailing stops when token rate raises', async function () {
			await contracts.dripOperator.requestUpdateTrailingStop(
				FUND_ID,
				true,
				ether('0.1'),
				true,
				ether('0.1'),
				{ from: signer1 }
			)
			await contracts.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});

			await contracts.interaction.drip(FUND_ID, usdc("200"), {from: trigger});
			
			await expectTrailingStop(trade, true, ether('0.1'), ether('0.171'), true, ether('0.1'), ether('0.171'))
		})

		it('doesnt move trailing stop when token rate falls', async function () {
			await contracts.dripOperator.requestUpdateTrailingStop(
				FUND_ID,
				true,
				ether('0.1'),
				true,
				ether('0.1'),
				{ from: signer1 }
			)
			await contracts.interaction.stake(FUND_ID, usdc("100").toString(), {from: signer1});
			await contracts.interaction.drip(FUND_ID, "0", {from: trigger});

			await contracts.interaction.drip(FUND_ID, usdc("95"), {from: trigger});
			
			await expectTrailingStop(trade, true, ether('0.1'), ether('0.09'), true, ether('0.1'), ether('0.09'))
		})

		it('doesnt allow to request trailing stop more than 100%', async function() {
			await expect(contracts.dripOperator.requestUpdateTrailingStop(FUND_ID, true, ether('1.1'), false, ether('0'), { from: signer1 }))
				.to.revertedWith('DO/ITS')
				await expect(contracts.dripOperator.requestUpdateTrailingStop(FUND_ID, false, ether('0'), true, ether('1.1'), { from: signer1 }))
				.to.revertedWith('DO/ITS')
		})
	})
});
