import { expect } from 'chai';
import { Contracts, setup } from './setup';
import { ethers } from 'hardhat';

contract('SubaccountRegistry', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
    let contracts: Contracts;

    beforeEach(async function () {
        const init = await setup(deployer, signer1, signer2, trigger, signer3);
		contracts = init.contracts;
    })

    it('allows to top up newly enabled account', async function () {
        const signer = await ethers.getSigner(signer2)
        const provider = signer.provider
        const currenBalance = await provider.getBalance(signer2)
        const newBalance = currenBalance.add('1')
        await contracts.subaccountRegistry.enableAccount(signer2, { from: signer1, value: '1' })

        expect((await provider.getBalance(signer2)).toString()).to.eq(newBalance.toString())
    })

    it('doesnt allow to enable one account for multiple owners', async function () {
        await contracts.subaccountRegistry.enableAccount(signer2, { from: signer1 })

        await expect(contracts.subaccountRegistry.enableAccount(signer2, { from: signer3 }))
            .to.revertedWith('SR/AAE')
    })

    it('doesnt allow to disable other owner\'s account', async function () {
        await contracts.subaccountRegistry.enableAccount(signer2, { from: signer1 })

        await expect(contracts.subaccountRegistry.disableAccount(signer2, { from: signer3 }))
            .to.revertedWith('SR/AD')
    })
})