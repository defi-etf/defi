import { expect } from 'chai';
import { Interface } from 'ethers/lib/utils';
import { Contracts, setup } from './setup';
import { time } from '@nomicfoundation/hardhat-network-helpers';
import { UPGRADE_PERIOD } from './constants';
import { parseEventFromLogs } from './utils';
import { DummyDelayedExecutorInstance } from '../typechain-types/contracts/main/DelayedExecutor.sol/DummyDelayedExecutor';

const DelayedExecutor = artifacts.require('DummyDelayedExecutor')

contract('DelayedExecutor', function ([deployer, signer1, signer2, trigger, signer3]: string[]) {
    let contracts: Contracts
    let contract: DummyDelayedExecutorInstance

    beforeEach(async function() {
        const init = await setup(deployer, signer1, signer2, trigger, signer3);
		contracts = init.contracts;
        contract = await DelayedExecutor.new(UPGRADE_PERIOD, UPGRADE_PERIOD / 2)
    })

    it('allows to set delay', async function () {
        await contract.setDelay(UPGRADE_PERIOD / 2)
        const calldata = new Interface([
            'function swap(address, address, uint256) external returns(uint256)'
        ]).encodeFunctionData(
            'swap',
            [contracts.usdt.address, contracts.stable.address, 1]
        )
        const receipt = await contract.requestTx(contracts.swapMock.address, calldata)
        const logs = parseEventFromLogs(receipt, 'event TxRequested(address indexed,uint256 indexed,uint256,address indexed,bytes)')
        const id = logs[0].args[1]
        await time.increase(UPGRADE_PERIOD / 2)

        await expect(contract.executeTx(id)).to.not.revertedWith('DE/DNP')
    })

    it('doesnt allow to set delay from other address than the owner', async function() {
        await expect(contract.setDelay(UPGRADE_PERIOD / 2, {from: signer1}))
            .to.revertedWith('Ownable: caller is not the owner')
    })

    it('doesnt allow to set delay less than minimal', async function() {
        await expect(contract.setDelay(UPGRADE_PERIOD / 2 - 1))
            .to.revertedWith('DE/DS')
    })

    it('reverts with an appropriate message when internal call gets reverted', async function() {
        const calldata = new Interface([
            'function swap(address, address, uint256) external returns(uint256)'
        ]).encodeFunctionData(
            'swap',
            [contracts.usdt.address, contracts.usdt.address, 1]
        )
        const receipt = await contract.requestTx(contracts.swapMock.address, calldata)
        const logs = parseEventFromLogs(receipt, 'event TxRequested(address indexed,uint256 indexed,uint256,address indexed,bytes)')
        const id = logs[0].args[1]
        await time.increase(UPGRADE_PERIOD)

        await expect(contract.executeTx(id)).to.revertedWith('Same assets')
    })
})