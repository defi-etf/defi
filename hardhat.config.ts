import { task, subtask, types, HardhatUserConfig, internalTask } from 'hardhat/config';
import {TASK_COMPILE_SOLIDITY_READ_FILE} from 'hardhat/builtin-tasks/task-names';
import addresses from './addresses.json'
import '@typechain/hardhat'
import '@nomicfoundation/hardhat-ethers'
import '@nomicfoundation/hardhat-chai-matchers'
import "@nomiclabs/hardhat-waffle";
import "@nomiclabs/hardhat-etherscan";
import '@nomiclabs/hardhat-truffle5';
import 'hardhat-deploy';
import 'hardhat-gas-reporter';
import 'solidity-coverage';
import 'hardhat-contract-sizer';
import 'hardhat-spdx-license-identifier';
import 'hardhat-abi-exporter';
import 'hardhat-storage-layout';
import '@openzeppelin/hardhat-upgrades';
import '@primitivefi/hardhat-marmite';
import '@openzeppelin/test-helpers';
import { getNetworkKey } from './deploy/utils/get-network-key';

require('dotenv').config();
const fs = require("fs");

const {
    GOERLI_PRIVATE_KEY,
    ALCHEMY_API_KEY,
    OPTIMISM_API_KEY,
    ARBITRUM_ALCHEMY_API_KEY,
    ARBITRUM_ETHERSCAN_API_KEY,
    BSC_API_KEY,
    GOERLI_ALCHEMY_API_KEY,
    GOERLI_ETHERSCAN_API_KEY,
} = process.env;

// task("fork_reset", "Reset to local fork", async (taskArgs) => {
//     await network.provider.request({
//         method: "hardhat_reset",
//         params: [],
//     });
// });


const config: HardhatUserConfig = {
    typechain: {
      target: 'truffle-v5',
    },
    solidity: {
      compilers: [
          {
              version: "0.8.20",
              settings: {
                  optimizer: {
                      enabled: true,
                      runs: 1000000,
                  },
                  outputSelection: {
                      "*": {
                          "*": ["storageLayout"],
                      },
                  },
                  viaIR: true,
                  evmVersion: "paris"
              },
          }
      ],
  },
    networks: {
        hardhat: {
            initialDate: '01 Jan 1970 00:00:00 GMT',
            accounts: {
                count: 61
            },
            gasPrice: 100000000, // 0.1 gwei
            initialBaseFeePerGas: 100000000, // 0.1 gwei
        },
        devnet: {
            url: `https://opt-goerli.g.alchemy.com/v2/${GOERLI_ALCHEMY_API_KEY}`,
            chainId: 420,
            accounts: [`${GOERLI_PRIVATE_KEY}`],
            gasPrice: 1000000000,
        },
        optimism: {
            url: `https://opt-mainnet.g.alchemy.com/v2/${ALCHEMY_API_KEY}`,
            accounts: [`${GOERLI_PRIVATE_KEY}`]
        },
        arbitrum: {
            url: `https://arb-mainnet.g.alchemy.com/v2/${ARBITRUM_ALCHEMY_API_KEY}`,
            chainId: 42161,
            accounts: [`${GOERLI_PRIVATE_KEY}`]
        },
        arbitrum_dev: {
            url: `https://arb-mainnet.g.alchemy.com/v2/${ARBITRUM_ALCHEMY_API_KEY}`,
            chainId: 42161,
            accounts: [`${GOERLI_PRIVATE_KEY}`]
        },
        bsc: {
            url: `https://flashy-proportionate-pond.bsc.discover.quiknode.pro/ee9ea33500eda014d36d708cb248cfcba4a0021c/`,
            chainId: 56,
            accounts: [`${GOERLI_PRIVATE_KEY}`]
        }
    },
    etherscan: {
        apiKey: {
            // ethereum: MAINNET_ETHERSCAN_KEY,
            goerli: GOERLI_ETHERSCAN_API_KEY!,
            optimisticEthereum: OPTIMISM_API_KEY!,
            arbitrumOne: ARBITRUM_ETHERSCAN_API_KEY!,
            bsc: BSC_API_KEY!,
        },
        customChains: [
            {
                network: "goerli",
                chainId: 420,
                urls: {
                    apiURL: "https://api-goerli-optimism.etherscan.io/api",
                    browserURL: "https://goerli-optimism.etherscan.io"
                }
            }
        ]
  },
    gasReporter: {
    enabled: true,
    currency: 'USD',
  },
    contractSizer: {
      alphaSort: true,
        disambiguatePaths: true,
        runOnCompile: true,
        strict: false,
    },
    spdxLicenseIdentifier: {
        overwrite: false,
        runOnCompile: true,
    },
     abiExporter: {
        path: './abi',
         runOnCompile: true,
         clear: true,
         flat: true,
         only: [''],
         spacing: 2,
         pretty: true,
    }
};

export default config;

function getSortedFiles(dependenciesGraph: any) {
    const tsort = require("tsort")
    const graph = tsort()

    const filesMap: Record<string, unknown> = {}
    const resolvedFiles = dependenciesGraph.getResolvedFiles()
    resolvedFiles.forEach((f: any) => (filesMap[f.sourceName] = f))

    for (const [from, deps] of dependenciesGraph.entries()) {
        for (const to of deps) {
            graph.add(to.sourceName, from.sourceName)
        }
    }

    const topologicalSortedNames = graph.sort()

    // If an entry has no dependency it won't be included in the graph, so we
    // add them and then dedup the array
    const withEntries = topologicalSortedNames.concat(resolvedFiles.map((f: any) => f.sourceName))

    const sortedNames = Array.from(new Set(withEntries))
    return sortedNames.map((n: any) => filesMap[n])
}

function getFileWithoutImports(resolvedFile: any) {
    const IMPORT_SOLIDITY_REGEX = /^\s*import(\s+)[\s\S]*?;\s*$/gm

    return resolvedFile.content.rawContent.replace(IMPORT_SOLIDITY_REGEX, "").trim()
}

subtask("flat:get-flattened-sources", "Returns all contracts and their dependencies flattened")
    .addOptionalParam("files", undefined, undefined, types.any)
    .addOptionalParam("output", undefined, undefined, types.string)
    .setAction(async ({ files, output }, { run }) => {
        const dependencyGraph = await run("flat:get-dependency-graph", { files })
        console.log(dependencyGraph)

        let flattened = ""

        if (dependencyGraph.getResolvedFiles().length === 0) {
            return flattened
        }

        const sortedFiles = getSortedFiles(dependencyGraph)

        let isFirst = true
        for (const file of sortedFiles) {
            if (!isFirst) {
                flattened += "\n"
            }
            flattened += `// File ${(file as any).getVersionedName()}\n`
            flattened += `${getFileWithoutImports(file)}\n`

            isFirst = false
        }

        // Remove every line started with "// SPDX-License-Identifier:"
        flattened = flattened.replace(/SPDX-License-Identifier:/gm, "License-Identifier:")

        flattened = `// SPDX-License-Identifier: MIXED\n\n${flattened}`

        // Remove every line started with "pragma experimental ABIEncoderV2;" except the first one
        flattened = flattened.replace(/pragma experimental ABIEncoderV2;\n/gm, ((i) => (m: string) => (!i++ ? m : ""))(0))
        // Remove every line started with "pragma abicoder v2;" except the first one
        flattened = flattened.replace(/pragma abicoder v2;\n/gm, ((i) => (m: string) => (!i++ ? m : ""))(0))
        // Remove every line started with "pragma solidity ****" except the first one
        flattened = flattened.replace(/pragma solidity .*$\n/gm, ((i) => (m: string) => (!i++ ? m : ""))(0))


        flattened = flattened.trim()
        if (output) {
            console.log("Writing to", output)
            fs.writeFileSync(output, flattened)
            return ""
        }
        return flattened
    })

subtask("flat:get-dependency-graph")
    .addOptionalParam("files", undefined, undefined, types.any)
    .setAction(async ({ files }, { run }) => {
        const sourcePaths = files === undefined ? await run("compile:solidity:get-source-paths") : files.map((f: any) => fs.realpathSync(f))

        const sourceNames = await run("compile:solidity:get-source-names", {
            sourcePaths,
        })

        const dependencyGraph = await run("compile:solidity:get-dependency-graph", { sourceNames })

        return dependencyGraph
    })

task("flat", "Flattens and prints contracts and their dependencies")
    .addOptionalVariadicPositionalParam("files", "The files to flatten", undefined, types.inputFile)
    .addOptionalParam("output", "Specify the output file", undefined, types.string)
    .setAction(async ({ files, output }, { run }) => {
        console.log(
            await run("flat:get-flattened-sources", {
                files,
                output,
            })
        )
    })


internalTask(TASK_COMPILE_SOLIDITY_READ_FILE).setAction(
    async ({absolutePath}: {absolutePath: string}, hre, runSuper): Promise<string> => {
      let content = await runSuper({absolutePath});
      let lines = content.split('\n')
      let result: string[] = []
      const directiveRegExp = new RegExp(/\/\/\s*@address:\s*(.+)/)
      const replaceRegExp = new RegExp(/(\s*)([^\s]+)\s(.+)=\s*.+;/)
      let iterator = lines[Symbol.iterator]()
      while (true) {
        const next = iterator.next()
        if (next.done) break
        result.push(next.value)
        let matches = directiveRegExp.exec(next.value)
        if (!matches) continue
        const addressName = matches[1]
        const nextLine = iterator.next()
        const addressValue = addresses[getNetworkKey(hre)][addressName]
        const processedValue = nextLine.value.replace(replaceRegExp, `$1$2 $3= $2(${addressValue});`)
        result.push(processedValue)
      }
      return result.join('\n');
    }
  );